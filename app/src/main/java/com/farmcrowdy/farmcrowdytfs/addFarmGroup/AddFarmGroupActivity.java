package com.farmcrowdy.farmcrowdytfs.addFarmGroup;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.farmcrowdy.farmcrowdytfs.DirectPojo.CropPojo;
import com.farmcrowdy.farmcrowdytfs.DirectPojo.FarmGroupPojo;
import com.farmcrowdy.farmcrowdytfs.DirectPojo.FarmLocationPojo;
import com.farmcrowdy.farmcrowdytfs.NoPhotoInterface;
import com.farmcrowdy.farmcrowdytfs.R;
import com.farmcrowdy.farmcrowdytfs.farmUpdates.FarmUpdateActvity;
import com.novachevskyi.datepicker.CalendarDatePickerDialog;
import com.rafakob.floatingedittext.FloatingEditText;
import com.raizlabs.android.dbflow.sql.language.SQLite;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * Created by Oluwatobi on 5/31/2018.
 */

public class AddFarmGroupActivity extends AppCompatActivity implements NoPhotoInterface{
    FloatingEditText cycleDate,createGroup_landAreaAssigned;
    TextView startCycle,createGroup_groupName;
    Spinner createGroup_farmType,numberOfAcres,locations_spinner;
    Button createGroupButton;

    String chosenAcres;
    String chosenCropString = "";
    String chosenLocationString="";
    int chosenCropId, chosenLocationId;
    ProgressDialog progressDialog;

    List<String> cropTypesListString, locationListString;
    List<Integer> cropTypesListInteger, locationListInteger;
    final String DATE_PICKER_TAG = "TAG";

    AddFarmGroupPresenter addFarmGroupPresenter;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.create_farmgroup_activity);

        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitleTextColor(Color.WHITE);
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Creating a new farm group");
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);
        addFarmGroupPresenter = new AddFarmGroupPresenterImpl(this);

        cycleDate = findViewById(R.id.createGroup_cycleDuration);
        createGroup_landAreaAssigned = findViewById(R.id.createGroup_landAreaAssigned);
        createGroup_groupName  = findViewById(R.id.createGroup_groupName);

        startCycle = findViewById(R.id.startCycle);
        startCycle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDatePicker();
            }
        });


        createGroup_farmType = (Spinner) findViewById(R.id.createGroup_farmType);
        numberOfAcres = (Spinner)findViewById(R.id.numberOfAcres);
        locations_spinner = (Spinner)findViewById(R.id.locations_spinner);
        createGroupButton = (Button) findViewById(R.id.createGroupButton);

        doActions();

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<>(this,
                android.R.layout.simple_spinner_item, cropTypesListString);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        createGroup_farmType.setAdapter(dataAdapter);

        ArrayAdapter<String> dataAdapter2 = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, locationListString);
        dataAdapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        locations_spinner.setAdapter(dataAdapter2);

        createGroup_farmType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                chosenCropId = cropTypesListInteger.get(i);
                chosenCropString = adapterView.getItemAtPosition(i).toString();
                String groupNamerrr = String.format("%s - %s - %s", chosenCropString,chosenLocationString, startCycle.getText().toString() );
                createGroup_groupName.setText(groupNamerrr);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        locations_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                chosenLocationId = locationListInteger.get(i);
                chosenLocationString = adapterView.getItemAtPosition(i).toString();
                String groupNamerrr = String.format("%s - %s - %s", chosenCropString,chosenLocationString, startCycle.getText().toString() );
                createGroup_groupName.setText(groupNamerrr);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        numberOfAcres.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                chosenAcres = adapterView.getItemAtPosition(i).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        createGroupButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(!TextUtils.isEmpty(createGroup_landAreaAssigned.getText()) ){
                    progressDialog.show();
                    String groupNamerrr = String.format("%s - %s - %s", chosenCropString,chosenLocationString, startCycle.getText().toString() );
                    addFarmGroupPresenter.addGroupNow(groupNamerrr,
                            chosenCropString,
                            cycleDate.getText(),
                            Integer.valueOf(createGroup_landAreaAssigned.getText()),
                    chosenAcres, chosenLocationId);
                }
                else {
                    Toast.makeText(AddFarmGroupActivity.this, "All fields must be filled", Toast.LENGTH_SHORT).show();
                }
            }
        });

    }

    private void doActions() {
        List<CropPojo> cropPojoList = SQLite.select().
                from(CropPojo.class).queryList();

        List<FarmLocationPojo> locationList = SQLite.select().
                from(FarmLocationPojo.class).queryList();

        cropTypesListString = new ArrayList<>();
        cropTypesListInteger = new ArrayList<>();
        locationListString = new ArrayList<>();
        locationListInteger = new ArrayList<>();


        for(int i=0; i<cropPojoList.size(); i++){
            cropTypesListString.add(cropPojoList.get(i).getCrop_name());
            cropTypesListInteger.add(cropPojoList.get(i).getCrop_id());
        }

        for(int i=0; i<locationList.size(); i++) {
            locationListString.add(locationList.get(i).getLocation());
            locationListInteger.add(locationList.get(i).getLocation_id());
        }


    }

    private void showDatePicker() {
        Calendar calendar = Calendar.getInstance();
        int year = calendar.get(Calendar.YEAR);
        int monthOfYear = calendar.get(Calendar.MONTH);
        int dayOfMonth = calendar.get(Calendar.DAY_OF_MONTH);
        final CalendarDatePickerDialog dialog =
                CalendarDatePickerDialog.newInstance(dateSetListener, year, monthOfYear, dayOfMonth);

        dialog.show(getSupportFragmentManager(), DATE_PICKER_TAG);

    }

    CalendarDatePickerDialog.OnDateSetListener dateSetListener =
            new CalendarDatePickerDialog.OnDateSetListener() {
                @Override
                public void onDateSet(CalendarDatePickerDialog dialog, int year, int monthOfYear,
                                      int dayOfMonth) {
                    startCycle.setText(String.valueOf(dayOfMonth+" / ")+monthOfYear+" / "+ year);
                    String groupNamerrr = String.format("%s-%s-%s", chosenCropString,chosenLocationString, startCycle.getText().toString() );
                    createGroup_groupName.setText(groupNamerrr);
                }
            };


    @Override
    public void successful() {
        progressDialog.dismiss();
        progressDialog.cancel();
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("New farm group has been created and logged to the headquaters database. Well done and lets keep farming!");
        builder.setTitle("Successful");
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
           finish();
            }
        });
        builder.show();

    }

    @Override
    public void failure(String error) {
        if(error.contains("No address")) error = "No internet connection found";
        progressDialog.dismiss();
        progressDialog.cancel();
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Due to  "+error);
        builder.setTitle("Failed");
        builder.show();
    }

    @Override
    public void uploadPhotoSuccessful(String url) {

    }
}
