package com.farmcrowdy.farmcrowdytfs.addFarmGroup;

import okhttp3.RequestBody;
import retrofit2.http.Part;

/**
 * Created by Oluwatobi on 5/31/2018.
 */

public interface AddFarmGroupPresenter {
void addGroupNow(String groupName,
                 String cropType,
                 String cycleDuraton,
                 int landAssignArea,
                 String unit,
                  int location_id);
}
