package com.farmcrowdy.farmcrowdytfs.addFarmGroup;

import com.farmcrowdy.farmcrowdytfs.ApplicationInstance;
import com.farmcrowdy.farmcrowdytfs.MyEndpoint;
import com.farmcrowdy.farmcrowdytfs.NoPhotoInterface;
import com.farmcrowdy.farmcrowdytfs.engine.TinyDB;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Oluwatobi on 5/31/2018.
 */

public class AddFarmGroupPresenterImpl implements AddFarmGroupPresenter {
    NoPhotoInterface noPhotoInterface;

    public AddFarmGroupPresenterImpl(NoPhotoInterface noPhotoInterface) {
        this.noPhotoInterface = noPhotoInterface;
    }

    @Override
    public void addGroupNow(String groupName, String cropType, String cycleDuraton, int landAssignArea, String unit, int location_id) {
        MyEndpoint myEndpoint = ApplicationInstance.getRetrofit().create(MyEndpoint.class);
        RequestBody groupNameRequest  =  RequestBody.create(MediaType.parse("text/plain"), groupName);
        final RequestBody cropTypeRequest  =  RequestBody.create(MediaType.parse("text/plain"), cropType);
        RequestBody cyclerDurationRequest  =  RequestBody.create(MediaType.parse("text/plain"), cycleDuraton);
        RequestBody unitRequest  =  RequestBody.create(MediaType.parse("text/plain"), unit);

        Call<ResponseBody> call = myEndpoint.addNewGroup(groupNameRequest,cropTypeRequest,cyclerDurationRequest,
                landAssignArea,unitRequest,location_id);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if(response.isSuccessful()){
                    noPhotoInterface.successful();
                }
                else noPhotoInterface.failure(response.message());
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
            noPhotoInterface.failure(t.getMessage());
            }
        });
    }
}
