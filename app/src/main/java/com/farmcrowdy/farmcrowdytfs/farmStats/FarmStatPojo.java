package com.farmcrowdy.farmcrowdytfs.farmStats;

import com.farmcrowdy.farmcrowdytfs.engine.MyDatabase;
import com.raizlabs.android.dbflow.annotation.Column;
import com.raizlabs.android.dbflow.annotation.PrimaryKey;
import com.raizlabs.android.dbflow.annotation.Table;
import com.raizlabs.android.dbflow.structure.BaseModel;

@Table(database = MyDatabase.class)
public class FarmStatPojo extends BaseModel {
    @Column
    @PrimaryKey
    int stat_id;
    @Column
    String farms_sponsored;
    @Column
            String sold_this_week;
    @Column
            String farmers_empowered;
    @Column
            String farm_sponsors;
    @Column
            String poultry;
    @Column
            String new_this_week;
    @Column
            String farm_followers;
    @Column
            String last_twentyfour_hours;
    @Column
            String states_covered;

    public void setFarms_sponsored(String farms_sponsored) {
        this.farms_sponsored = farms_sponsored;
    }

    public void setSold_this_week(String sold_this_week) {
        this.sold_this_week = sold_this_week;
    }

    public void setFarmers_empowered(String farmers_empowered) {
        this.farmers_empowered = farmers_empowered;
    }

    public void setFarm_sponsors(String farm_sponsors) {
        this.farm_sponsors = farm_sponsors;
    }

    public void setPoultry(String poultry) {
        this.poultry = poultry;
    }

    public void setNew_this_week(String new_this_week) {
        this.new_this_week = new_this_week;
    }

    public void setFarm_followers(String farm_followers) {
        this.farm_followers = farm_followers;
    }

    public void setLast_twentyfour_hours(String last_twentyfour_hours) {
        this.last_twentyfour_hours = last_twentyfour_hours;
    }

    public void setStates_covered(String states_covered) {
        this.states_covered = states_covered;
    }

    public String getFarms_sponsored() {
        return farms_sponsored;
    }

    public String getSold_this_week() {
        return sold_this_week;
    }

    public String getFarmers_empowered() {
        return farmers_empowered;
    }

    public String getFarm_sponsors() {
        return farm_sponsors;
    }

    public String getPoultry() {
        return poultry;
    }

    public String getNew_this_week() {
        return new_this_week;
    }

    public String getFarm_followers() {
        return farm_followers;
    }

    public String getLast_twentyfour_hours() {
        return last_twentyfour_hours;
    }

    public String getStates_covered() {
        return states_covered;
    }
}
