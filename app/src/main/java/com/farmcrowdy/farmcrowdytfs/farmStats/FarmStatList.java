package com.farmcrowdy.farmcrowdytfs.farmStats;

import java.util.ArrayList;

public class FarmStatList {
    String status;
    ArrayList<FarmStatPojo> message;

    public String getStatus() {
        return status;
    }

    public ArrayList<FarmStatPojo> getMessage() {
        return message;
    }
}
