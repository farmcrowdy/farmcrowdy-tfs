package com.farmcrowdy.farmcrowdytfs.onlyFarmer;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.farmcrowdy.farmcrowdytfs.ListPojo.FarmerListPojo;
import com.farmcrowdy.farmcrowdytfs.R;

/**
 * Created by Oluwatobi on 6/1/2018.
 */

public class JustFarmerActivity extends AppCompatActivity implements JustFarmerActivityInterface {
    ProgressBar progressBar;
    TextView callback, grouplabel;
    JustFarmerPresenter presenter;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.farmer_list_activity);

        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitleTextColor(Color.WHITE);
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        callback = (TextView)findViewById(R.id.callback);
        progressBar = (ProgressBar)findViewById(R.id.grgProgress);

        grouplabel = (TextView) findViewById(R.id.grouplabel);
        grouplabel.setText(getIntent().getExtras().getString("label"));
        startSearchMock();
        presenter = new JustFarmerPresenterImpl(this);
        presenter.checkForFarmersWithGroupId(getIntent().getExtras().getInt("id", 0));



    }

    private void startSearchMock(){
        progressBar.setVisibility(View.VISIBLE);
        callback.setText("Getting information");
        callback.setVisibility(View.VISIBLE);

    }
    private void endSearchMock(){
        progressBar.setVisibility(View.GONE);
        callback.setVisibility(View.GONE);
    }
    private void negativeFeedback(){
        progressBar.setVisibility(View.GONE);
        callback.setText("No farmer found in this group");
    }

    @Override
    public void receiveSuccessful(FarmerListPojo farmerListPojo) {
    endSearchMock();
        grouplabel.append(" ("+farmerListPojo.getMessage().size()+" farmers) ");
        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.farmerListMainRecycler);
        LinearLayoutManager linearLayout = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(linearLayout);
        recyclerView.setHasFixedSize(true);
        JustFarmerAdapter justAdapter = new JustFarmerAdapter(farmerListPojo.getMessage(), this);
        recyclerView.setAdapter(justAdapter);
    }

    @Override
    public void failureRcv(String error) {
    negativeFeedback();
    }
}
