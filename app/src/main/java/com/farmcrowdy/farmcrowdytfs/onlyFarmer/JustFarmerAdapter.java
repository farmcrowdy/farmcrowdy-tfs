package com.farmcrowdy.farmcrowdytfs.onlyFarmer;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.farmcrowdy.farmcrowdytfs.DirectPojo.FarmerPojo;
import com.farmcrowdy.farmcrowdytfs.R;
import com.farmcrowdy.farmcrowdytfs.profile.ProfileActivity;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Oluwatobi on 6/1/2018.
 */

public class JustFarmerAdapter extends RecyclerView.Adapter<JustFarmerAdapter.JustFarmerViewHoldeR> {
    private ArrayList<FarmerPojo> farmerPojoArrayList;
    private Context context;

    public JustFarmerAdapter(ArrayList<FarmerPojo> farmerPojoArrayList, Context context) {
        this.farmerPojoArrayList = farmerPojoArrayList;
        this.context = context;
    }

    @Override
    public JustFarmerViewHoldeR onCreateViewHolder(ViewGroup parent, int viewType) {
        View view  = LayoutInflater.from(parent.getContext()).inflate(R.layout.each_farmer_items, parent,false);
        return new JustFarmerViewHoldeR(view);
    }

    @Override
    public void onBindViewHolder(@NonNull JustFarmerViewHoldeR holder, @SuppressLint("RecyclerView") final int position) {
        holder.farmerName.setText(String.format("%s %s", farmerPojoArrayList.get(position).getFname(), farmerPojoArrayList.get(position).getSname()));
        holder.gender.setText(farmerPojoArrayList.get(position).getGender());
        String link = "https://www.farmcrowdy.com/farmapp/assets/images/farmerpix/"+farmerPojoArrayList.get(position).getPro_image_thumbnail();
        Picasso.with(context).load(link).placeholder(R.drawable.ic_burst_mode_grey_24dp).into(holder.circleImageView);
       final  String croptype = farmerPojoArrayList.get(position).getCrop_proficiency();
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, ProfileActivity.class);
                intent.putExtra("data", croptype);
                intent.putExtra("id", farmerPojoArrayList.get(position).getId());
                context.startActivity(intent);

            }
        });

    }

    @Override
    public int getItemCount() {
        if(farmerPojoArrayList!=null) return farmerPojoArrayList.size();
        else return 0;
    }

    public class JustFarmerViewHoldeR extends RecyclerView.ViewHolder {
        TextView farmerName, gender;
        CircleImageView circleImageView;


        public JustFarmerViewHoldeR(View itemView) {
            super(itemView);
            farmerName = (TextView) itemView.findViewById(R.id.farmer_name);
            gender = (TextView) itemView.findViewById(R.id.farmer_gender);
            circleImageView = (CircleImageView)itemView.findViewById(R.id.each_farmer_photo);
        }
    }
}
