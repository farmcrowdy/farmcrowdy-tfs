package com.farmcrowdy.farmcrowdytfs.onlyFarmer;

import com.farmcrowdy.farmcrowdytfs.ApplicationInstance;
import com.farmcrowdy.farmcrowdytfs.ListPojo.FarmerListPojo;
import com.farmcrowdy.farmcrowdytfs.MyEndpoint;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Oluwatobi on 6/1/2018.
 */

public class JustFarmerPresenterImpl implements JustFarmerPresenter {
    JustFarmerActivityInterface farmerActivityInterface;

    public JustFarmerPresenterImpl(JustFarmerActivityInterface farmerActivityInterface) {
        this.farmerActivityInterface = farmerActivityInterface;
    }

    @Override
    public void checkForFarmersWithGroupId(int groupId) {
        MyEndpoint myEndpoint = ApplicationInstance.getRetrofit().create(MyEndpoint.class);
        Call<FarmerListPojo> call = myEndpoint.getAllFarmerInAFarmGroup(groupId);
        call.enqueue(new Callback<FarmerListPojo>() {
            @Override
            public void onResponse(Call<FarmerListPojo> call, Response<FarmerListPojo> response) {
                if(response.isSuccessful()){
                farmerActivityInterface.receiveSuccessful(response.body());
                }
                else farmerActivityInterface.failureRcv(response.message());
            }

            @Override
            public void onFailure(Call<FarmerListPojo> call, Throwable t) {
            farmerActivityInterface.failureRcv(t.getMessage());
            }
        });
    }

    @Override
    public void searchForFarmers(String query) {
        MyEndpoint myEndpoint = ApplicationInstance.getRetrofit().create(MyEndpoint.class);
        Call<FarmerListPojo> call = myEndpoint.getFarmerList(query);
        call.enqueue(new Callback<FarmerListPojo>() {
            @Override
            public void onResponse(Call<FarmerListPojo> call, Response<FarmerListPojo> response) {
                if(response.isSuccessful()){
                    farmerActivityInterface.receiveSuccessful(response.body());
                }
                else farmerActivityInterface.failureRcv(response.message());
            }

            @Override
            public void onFailure(Call<FarmerListPojo> call, Throwable t) {
                farmerActivityInterface.failureRcv(t.getMessage());
            }
        });
    }
}
