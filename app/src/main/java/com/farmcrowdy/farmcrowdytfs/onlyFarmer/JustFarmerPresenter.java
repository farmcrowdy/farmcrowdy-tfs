package com.farmcrowdy.farmcrowdytfs.onlyFarmer;

/**
 * Created by Oluwatobi on 6/1/2018.
 */

public interface JustFarmerPresenter {
    void checkForFarmersWithGroupId(int groupId);
    void searchForFarmers(String query);
}
