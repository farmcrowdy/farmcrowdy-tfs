package com.farmcrowdy.farmcrowdytfs.onlyFarmer;

import com.farmcrowdy.farmcrowdytfs.ListPojo.FarmerListPojo;

/**
 * Created by Oluwatobi on 6/1/2018.
 */

public interface JustFarmerActivityInterface {
    void receiveSuccessful(FarmerListPojo farmerListPojo);
    void failureRcv(String error);
}
