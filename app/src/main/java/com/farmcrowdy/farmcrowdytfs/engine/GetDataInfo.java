package com.farmcrowdy.farmcrowdytfs.engine;

import com.farmcrowdy.farmcrowdytfs.DirectPojo.CropPojo;
import com.farmcrowdy.farmcrowdytfs.DirectPojo.CropPojo_Table;
import com.farmcrowdy.farmcrowdytfs.DirectPojo.FarmGroupPojo;
import com.farmcrowdy.farmcrowdytfs.DirectPojo.FarmGroupPojo_Table;
import com.raizlabs.android.dbflow.sql.language.SQLite;

import java.util.List;

public class GetDataInfo {
    public String getCropNameFromId(int id) {
        CropPojo fgList = SQLite.select().from(CropPojo.class).where(CropPojo_Table.crop_id.is(id)).querySingle();
        if(fgList!=null)return fgList.getCrop_name();
        else return "No available";
    }
    public String getFarmGroupFromId(int id) {
        FarmGroupPojo farmGroupPojo = SQLite.select().from(FarmGroupPojo.class).where(FarmGroupPojo_Table.group_id.is(id)).querySingle();
        if(farmGroupPojo!=null) return farmGroupPojo.getGroup_name();
        else return "No seen";
    }

}
