package com.farmcrowdy.farmcrowdytfs.engine;

import com.raizlabs.android.dbflow.annotation.Database;

@Database(name = MyDatabase.NAME, version = MyDatabase.VERSION)
public class MyDatabase {
    public static final String NAME = "FcdyDataBase";

    public static final int VERSION = 5;
}