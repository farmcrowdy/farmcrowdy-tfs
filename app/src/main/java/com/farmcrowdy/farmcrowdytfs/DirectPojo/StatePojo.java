package com.farmcrowdy.farmcrowdytfs.DirectPojo;

import com.farmcrowdy.farmcrowdytfs.engine.MyDatabase;
import com.raizlabs.android.dbflow.annotation.Column;
import com.raizlabs.android.dbflow.annotation.PrimaryKey;
import com.raizlabs.android.dbflow.annotation.Table;
import com.raizlabs.android.dbflow.structure.BaseModel;

/**
 * Created by Oluwatobi on 5/25/2018.
 */
@Table(database = MyDatabase.class)
public class StatePojo extends BaseModel {
    @Column
    @PrimaryKey
    int state_id;
    @Column
    String name;

    public int getState_id() {
        return state_id;
    }

    public String getName() {
        return name;
    }

    public void setState_id(int state_id) {
        this.state_id = state_id;
    }

    public void setName(String name) {
        this.name = name;
    }
}
