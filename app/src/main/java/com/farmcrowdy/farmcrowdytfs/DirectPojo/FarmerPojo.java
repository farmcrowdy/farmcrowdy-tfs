package com.farmcrowdy.farmcrowdytfs.DirectPojo;

/**
 * Created by Oluwatobi on 6/1/2018.
 */

public class FarmerPojo {
    String fname, sname,gender,pro_image,crop_proficiency, phone;
    int id, access_pin, farm_group_id;

    public int getAccess_pin() { return access_pin; }

    public int getFarm_group_id() { return farm_group_id; }

    public String getFname() {
        return fname;
    }

    public String getSname() {
        return sname;
    }

    public String getGender() {
        return gender;
    }

    public String getPro_image_thumbnail() {
        return pro_image;
    }

    public String getCrop_proficiency() {
        return crop_proficiency;
    }

    public String getPhone() {
        return phone;
    }

    public int getId() {
        return id;
    }
}
