package com.farmcrowdy.farmcrowdytfs.DirectPojo;

import android.os.Parcel;
import android.os.Parcelable;

import com.farmcrowdy.farmcrowdytfs.engine.MyDatabase;
import com.raizlabs.android.dbflow.annotation.Column;
import com.raizlabs.android.dbflow.annotation.PrimaryKey;
import com.raizlabs.android.dbflow.annotation.Table;
import com.raizlabs.android.dbflow.structure.BaseModel;

public class MessagePojo implements Parcelable{

    int msg_id;

    
    String message_text;
    
    String msg_videolink;
    
    String msg_photolink;
    
    String msg_audiolink;
    
    String msg_date;
    
    String farmername;
    
    String mobile;
    
    String farm_group;
    
    String gender;
    String propix;
    
    int msg_read;


    public MessagePojo() {
    }

    protected MessagePojo(Parcel in) {
        msg_id = in.readInt();
        message_text = in.readString();
        msg_videolink = in.readString();
        msg_photolink = in.readString();
        msg_audiolink = in.readString();
        msg_date = in.readString();
        farmername = in.readString();
        mobile = in.readString();
        propix = in.readString();
        farm_group = in.readString();
        gender = in.readString();
        msg_read = in.readInt();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(msg_id);
        dest.writeString(message_text);
        dest.writeString(msg_videolink);
        dest.writeString(msg_photolink);
        dest.writeString(msg_audiolink);
        dest.writeString(msg_date);
        dest.writeString(farmername);
        dest.writeString(mobile);
        dest.writeString(propix);
        dest.writeString(farm_group);
        dest.writeString(gender);
        dest.writeInt(msg_read);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<MessagePojo> CREATOR = new Creator<MessagePojo>() {
        @Override
        public MessagePojo createFromParcel(Parcel in) {
            return new MessagePojo(in);
        }

        @Override
        public MessagePojo[] newArray(int size) {
            return new MessagePojo[size];
        }
    };

    public void setMsg_id(int msg_id) {
        this.msg_id = msg_id;
    }

    public void setMessage_text(String message_text) {
        this.message_text = message_text;
    }

    public void setMsg_videolink(String msg_videolink) {
        this.msg_videolink = msg_videolink;
    }

    public String getPropix() {
        return propix;
    }

    public void setPropix(String propix) {
        this.propix = propix;
    }

    public void setMsg_photolink(String msg_photolink) {
        this.msg_photolink = msg_photolink;
    }

    public void setMsg_audiolink(String msg_audiolink) {
        this.msg_audiolink = msg_audiolink;
    }

    public void setMsg_date(String msg_date) {
        this.msg_date = msg_date;
    }

    public void setFarmername(String farmername) {
        this.farmername = farmername;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public void setFarm_group(String farm_group) {
        this.farm_group = farm_group;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public void setMsg_read(int msg_read) {
        this.msg_read = msg_read;
    }

    public int getMsg_read() {
        return msg_read;
    }

    public int getMsg_id() {
        return msg_id;
    }

    public String getMessage_text() {
        return message_text;
    }

    public String getMsg_videolink() {
        return msg_videolink;
    }

    public String getMsg_photolink() {
        return msg_photolink;
    }

    public String getMsg_audiolink() {
        return msg_audiolink;
    }

    public String getMsg_date() {
        return msg_date;
    }

    public String getFarmername() {
        return farmername;
    }

    public String getMobile() {
        return mobile;
    }

    public String getFarm_group() {
        return farm_group;
    }

    public String getGender() {
        return gender;
    }

}
