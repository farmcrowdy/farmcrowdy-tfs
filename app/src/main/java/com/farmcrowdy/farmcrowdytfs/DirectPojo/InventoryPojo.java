package com.farmcrowdy.farmcrowdytfs.DirectPojo;

import com.farmcrowdy.farmcrowdytfs.engine.MyDatabase;
import com.raizlabs.android.dbflow.annotation.Table;
import com.raizlabs.android.dbflow.structure.BaseModel;

/**
 * Created by Oluwatobi on 5/24/2018.
 */

public class InventoryPojo {
    int input_id;
    String input_name, input_unit;

    public String getInput_unit() {
        return input_unit;
    }

    public int getInput_id() {
        return input_id;
    }

    public String getInput_name() {
        return input_name;
    }

}
