package com.farmcrowdy.farmcrowdytfs.DirectPojo;

import com.farmcrowdy.farmcrowdytfs.engine.MyDatabase;
import com.raizlabs.android.dbflow.annotation.Column;
import com.raizlabs.android.dbflow.annotation.PrimaryKey;
import com.raizlabs.android.dbflow.annotation.Table;
import com.raizlabs.android.dbflow.structure.BaseModel;

/**
 * Created by Oluwatobi on 5/25/2018.
 */
@Table(database = MyDatabase.class)
public class FarmGroupPojo extends BaseModel {
    @Column
    @PrimaryKey
    int group_id;
    @Column
    String group_name;

    public int getGroup_id() {
        return group_id;
    }

    public String getGroup_name() {
        return group_name;
    }

    public void setGroup_id(int group_id) {
        this.group_id = group_id;
    }

    public void setGroup_name(String group_name) {
        this.group_name = group_name;
    }
}
