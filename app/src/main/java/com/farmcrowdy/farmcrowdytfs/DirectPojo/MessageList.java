package com.farmcrowdy.farmcrowdytfs.DirectPojo;

import android.os.Parcel;
import android.os.Parcelable;

import com.farmcrowdy.farmcrowdytfs.messages.MessageMyPojo;

import java.util.ArrayList;

public class MessageList {
    ArrayList<MessageMyPojo> message;

    public MessageList(ArrayList<MessageMyPojo> message) {
        this.message = message;
    }


    public ArrayList<MessageMyPojo> getMessage() {
        return message;
    }




}
