package com.farmcrowdy.farmcrowdytfs.DirectPojo;

import android.os.Parcel;
import android.os.Parcelable;

public class FingerprintDecidePojo implements Parcelable{
    //0 = enrol, 1 = validate
    int type, farmerId, returnGiver;
    String fileLink;

    public FingerprintDecidePojo(int type, int farmerId, String fileLink, int returnGiver) {
        this.type = type;
        this.farmerId = farmerId;
        this.fileLink = fileLink;
        this.returnGiver = returnGiver;
    }

    protected FingerprintDecidePojo(Parcel in) {
        type = in.readInt();
        farmerId = in.readInt();
        fileLink = in.readString();
        returnGiver = in.readInt();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(type);
        dest.writeInt(farmerId);
        dest.writeString(fileLink);
        dest.writeInt(returnGiver);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<FingerprintDecidePojo> CREATOR = new Creator<FingerprintDecidePojo>() {
        @Override
        public FingerprintDecidePojo createFromParcel(Parcel in) {
            return new FingerprintDecidePojo(in);
        }

        @Override
        public FingerprintDecidePojo[] newArray(int size) {
            return new FingerprintDecidePojo[size];
        }
    };

    public int getType() {
        return type;
    }

    public int getReturnGiver() {
        return returnGiver;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getFarmerId() {
        return farmerId;
    }

    public void setFarmerId(int farmerId) {
        this.farmerId = farmerId;
    }

    public String getFileLink() {
        return fileLink;
    }

    public void setFileLink(String fileLink) {
        this.fileLink = fileLink;
    }
}
