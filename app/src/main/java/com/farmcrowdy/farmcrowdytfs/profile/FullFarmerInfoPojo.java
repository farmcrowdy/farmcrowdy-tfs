package com.farmcrowdy.farmcrowdytfs.profile;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Oluwatobi on 5/25/2018.
 */

public class FullFarmerInfoPojo implements Parcelable {
    String fname, sname, dob,dobForPaga,gender,f_print, phone,marital_status,crop_proficiency,income_range,pro_image,pro_image_thumbnail,comment,acct_number,land_area_farmed;

    int user_add_id,state_id,local_id,farm_group_id,farm_location_id,bank_id,
            number_of_dependants,add_labour,years_of_experience,previous_training, accessPin, access_pin;

    public FullFarmerInfoPojo() {
    }

    public String getFname() {
        return fname;
    }

    public int getAccess_pin() {
        return access_pin;
    }

    public void setFname(String fname) {
        this.fname = fname;
    }

    public String getSname() {
        return sname;
    }

    public void setSname(String sname) {
        this.sname = sname;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getDobForPaga() {
        return dobForPaga;
    }

    public void setDobForPaga(String dobForPaga) {
        this.dobForPaga = dobForPaga;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getF_print() {
        return f_print;
    }

    public void setF_print(String f_print) {
        this.f_print = f_print;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getMarital_status() {
        return marital_status;
    }

    public void setMarital_status(String marital_status) {
        this.marital_status = marital_status;
    }

    public String getCrop_proficiency() {
        return crop_proficiency;
    }

    public void setCrop_proficiency(String crop_proficiency) {
        this.crop_proficiency = crop_proficiency;
    }

    public String getIncome_range() {
        return income_range;
    }

    public void setIncome_range(String income_range) {
        this.income_range = income_range;
    }

    public String getPro_image() {
        return pro_image;
    }

    public void setPro_image(String pro_image) {
        this.pro_image = pro_image;
    }

    public String getPro_image_thumbnail() {
        return pro_image_thumbnail;
    }

    public void setPro_image_thumbnail(String pro_image_thumbnail) {
        this.pro_image_thumbnail = pro_image_thumbnail;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getAcct_number() {
        return acct_number;
    }

    public void setAcct_number(String acct_number) {
        this.acct_number = acct_number;
    }

    public String getLand_area_farmed() {
        return land_area_farmed;
    }

    public void setLand_area_farmed(String land_area_farmed) {
        this.land_area_farmed = land_area_farmed;
    }

    public int getUser_add_id() {
        return user_add_id;
    }

    public void setUser_add_id(int user_add_id) {
        this.user_add_id = user_add_id;
    }

    public int getState_id() {
        return state_id;
    }

    public void setState_id(int state_id) {
        this.state_id = state_id;
    }

    public int getLocal_id() {
        return local_id;
    }

    public void setLocal_id(int local_id) {
        this.local_id = local_id;
    }

    public int getFarm_group_id() {
        return farm_group_id;
    }

    public void setFarm_group_id(int farm_group_id) {
        this.farm_group_id = farm_group_id;
    }

    public int getFarm_location_id() {
        return farm_location_id;
    }

    public void setFarm_location_id(int farm_location_id) {
        this.farm_location_id = farm_location_id;
    }

    public int getBank_id() {
        return bank_id;
    }

    public void setBank_id(int bank_id) {
        this.bank_id = bank_id;
    }

    public int getNumber_of_dependants() {
        return number_of_dependants;
    }

    public void setNumber_of_dependants(int number_of_dependants) {
        this.number_of_dependants = number_of_dependants;
    }

    public int getAdd_labour() {
        return add_labour;
    }

    public void setAdd_labour(int add_labour) {
        this.add_labour = add_labour;
    }

    public int getYears_of_experience() {
        return years_of_experience;
    }

    public void setYears_of_experience(int years_of_experience) {
        this.years_of_experience = years_of_experience;
    }

    public int getPrevious_training() {
        return previous_training;
    }

    public void setPrevious_training(int previous_training) {
        this.previous_training = previous_training;
    }

    public int getAccessPin() {
        return accessPin;
    }

    public void setAccessPin(int accessPin) {
        this.accessPin = accessPin;
    }

    public static Creator<FullFarmerInfoPojo> getCREATOR() {
        return CREATOR;
    }

    protected FullFarmerInfoPojo(Parcel in) {
        fname = in.readString();
        sname = in.readString();
        dob = in.readString();
        dobForPaga = in.readString();
        gender = in.readString();
        f_print = in.readString();
        phone = in.readString();
        marital_status = in.readString();
        crop_proficiency = in.readString();
        income_range = in.readString();
        pro_image = in.readString();
        pro_image_thumbnail = in.readString();
        comment = in.readString();
        acct_number = in.readString();
        land_area_farmed = in.readString();
        user_add_id = in.readInt();
        state_id = in.readInt();
        local_id = in.readInt();
        farm_group_id = in.readInt();
        farm_location_id = in.readInt();
        bank_id = in.readInt();
        number_of_dependants = in.readInt();
        add_labour = in.readInt();
        years_of_experience = in.readInt();
        previous_training = in.readInt();
        accessPin = in.readInt();
        access_pin = in.readInt();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(fname);
        dest.writeString(sname);
        dest.writeString(dob);
        dest.writeString(dobForPaga);
        dest.writeString(gender);
        dest.writeString(f_print);
        dest.writeString(phone);
        dest.writeString(marital_status);
        dest.writeString(crop_proficiency);
        dest.writeString(income_range);
        dest.writeString(pro_image);
        dest.writeString(pro_image_thumbnail);
        dest.writeString(comment);
        dest.writeString(acct_number);
        dest.writeString(land_area_farmed);
        dest.writeInt(user_add_id);
        dest.writeInt(state_id);
        dest.writeInt(local_id);
        dest.writeInt(farm_group_id);
        dest.writeInt(farm_location_id);
        dest.writeInt(bank_id);
        dest.writeInt(number_of_dependants);
        dest.writeInt(add_labour);
        dest.writeInt(years_of_experience);
        dest.writeInt(previous_training);
        dest.writeInt(accessPin);
        dest.writeInt(access_pin);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<FullFarmerInfoPojo> CREATOR = new Creator<FullFarmerInfoPojo>() {
        @Override
        public FullFarmerInfoPojo createFromParcel(Parcel in) {
            return new FullFarmerInfoPojo(in);
        }

        @Override
        public FullFarmerInfoPojo[] newArray(int size) {
            return new FullFarmerInfoPojo[size];
        }
    };
}
