package com.farmcrowdy.farmcrowdytfs.profile;

import com.farmcrowdy.farmcrowdytfs.DirectPojo.FarmGroupPojo;
import com.farmcrowdy.farmcrowdytfs.ListPojo.InventoryList;

public interface ProfileGetInterface {
    void successful(FullFarmerInfoPojo fullFarmerInfoPojo);
    void successfulInput(InputListPojo inputListPojo);
    void successGroupDesc(FarmGroupPojo farmGroupPojo);
    void failure(String error);
    void successInventoryLoaded(InventoryList inventoryList);
    void actionSuccessful();
    void actionFailed(String error);

}
