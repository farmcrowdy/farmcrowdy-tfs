package com.farmcrowdy.farmcrowdytfs.profile;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.farmcrowdy.farmcrowdytfs.R;

import java.text.DateFormat;
import java.text.Format;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class InputListAdapter extends RecyclerView.Adapter<InputListAdapter.InputListViewHolder> {
    private InputListPojo inputListPojo;

    public InputListAdapter(InputListPojo inputListPojo) {
        this.inputListPojo = inputListPojo;
    }


    @NonNull
    @Override
    public InputListViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.inpute_collected_items, viewGroup,false);
        return new InputListViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull InputListViewHolder inputListViewHolder, int i) {
        inputListViewHolder.inputName.setText(inputListPojo.getMessage().get(i).getInputName());


            inputListViewHolder.inputDate.setText(inputListPojo.getMessage().get(i).getDate());

        inputListViewHolder.inputSize.setText(String.valueOf(inputListPojo.getMessage().get(i).getFarmer_input_quantity()+" "+inputListPojo.getMessage().get(i).getUnit()));

    }

    @Override
    public int getItemCount() {
        if(inputListPojo.getMessage() !=null) return  inputListPojo.getMessage().size();
        else return 0;
    }

    public class InputListViewHolder extends RecyclerView.ViewHolder{
        TextView inputName, inputSize, inputDate;
        public InputListViewHolder(@NonNull View itemView) {
            super(itemView);
            inputName = itemView.findViewById(R.id.inputName);
            inputSize = itemView.findViewById(R.id.inputSize);
            inputDate = itemView.findViewById(R.id.inputDate);

        }
    }
}
