package com.farmcrowdy.farmcrowdytfs.profile;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.farmcrowdy.farmcrowdytfs.DirectPojo.FarmGroupPojo;
import com.farmcrowdy.farmcrowdytfs.DirectPojo.FingerprintDecidePojo;
import com.farmcrowdy.farmcrowdytfs.ListPojo.InventoryList;
import com.farmcrowdy.farmcrowdytfs.MainActivity;
import com.farmcrowdy.farmcrowdytfs.R;
import com.farmcrowdy.farmcrowdytfs.ViewPhotoActivity;
import com.farmcrowdy.farmcrowdytfs.farm_monitoring.MonitoringMainActivity;
import com.farmcrowdy.farmcrowdytfs.updateProfile.ListOfUpdatablesActivity;
import com.fgtit.reader.BluetoothReaderTest;
import com.google.android.gms.location.LocationRequest;
import com.rafakob.floatingedittext.FloatingEditText;
import com.squareup.picasso.Picasso;
import com.vishalsojitra.easylocation.EasyLocationAppCompatActivity;
import com.vishalsojitra.easylocation.EasyLocationRequest;
import com.vishalsojitra.easylocation.EasyLocationRequestBuilder;

import java.util.ArrayList;
import java.util.Random;

import de.hdodenhof.circleimageview.CircleImageView;
import im.delight.android.location.SimpleLocation;

public class ProfileActivity extends EasyLocationAppCompatActivity implements ProfileGetInterface, View.OnClickListener {
    String cropType;
    int farmerId;
    ProfileStuffPresenter profileStuffPresenter;
    CircleImageView profileImage;
    TextView profileName, farmGroupName, cropTypeAnswer, cropTypeAnswer2, landSize, innerProfileFarmGroup, startDateAnswer;
    ProgressBar progressBar;
    private static final int REQUEST_PHONE_CALL = 300;
    FloatingEditText giveInputEditText, harvestQuantityEdit, harvestCommentEdit, harvestNumOfBirdsEdit, harvestNumOfMortEdit;


    ArrayList<String> inventoryListString = new ArrayList<>();
    ArrayList<String> inventoryUnits = new ArrayList<>();
    ArrayList<Integer> inventoryListInteger = new ArrayList<>();
    String initlat="loading", initlong="loading";

    DialogInterface dialogInterfacer;

    int inputId;
    String plantingStageString, harvestQuantity, farmType, typeSpinnerString, groupString;

    ProgressDialog progressBarAction;
    FullFarmerInfoPojo fullFarmerInfoPojo;
    TextView fingerPrintGiven;
    SimpleLocation location;
    String popularLat, popularLong;
    ProgressDialog activateProgressDialog;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.profile_activity_layout);

        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitleTextColor(Color.WHITE);

        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        cropType = getIntent().getExtras().getString("data");
        farmerId = getIntent().getExtras().getInt("id", 1);

        profileStuffPresenter = new ProfileStuffPresenterImpl(this);
        profileStuffPresenter.getFarmerInfo(farmerId);
        profileStuffPresenter.getAllInputs();

        profileImage = findViewById(R.id.profile_image_id);
        profileName = findViewById(R.id.profile_farmer_name);
        farmGroupName = findViewById(R.id.profile_farm_group);
        cropTypeAnswer = findViewById(R.id.cropTypeAnswer);
        cropTypeAnswer2 = findViewById(R.id.cropTypeAnswer2);
        landSize = findViewById(R.id.sizeAllocatedAnswer);
        innerProfileFarmGroup = findViewById(R.id.inner_profile_farmGroup);
        progressBar = findViewById(R.id.pip);
        startDateAnswer = findViewById(R.id.startDateAnswer);
        fingerPrintGiven = findViewById(R.id.fingerPrintGiven);

        activateProgressDialog = new ProgressDialog(this);
        findViewById(R.id.ops1).setOnClickListener(this);
        findViewById(R.id.ops2).setOnClickListener(this);
        findViewById(R.id.ops3).setOnClickListener(this);
        findViewById(R.id.ops4).setOnClickListener(this);

        findViewById(R.id.flag_farmer).setOnClickListener(this);

        findViewById(R.id.viewDetails).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ProfileActivity.this, ListOfUpdatablesActivity.class);
                intent.putExtra("pojo", fullFarmerInfoPojo);
                intent.putExtra("id", farmerId);
                startActivity(intent);
            }
        });

        location = new SimpleLocation(this);


        progressBarAction = new ProgressDialog(this);
        progressBarAction.setMessage("Sending information to the admin, please wait");
        progressBarAction.setCanceledOnTouchOutside(false);
        progressBarAction.setCancelable(false);
        findWeatherWithNeccesaryChecks();

        LocationRequest locationRequest = new LocationRequest()
                .setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY)
                .setInterval(5000)
                .setFastestInterval(5000);
        EasyLocationRequest easyLocationRequest = new EasyLocationRequestBuilder()
                .setLocationRequest(locationRequest)
                .setFallBackToLastLocationTime(3000)
                .build();
        requestSingleLocationFix(easyLocationRequest);


    }
    @Override
    public void onLocationPermissionGranted() {
        showToast("Location permission granted");
    }

    private void showToast(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onLocationPermissionDenied() {
        showToast("Location permission denied");
    }

    @Override
    public void onLocationReceived(Location location) {
        initlat = String.valueOf(location.getLatitude());
        initlong = String.valueOf(location.getLongitude());
      //  showToast(location.getProvider() + "," + location.getLatitude() + "," + location.getLongitude());
    }

    @Override
    public void onLocationProviderEnabled() {
        showToast("Location services are now ON");
    }

    @Override
    public void onLocationProviderDisabled() {
        showToast("Location services are still Off");
    }

    @Override
    public void successful(final FullFarmerInfoPojo fullFarmerInfoPojo) {
        this.fullFarmerInfoPojo = fullFarmerInfoPojo;
        LinearLayout linearLayout = findViewById(R.id.popo);
        progressBar.setVisibility(View.GONE);
        linearLayout.setVisibility(View.VISIBLE);
        Picasso.with(this).load("https://www.farmcrowdy.com/farmapp/assets/images/farmerpix/" + fullFarmerInfoPojo.getPro_image()).placeholder(R.drawable.ic_burst_mode_grey_24dp).into(profileImage);

        //  profileImage.setImageURI(Uri.parse("https://www.farmcrowdy.com/farmapp/assets/images/farmerpix/"+fullFarmerInfoPojo.getPro_image()));
        profileName.setText(fullFarmerInfoPojo.getFname() + " " + fullFarmerInfoPojo.getSname());
        cropType = fullFarmerInfoPojo.getCrop_proficiency();
        startDateAnswer.setText(fullFarmerInfoPojo.getMarital_status());
        cropTypeAnswer.setText(fullFarmerInfoPojo.getCrop_proficiency());
        cropTypeAnswer2.setText(fullFarmerInfoPojo.getPhone());
        if(TextUtils.getTrimmedLength(cropTypeAnswer2.getText()) == 11) {
            findViewById(R.id.tapToCall).setOnClickListener(this);
            cropTypeAnswer2.setOnClickListener(this);
        }

        landSize.setText(fullFarmerInfoPojo.getLand_area_farmed() + "acres");
        profileStuffPresenter.getGroupFromId(fullFarmerInfoPojo.getFarm_group_id());
        if(fullFarmerInfoPojo.getF_print().isEmpty()) {
            fingerPrintGiven.setText("Not yet enrolled");
            fingerPrintGiven.setActivated(false);
            fingerPrintGiven.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent ins = new Intent(ProfileActivity.this, BluetoothReaderTest.class);
                    ins.putExtra("data", new FingerprintDecidePojo(0,farmerId,"update", 1));
                    startActivityForResult(ins, 320);
                }
            });
        }
        else {
            fingerPrintGiven.setText("Fingerprint enrolled");
            fingerPrintGiven.setActivated(true);
            fingerPrintGiven.setClickable(false);
        }

        profileImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ProfileActivity.this, ViewPhotoActivity.class);
                intent.putExtra("data", "https://www.farmcrowdy.com/farmapp/assets/images/farmerpix/" + fullFarmerInfoPojo.getPro_image());
                startActivity(intent);
            }
        });

        findViewById(R.id.activateMobileApp).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                handlePIN();
            }
        });

    }

    @Override
    public void successfulInput(InputListPojo inputListPojo) {

        for (int i = 0; i < inputListPojo.getMessage().size(); i++) {
            inputListPojo.getMessage().get(i).setInputName(inventoryListString.get
                    (inventoryListInteger.indexOf(inputListPojo.getMessage().get(i).getFarmer_input_id())));

            try {
                inputListPojo.getMessage().get(i).setUnit(inventoryUnits.get
                        (inventoryListInteger.indexOf(inputListPojo.getMessage().get(i).getFarmer_input_id())));
            } catch (Exception e) {
                inputListPojo.getMessage().get(i).setUnit("units");
            }
        }

        RecyclerView recyclerView = findViewById(R.id.inputCollectedRecycler);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setHasFixedSize(true);
        InputListAdapter inputListAdapter = new InputListAdapter(inputListPojo);
        recyclerView.setAdapter(inputListAdapter);
    }

    @Override
    public void successGroupDesc(FarmGroupPojo farmGroupPojo) {
        farmGroupName.setText(farmGroupPojo.getGroup_name());
        innerProfileFarmGroup.setText(farmGroupPojo.getGroup_name());
    }

    @Override
    public void failure(String error) {
        Toast.makeText(this, "Something went wrong due to " + error, Toast.LENGTH_SHORT).show();

    }


    @Override
    public void successInventoryLoaded(InventoryList inventoryList) {
        for (int i = 0; i < inventoryList.getMessage().size(); i++) {
            inventoryListString.add(inventoryList.getMessage().get(i).getInput_name());
            inventoryUnits.add(inventoryList.getMessage().get(i).getInput_unit());
            inventoryListInteger.add(inventoryList.getMessage().get(i).getInput_id());
        }
        profileStuffPresenter.getInputs(farmerId);

    }

    @Override
    public void actionSuccessful() {
        progressBarAction.dismiss();
        progressBarAction.cancel();
        Toast.makeText(this, "Successfully done", Toast.LENGTH_SHORT).show();

    }

    @Override
    public void actionFailed(String error) {
        if(error.contains("No address")) error = "No internet connection found";
        progressBarAction.dismiss();
        progressBarAction.cancel();
        // Toast.makeText(this, "Information Successfully sent to admin", Toast.LENGTH_SHORT).show();
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
        alertDialog.setTitle("Failed");
        alertDialog.setMessage("Sending this action to the admin failed due to: " + error);
        alertDialog.show();
    }


    void handlePIN() {
        progressBarAction.setMessage("Activating mobile app for farmer, please wait...");
        progressBarAction.setCanceledOnTouchOutside(false);
        progressBarAction.setCancelable(false);
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Activate mobile app for farmer");
        LayoutInflater inflater = this.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.activate_app_layout, null);
        builder.setView(dialogView);

       TextView pinText = dialogView.findViewById(R.id.donePin);
        int generatedPIN = new Random().nextInt(9999);
       pinText.setText(String.valueOf(fullFarmerInfoPojo.getAccess_pin()));


        builder.setPositiveButton("Activate Mobile now", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterfacer = dialogInterface;
                progressBarAction.show();
                profileStuffPresenter.activatePin(fullFarmerInfoPojo);

            }
        });

        builder.show();
    }

    void handleCollectPoultryHarvest() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Collect poultry harvest from farmer");
        LayoutInflater inflater = this.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.poultry_harvest_collection_layout, null);
        builder.setView(dialogView);
        harvestQuantityEdit = dialogView.findViewById(R.id.po_weight);
        harvestNumOfBirdsEdit = dialogView.findViewById(R.id.po_numberOfBirds);
        harvestNumOfMortEdit = dialogView.findViewById(R.id.po_numOfMortality);
        harvestCommentEdit = dialogView.findViewById(R.id.po_comments);

        builder.setPositiveButton("Record harvest", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterfacer = dialogInterface;
                Intent ins = new Intent(ProfileActivity.this, BluetoothReaderTest.class);
                ins.putExtra("data", new FingerprintDecidePojo(1,farmerId, fullFarmerInfoPojo.getF_print(), 3));
                startActivityForResult(ins, 340);


            }
        });

        builder.show();
    }

    void handleCollectCropHarvest() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Collect crop harvest from farmer");
        LayoutInflater inflater = this.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.harvest_collection_layout, null);
        builder.setView(dialogView);

        harvestQuantityEdit = dialogView.findViewById(R.id.collected_id);
        harvestCommentEdit = dialogView.findViewById(R.id.comments_harvest_id);
        Spinner harvestQuantitySpinner = dialogView.findViewById(R.id.harvest_quantity_units);
        Spinner farmTypeSpinner = dialogView.findViewById(R.id.harvest_farm_type);

        farmTypeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                farmType = adapterView.getItemAtPosition(i).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        harvestQuantitySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                harvestQuantity = adapterView.getItemAtPosition(i).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        builder.setPositiveButton("Record Harvest", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                if(harvestQuantityEdit.getText().isEmpty()) {
                    Toast.makeText(ProfileActivity.this, "Enter harvest quantity", Toast.LENGTH_SHORT).show();
                    return;
                }
                else if(harvestCommentEdit.getText().isEmpty())  {
                    harvestCommentEdit.setText("");
                }
                dialogInterfacer = dialogInterface;
                Intent ins = new Intent(ProfileActivity.this, BluetoothReaderTest.class);
                ins.putExtra("data", new FingerprintDecidePojo(1,farmerId, fullFarmerInfoPojo.getF_print(), 2));
                startActivityForResult(ins, 340);



            }
        });

        builder.show();

    }

    void handleAssignInputToFarmer() {

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Give farmer inputs");
        LayoutInflater inflater = this.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.assign_input_item_to_farmer_layout, null);
        builder.setView(dialogView);


        giveInputEditText = dialogView.findViewById(R.id.quantityEditText);
        Spinner inputs = dialogView.findViewById(R.id.inputInputsSpinner);
        Spinner plantingStageSpinner = dialogView.findViewById(R.id.planting_stage);


        ArrayAdapter<String> dataAdapteFarmGroup = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, inventoryListString);
        dataAdapteFarmGroup.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        inputs.setAdapter(dataAdapteFarmGroup);

        inputs.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                inputId = inventoryListInteger.get(i);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        plantingStageSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                plantingStageString = adapterView.getItemAtPosition(i).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        builder.setPositiveButton("Assign item to farmer", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

                if(giveInputEditText.getText().isEmpty())  {
                    Toast.makeText(ProfileActivity.this, "Enter harvest quantity", Toast.LENGTH_SHORT).show();
                    return;
                }

                dialogInterfacer = dialogInterface;

                Intent ins = new Intent(ProfileActivity.this, BluetoothReaderTest.class);
                ins.putExtra("data", new FingerprintDecidePojo(1,farmerId, fullFarmerInfoPojo.getF_print(), 1));
                startActivityForResult(ins, 340);


            }
        });


        builder.show();
    }

    void assignFarmLandToFarmer() {

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Give farmer inputs");
        LayoutInflater inflater = this.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.assign_land_layout, null);
        builder.setView(dialogView);

        final FloatingEditText landAreaEdit = dialogView.findViewById(R.id.landAreaProfile_id);

        final FloatingEditText latitudeEdit = dialogView.findViewById(R.id.latitude_id);
        final FloatingEditText longitudeEdit = dialogView.findViewById(R.id.longitude_id);
        latitudeEdit.setText(initlat);
        longitudeEdit.setText(initlong);

        /*Location loc1 = new Location("");
        loc1.setLatitude(lat1);
        loc1.setLongitude(lon1);

        Location loc2 = new Location("");
        loc2.setLatitude(lat2);
        loc2.setLongitude(lon2);

        float distanceInMeters = loc1.distanceTo(loc2);
        */
        final FloatingEditText latitudeEditB = dialogView.findViewById(R.id.latitude_id_b);
        final FloatingEditText longitudeEditB = dialogView.findViewById(R.id.longitude_id_b);

        final FloatingEditText latitudeEditC = dialogView.findViewById(R.id.latitude_id_c);
        final FloatingEditText longitudeEdiC = dialogView.findViewById(R.id.longitude_id_c);

        final FloatingEditText latitudeEditD = dialogView.findViewById(R.id.latitude_id_d);
        final FloatingEditText longitudeEditD = dialogView.findViewById(R.id.longitude_id_d);


        //latitudeEdit.setText("dadfa");
        //longitudeEdit.setText(popularLong);


        Spinner typeSpinner = dialogView.findViewById(R.id.dsp);

        typeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                typeSpinnerString = adapterView.getItemAtPosition(i).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        builder.setPositiveButton("Assign Land", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                progressBarAction.show();
                profileStuffPresenter.assignLand(farmerId, landAreaEdit.getText(), "", latitudeEdit.getText(), longitudeEdit.getText(),latitudeEditB.getText(), longitudeEditB.getText(),
                        latitudeEditC.getText(), longitudeEdiC.getText(), latitudeEditD.getText(), longitudeEditD.getText());
                dialogInterface.dismiss();
            }
        });
        builder.show();

    }

    void flagFarmer() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Flag farmer");
        LayoutInflater inflater = this.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.flag_farmer, null);
        builder.setView(dialogView);

        final FloatingEditText flagEdit = dialogView.findViewById(R.id.flagComment);
        final CheckBox checkBox = dialogView.findViewById(R.id.flag_check);

        builder.setPositiveButton("Confirm flag", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                if (checkBox.isChecked())
                    profileStuffPresenter.flagFarmer(farmerId, flagEdit.getText());
                else
                    Toast.makeText(ProfileActivity.this, "Please check box to confirm flag", Toast.LENGTH_SHORT).show();
            }
        });
        builder.show();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ops3:
                handleAssignInputToFarmer();
                break;
            case R.id.ops4:
                if (cropType.equals("poultry")) handleCollectPoultryHarvest();
                else handleCollectCropHarvest();
                break;
            case R.id.ops1:
                assignFarmLandToFarmer();
                break;
            case R.id.flag_farmer:
                flagFarmer();
                break;

            case R.id.ops2:
                Intent intent = new Intent(this, MonitoringMainActivity.class);
                intent.putExtra("data", cropType);
                intent.putExtra("id", farmerId);
                intent.putExtra("pojo", fullFarmerInfoPojo);
                intent.putExtra("group", farmGroupName.getText());
                startActivity(intent);
                break;
            case R.id.cropTypeAnswer2:
                caller();
                break;

            case R.id.tapToCall:
                caller();
                break;


        }
    }
    void caller() {
        try {
            Intent callIntent = new Intent(Intent.ACTION_CALL);
            callIntent.setData(Uri.parse("tel:" + cropTypeAnswer2.getText().toString()));
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                ActivityCompat.requestPermissions(ProfileActivity.this, new String[]{Manifest.permission.CALL_PHONE},REQUEST_PHONE_CALL);
                return;

            }
            startActivity(callIntent);
        } catch (ActivityNotFoundException activityException) {
            Log.e("Calling a Phone Number", "Call failed", activityException);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == 340) {
            if(resultCode == RESULT_OK) {
                int result = data.getIntExtra("type", 0);
                switch (result) {
                    case 0:
                        break;

                    case 1:
                        progressBarAction.show();
                        profileStuffPresenter.giveInputsToFamer(farmerId, inputId, plantingStageString, Integer.valueOf(giveInputEditText.getText().toString()));
                        dialogInterfacer.dismiss();
                        break;
                    case 2:
                        progressBarAction.show();
                        profileStuffPresenter.recordHarvest(farmerId, "0", 0, Integer.valueOf(harvestQuantityEdit.getText().toString()), harvestCommentEdit.getText().toString(), farmType, harvestQuantity);
                        dialogInterfacer.dismiss();
                        break;

                    case 3:
                        progressBarAction.show();
                        profileStuffPresenter.recordHarvest(farmerId, harvestQuantityEdit.getText(), Integer.valueOf(harvestNumOfBirdsEdit.getText().toString()),
                                Integer.valueOf(harvestNumOfMortEdit.getText().toString()), harvestCommentEdit.getText().toString(), cropType, "kg");
                        dialogInterfacer.dismiss();
                        break;
                }
            }
        }
        else if (requestCode == 320) {
            if(resultCode == RESULT_OK) {
            int result = data.getIntExtra("type", 0);
            switch (result) {
                case 1: fingerPrintGiven.setText("Fingerprint enrolled");
                    fingerPrintGiven.setActivated(true);
                    fingerPrintGiven.setClickable(false);
                    break;
            }
            }
        }
    }

    @SuppressLint("MissingPermission")
    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case REQUEST_PHONE_CALL: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Intent callIntent = new Intent(Intent.ACTION_CALL);
                    callIntent.setData(Uri.parse("tel:" + cropTypeAnswer2.getText().toString()));
                    startActivity(callIntent);
                }
                else
                {

                }
                return;
            }

            case 440: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // location-related task you need to do.
                    if (ContextCompat.checkSelfPermission(this,
                            Manifest.permission.ACCESS_FINE_LOCATION)
                            == PackageManager.PERMISSION_GRANTED) {

                        //Request location updates:
                        //locationManager.requestLocationUpdates(provider, 400, 1, this);
                        final double latitude = location.getLatitude();
                        final double longitude = location.getLongitude();

                         fetchWeather(latitude, longitude);

                    }

                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.

                }
                return;
            }
        }
    }

    void findWeatherWithNeccesaryChecks() {
        if (!location.hasLocationEnabled()) {
            // ask the user to enable location access
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage("Kindly turn on your location at your settings");
            builder.setTitle("Location settings");
            builder.setCancelable(false);
            builder.setPositiveButton("Open settings", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    SimpleLocation.openSettings(ProfileActivity.this);
                }
            });
            builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    finish();
                }
            });
            builder.show();

        }
        else findLocation1();
    }

    void findLocation1() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION)
                    == PackageManager.PERMISSION_GRANTED && checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION)
                    == PackageManager.PERMISSION_GRANTED) {

                final double latitude = location.getLatitude();
                final double longitude = location.getLongitude();
                Log.d("OOK", "lat: "+latitude +" with longi:"+longitude);

                fetchWeather(latitude, longitude);

            }
            else {
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION},
                        440);
            }
        }
    }

    void fetchWeather(double lat, double longi) {
        Log.d("OOK", "HERE OOO");
        popularLat = String.valueOf(lat);
        popularLong = String.valueOf(longi);
    }

}
