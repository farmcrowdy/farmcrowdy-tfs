package com.farmcrowdy.farmcrowdytfs.profile;

/**
 * Created by Oluwatobi on 5/28/2018.
 */

public class InputPojo {
    int farmer_input_id,farmer_input_quantity;
    String date, unit, inputName;
    String farmername;

    public String getFarmername() {
        return farmername;
    }

    public int getFarmer_input_id() {
        return farmer_input_id;
    }

    public int getFarmer_input_quantity() {
        return farmer_input_quantity;
    }

    public String getDate() {
        return date;
    }

    public void setFarmer_input_id(int farmer_input_id) {
        this.farmer_input_id = farmer_input_id;
    }

    public void setFarmer_input_quantity(int farmer_input_quantity) {
        this.farmer_input_quantity = farmer_input_quantity;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getInputName() {
        return inputName;
    }

    public void setInputName(String inputName) {
        this.inputName = inputName;
    }
}
