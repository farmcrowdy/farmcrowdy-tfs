package com.farmcrowdy.farmcrowdytfs.profile;

/**
 * Created by Oluwatobi on 5/25/2018.
 */

public interface ProfileStuffPresenter {
    void getFarmerInfo(int farmerId);
    void getGroupFromId(int groupId);
    void getInputs(int farmerId);
    void getAllInputs();
    void giveInputsToFamer(int farmerId, int inputId, String plantingStage, int inputQuantity);
    void recordHarvest (int farmerId, String weightOfBird, int mortalityNumb, int numberOfBirds, String comments, String cropType, String measurement);
    void assignLand(int farmerId, String landArea, String unit, String lat, String longi, String lat_b, String longi_b, String lat_c, String longi_c, String lat_d, String longi_d );
    void flagFarmer(int farmerId, String comment);
    void activatePin(FullFarmerInfoPojo fullFarmerInfoPojo);
}
