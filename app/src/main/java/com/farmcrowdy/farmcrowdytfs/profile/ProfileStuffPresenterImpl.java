package com.farmcrowdy.farmcrowdytfs.profile;

import android.text.TextUtils;
import android.util.Log;

import com.farmcrowdy.farmcrowdytfs.ApplicationInstance;
import com.farmcrowdy.farmcrowdytfs.DirectPojo.FarmGroupPojo;
import com.farmcrowdy.farmcrowdytfs.DirectPojo.PagaResponsePojo;
import com.farmcrowdy.farmcrowdytfs.ListPojo.FarmGroupSinglePojo;
import com.farmcrowdy.farmcrowdytfs.ListPojo.InventoryList;
import com.farmcrowdy.farmcrowdytfs.MyEndpoint;
import com.farmcrowdy.farmcrowdytfs.addFarmer.PagaAddBodyPojo;
import com.farmcrowdy.farmcrowdytfs.engine.TinyDB;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.IOException;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.util.Date;
import java.util.Random;

import okhttp3.Interceptor;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

public class ProfileStuffPresenterImpl implements ProfileStuffPresenter {
    ProfileGetInterface profileGetInterface;

    public ProfileStuffPresenterImpl(ProfileGetInterface profileGetInterface) {
        this.profileGetInterface = profileGetInterface;
    }

    @Override
    public void getFarmerInfo(final int farmerId) {
        MyEndpoint myEndpoint = ApplicationInstance.getRetrofit().create(MyEndpoint.class);
        Call<ListFullfarmer> call = myEndpoint.getOneFarmerInfo(farmerId);
        call.enqueue(new Callback<ListFullfarmer>() {
            @Override
            public void onResponse(Call<ListFullfarmer> call, Response<ListFullfarmer> response) {
                if(response.isSuccessful()){
                    profileGetInterface.successful(response.body().getMessage());
                }
                else profileGetInterface.failure(response.message() +"from farmerInfo");
            }

            @Override
            public void onFailure(Call<ListFullfarmer> call, Throwable t) {
                profileGetInterface.failure("farmeriD: "+farmerId );
            }


        });
    }

    @Override
    public void getGroupFromId(int groupId) {
        MyEndpoint myEndpoint = ApplicationInstance.getRetrofit().create(MyEndpoint.class);
        Call<FarmGroupSinglePojo> call = myEndpoint.getFarmGroupById(groupId);
        call.enqueue(new Callback<FarmGroupSinglePojo>() {
            @Override
            public void onResponse(Call<FarmGroupSinglePojo> call, Response<FarmGroupSinglePojo> response) {
                if(response.isSuccessful()) profileGetInterface.successGroupDesc(response.body().getMessage() );
                else profileGetInterface.failure(response.message() +"from groupInfo");
            }

            @Override
            public void onFailure(Call<FarmGroupSinglePojo> call, Throwable t) {
                profileGetInterface.failure(t.getMessage()+"from groupInfo Throwable" );
            }
        });
    }

    @Override
    public void getInputs(int farmerId) {
            MyEndpoint myEndpoint = ApplicationInstance.getRetrofit().create(MyEndpoint.class);
            Call<InputListPojo> call = myEndpoint.getFarmersInputById(farmerId);
            call.enqueue(new Callback<InputListPojo>() {
                @Override
                public void onResponse(Call<InputListPojo> call, Response<InputListPojo> response) {
                    if(response.isSuccessful()) profileGetInterface.successfulInput(response.body());
                    else profileGetInterface.failure(response.message()+"from FarmersInputInfo");
                }

                @Override
                public void onFailure(Call<InputListPojo> call, Throwable t) {
                    profileGetInterface.failure(t.getMessage()+"from FarmerINputInfo Throwable" );
                }
            });
    }

    @Override
    public void getAllInputs() {
        MyEndpoint myEndpoint = ApplicationInstance.getRetrofit().create(MyEndpoint.class);
        Call<InventoryList> inventoryListCall = myEndpoint.getAllInputInventory();
        inventoryListCall.enqueue(new Callback<InventoryList>() {
            @Override
            public void onResponse(Call<InventoryList> call, Response<InventoryList> response) {
                if(response.isSuccessful()) profileGetInterface.successInventoryLoaded(response.body());
                else profileGetInterface.failure(response.message() +"from AllInputsInfo");
            }

            @Override
            public void onFailure(Call<InventoryList> call, Throwable t) {
                profileGetInterface.failure(t.getMessage() +"from AllInputsInfo Throwable" );
            }
        });
    }

    @Override
    public void giveInputsToFamer(int farmerId,int inputId, String plantingStage, int inputQuantity) {
        MyEndpoint myEndpoint = ApplicationInstance.getRetrofit().create(MyEndpoint.class);
        RequestBody plantingStageBody  =  RequestBody.create(MediaType.parse("text/plain"), plantingStage);


        int tfs_id = new TinyDB(ApplicationInstance.getContext()).getInt("user_id", 1);
        Call<ResponseBody> call = myEndpoint.assignInputToFarmer(farmerId, farmerId, tfs_id,inputId,inputQuantity,plantingStageBody);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if(response.isSuccessful()) {
                    profileGetInterface.actionSuccessful();
                }
                else {
                    profileGetInterface.actionFailed(response.message());
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                    profileGetInterface.actionFailed(t.getMessage());
            }
        });
    }

    @Override
    public void recordHarvest(int farmerId, String weightOfBird, int mortalityNumb, int numberOfBirds, String comments, String cropType, String measurement) {
        MyEndpoint myEndpoint = ApplicationInstance.getRetrofit().create(MyEndpoint.class);
        int tfs_id = new TinyDB(ApplicationInstance.getContext()).getInt("user_id", 1);
        RequestBody weightOfBirdBody  =  RequestBody.create(MediaType.parse("text/plain"), weightOfBird);
        RequestBody commentBody  =  RequestBody.create(MediaType.parse("text/plain"), comments);
        RequestBody cropyTypeBody  =  RequestBody.create(MediaType.parse("text/plain"), cropType);
        RequestBody unitsBody = RequestBody.create(MediaType.parse("text/plain"), measurement);


        Call<ResponseBody> call = myEndpoint.sendHarvestCollection(tfs_id,farmerId,weightOfBirdBody,mortalityNumb,cropyTypeBody,numberOfBirds,unitsBody, commentBody);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if(response.isSuccessful()) profileGetInterface.actionSuccessful();
                else profileGetInterface.actionFailed(response.message());
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                profileGetInterface.actionFailed(t.getMessage());
            }
        });
    }

    @Override
    public void assignLand(int farmerId, String landArea, String unit, String lat, String longi, String lat_b, String longi_b, String lat_c, String longi_c, String lat_d, String longi_d ) {
        MyEndpoint myEndpoint = ApplicationInstance.getRetrofit().create(MyEndpoint.class);
        RequestBody landAreaBody = RequestBody.create(MediaType.parse("text/plain"), landArea);

        if(lat.isEmpty() || longi.isEmpty()) {
            profileGetInterface.actionFailed("Coordinates are incorrect, please check");
            return;
        }
        RequestBody latlongBodyA = RequestBody.create(MediaType.parse("text/plain"), lat+","+longi);
        //RequestBody longiBody = RequestBody.create(MediaType.parse("text/plain"), longi);

        if(lat_b.isEmpty()) lat_b = "0";
        if(longi_b.isEmpty()) longi_b = "0";

        RequestBody latlongBody_B = RequestBody.create(MediaType.parse("text/plain"), lat_b+","+longi_b);
       // RequestBody longiBody_B = RequestBody.create(MediaType.parse("text/plain"), longi_b);

        if(lat_c.isEmpty()) lat_c = "0";
        if(longi_c.isEmpty()) longi_c = "0";
        RequestBody latlongBody_C = RequestBody.create(MediaType.parse("text/plain"), lat_c+","+longi_c);
        //RequestBody longiBody_C = RequestBody.create(MediaType.parse("text/plain"), longi_c);

        if(lat_d.isEmpty()) lat_d = "0";
        if(longi_d.isEmpty()) longi_d = "0";

        RequestBody latBody_D = RequestBody.create(MediaType.parse("text/plain"), lat_d+","+longi_d);
        //RequestBody longiBody_D = RequestBody.create(MediaType.parse("text/plain"), longi_d);

        int tfs_id = new TinyDB(ApplicationInstance.getContext()).getInt("user_id", 1);


        Call<ResponseBody> call = myEndpoint.assignLand(farmerId,tfs_id,farmerId,landAreaBody,latlongBodyA,latlongBody_B, latlongBody_C, latBody_D);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if(response.isSuccessful()) profileGetInterface.actionSuccessful();
                else profileGetInterface.actionFailed(response.message());
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                    profileGetInterface.actionFailed(t.getMessage());
            }
        });
    }

    @Override
    public void flagFarmer(int farmerId, String comment) {
        MyEndpoint myEndpoint = ApplicationInstance.getRetrofit().create(MyEndpoint.class);
        RequestBody commentBody = RequestBody.create(MediaType.parse("text/plain"), comment);
        Call<ResponseBody> call = myEndpoint.sendFlagFarmer(farmerId, commentBody);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if(response.isSuccessful()) profileGetInterface.actionSuccessful();
                else profileGetInterface.actionFailed(response.message());
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                profileGetInterface.actionFailed(t.getMessage());
            }
        });
    }

    @Override
    public void activatePin(FullFarmerInfoPojo fullFarmerInfoPojo) {
    doWalletRegistration(fullFarmerInfoPojo);
    }


    String sortPhoneNumberExtension(String initialNumber) {
        String finalString = "";
        if(initialNumber.substring(0,1).equals("0") ) {
            finalString = "+234"+initialNumber.substring(1);
        }
        return  finalString;
    }
    String generateRandomReferenceNumber(String firstname) {
        String initial = "Farmcrowdy";
        int middle = new Random().nextInt(9999);
        String lastOne = firstname.substring(2);
        long timing = new Date().getTime();
        return String.valueOf(initial+middle+""+timing);
    }
    void doWalletRegistration(FullFarmerInfoPojo fullFarmerInfoPojo) {
        try {
            String referenceNumber = generateRandomReferenceNumber(fullFarmerInfoPojo.getFname());
            String customerPrincipal = sortPhoneNumberExtension(fullFarmerInfoPojo.getPhone());
            String customerCredentials = "Farmcrowdy"+fullFarmerInfoPojo.getAccessPin();
            String customerFirstName = fullFarmerInfoPojo.getFname();
            String customerLastName = fullFarmerInfoPojo.getSname();
            String customerDateOfBirth = fullFarmerInfoPojo.getDobForPaga();

            String sBuilder = referenceNumber + customerPrincipal + customerCredentials + ApplicationInstance.hashKey;

            Log.d("HASH", "has is : "+ sBuilder);


            final String hmac = hashSHA512(sBuilder).toString();
            Log.d("HASH", "has is : "+ hmac);
            Gson gson = new GsonBuilder()
                    .setLenient()
                    .create();

            OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
            httpClient.addInterceptor(new Interceptor() {
                @Override
                public okhttp3.Response intercept(Interceptor.Chain chain) throws IOException {
                    Request original = chain.request();

                    // Request customization: add request headers
                    Request.Builder requestBuilder = original.newBuilder()
                            .addHeader("Content-Type", "application/json")
                            .addHeader("principal", ApplicationInstance.pagaPrincipal)
                            .addHeader("credentials",ApplicationInstance.pagaCredentials)
                            .addHeader("hash", hmac);

                    Request request = requestBuilder.build();

                    return chain.proceed(request);
                }
            });

            OkHttpClient client = httpClient.build();
            Retrofit retrofit = new Retrofit.Builder()
                    .client(client)
                    .addConverterFactory(ScalarsConverterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .baseUrl("https://mypaga.com/")
                    .build();



            MyEndpoint myEndpoint = retrofit.create(MyEndpoint.class);
            // Call<PagaResponsePojo>  caller = myEndpoint.buyAirtimePaga(new PagaBuyAirtime(referenceNumber, "en"));
            Call<PagaResponsePojo> caller = myEndpoint.registerFarmerOnPaga(new PagaAddBodyPojo(referenceNumber,customerPrincipal,customerCredentials,customerFirstName,customerLastName,customerDateOfBirth));
            caller.enqueue(new Callback<PagaResponsePojo>() {
                @Override
                public void onResponse(Call<PagaResponsePojo> call, Response<PagaResponsePojo> response) {
                    if(response.isSuccessful()) {
                        if(response.body().getResponseCode() == -1) {
                            profileGetInterface.actionSuccessful();
                        }
                        else {
                            profileGetInterface.actionFailed("Already activated: "+response.body().getMessage());
                        }

                    }
                    else  Log.d("pagatech", "not good still...");
                    //withPhotoInterface.failure("error due to: "+ response.code());
                }

                @Override
                public void onFailure(Call<PagaResponsePojo> call, Throwable t) {
                    profileGetInterface.actionFailed("Something went wrong due to: "+ t.getMessage());
                }
            });
        }catch (Exception e) {
            e.printStackTrace();
            Log.e("Pagatech", "error exception due to "+e.getMessage());
            profileGetInterface.actionFailed("Internet connection not found");
        }


    }


    public static Object hashSHA512(String message) {

        try {
            String toReturn = null;
            try {
                MessageDigest digest = MessageDigest.getInstance("SHA-512");
                digest.reset();
                digest.update(message.getBytes("utf8"));
                toReturn = String.format("%040x", new BigInteger(1, digest.digest()));
            } catch (Exception e) {
                e.printStackTrace();
            }

            return toReturn;

        } catch(Exception e) {
            return null;
        }
    }


}
