package com.farmcrowdy.farmcrowdytfs;

/**
 * Created by Oluwatobi on 5/25/2018.
 */

public interface WithPhotoInterface {
    void successful(String farmerId);
    void failure(String error);
    void uploadPhotoSuccessful(String urlHigh, String urlLow);
}
