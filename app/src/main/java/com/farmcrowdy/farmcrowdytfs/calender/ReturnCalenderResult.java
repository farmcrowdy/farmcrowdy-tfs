package com.farmcrowdy.farmcrowdytfs.calender;

import java.util.ArrayList;
import java.util.List;

public class ReturnCalenderResult {
    List<String> strings;
    ArrayList<Long> times;
    ArrayList<String> dater;

    public List<String> getStrings() {
        return strings;
    }

    public void setStrings(List<String> strings) {
        this.strings = strings;
    }

    public ArrayList<Long> getTimes() {
        return times;
    }

    public void setTimes(ArrayList<Long> times) {
        this.times = times;
    }

    public ArrayList<String> getDater() {
        return dater;
    }

    public void setDater(ArrayList<String> dater) {
        this.dater = dater;
    }
}
