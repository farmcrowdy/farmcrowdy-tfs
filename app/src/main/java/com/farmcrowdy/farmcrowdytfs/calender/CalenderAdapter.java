package com.farmcrowdy.farmcrowdytfs.calender;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.farmcrowdy.farmcrowdytfs.R;
import com.github.curioustechizen.ago.RelativeTimeTextView;

import java.util.ArrayList;
import java.util.List;

public class CalenderAdapter extends RecyclerView.Adapter<CalenderAdapter.ClassViewHolder> {
    private List<String> contentList;
    private ArrayList<Long> timeLeftList;

    public CalenderAdapter(List<String> contentList, ArrayList<Long> timeLeftList) {
        this.contentList = contentList;
        this.timeLeftList = timeLeftList;
    }

    @NonNull
    @Override
    public ClassViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.calender_my_items,viewGroup,false);
        return new ClassViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ClassViewHolder classViewHolder, int i) {
    classViewHolder.daysLeft.setReferenceTime(timeLeftList.get(i));

    classViewHolder.fullContent.setText(contentList.get(i));
        String dayOfTheWeek = (String) DateFormat.format("EEEE", timeLeftList.get(i)); // Thursday
        String day          = (String) DateFormat.format("dd",   timeLeftList.get(i)); // 20
        String monthString  = (String) DateFormat.format("MMM", timeLeftList.get(i)); // Jun
        //String monthNumber  = (String) DateFormat.format("MM",   timeLeftList.get(i)); // 06
        String year         = (String) DateFormat.format("yyyy", timeLeftList.get(i));

        classViewHolder.fullDate.setText(String.valueOf(dayOfTheWeek +", "+day +", "+monthString+" "+year));
    }

    @Override
    public int getItemCount() {
        if(contentList!=null) return contentList.size();
        else return 0;
    }

    public class ClassViewHolder extends RecyclerView.ViewHolder {
        TextView  fullContent,fullDate;
        RelativeTimeTextView daysLeft;
        public ClassViewHolder(@NonNull View itemView) {
            super(itemView);
            fullContent = itemView.findViewById(R.id.full_content);
            daysLeft = itemView.findViewById(R.id.days_remaining);
            fullDate = itemView.findViewById(R.id.fulldate);
        }
    }
}
