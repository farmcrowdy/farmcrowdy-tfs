package com.farmcrowdy.farmcrowdytfs;

import com.farmcrowdy.farmcrowdytfs.DirectPojo.FarmGroupPojo;
import com.farmcrowdy.farmcrowdytfs.DirectPojo.InventoryPojo;
import com.farmcrowdy.farmcrowdytfs.DirectPojo.MessageList;
import com.farmcrowdy.farmcrowdytfs.DirectPojo.MessagePojo;
import com.farmcrowdy.farmcrowdytfs.DirectPojo.PagaResponsePojo;
import com.farmcrowdy.farmcrowdytfs.DirectPojo.TFS_userPojo;
import com.farmcrowdy.farmcrowdytfs.ListPojo.CropListPojo;
import com.farmcrowdy.farmcrowdytfs.ListPojo.FarmGroupListPojo;
import com.farmcrowdy.farmcrowdytfs.ListPojo.FarmGroupListSearch;
import com.farmcrowdy.farmcrowdytfs.ListPojo.FarmGroupSinglePojo;
import com.farmcrowdy.farmcrowdytfs.ListPojo.FarmLocationListPojo;
import com.farmcrowdy.farmcrowdytfs.ListPojo.FarmerListPojo;
import com.farmcrowdy.farmcrowdytfs.ListPojo.GetBanksListPojo;
import com.farmcrowdy.farmcrowdytfs.ListPojo.InventoryList;
import com.farmcrowdy.farmcrowdytfs.ListPojo.LocalGovtListPojo;
import com.farmcrowdy.farmcrowdytfs.ListPojo.StateListPojo;
import com.farmcrowdy.farmcrowdytfs.addFarmer.AddFarmerResponsePojo;
import com.farmcrowdy.farmcrowdytfs.addFarmer.PagaAddBodyPojo;
import com.farmcrowdy.farmcrowdytfs.addFarmer.PagaBuyAirtime;
import com.farmcrowdy.farmcrowdytfs.farmStats.FarmStatList;
import com.farmcrowdy.farmcrowdytfs.history.HarvestHistoryLister;
import com.farmcrowdy.farmcrowdytfs.history.InputeHistoryLister;
import com.farmcrowdy.farmcrowdytfs.history.MonitoredHistoryPojo;
import com.farmcrowdy.farmcrowdytfs.history.MonitoringLister;
import com.farmcrowdy.farmcrowdytfs.profile.FullFarmerInfoPojo;
import com.farmcrowdy.farmcrowdytfs.profile.InputListPojo;
import com.farmcrowdy.farmcrowdytfs.profile.ListFullfarmer;

import java.util.ArrayList;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by Oluwatobi on 5/23/2018.
 */

public interface MyEndpoint {

    @Headers({
            "Accept: application/json",
            "Content-Type: application/json"
    })
    @POST("paga-webservices/business-rest/secured/registerCustomer")
    Call<PagaResponsePojo> registerFarmerOnPaga(@Body PagaAddBodyPojo pagaAddBodyPojo);

    @Headers({
            "Accept: application/json",
            "Content-Type: application/json"
    })
    @POST("paga-webservices/business-rest/secured/getOperationStatus")
    Call<PagaResponsePojo> buyAirtimePaga(@Body PagaBuyAirtime pagaBuyAirtime);

    /*Call<PagaResponsePojo> registerFarmerOnPaga(@Part("referenceNumber")RequestBody referenceNumber,
                                                @Part("customerPrincipal") RequestBody customerPrincipal,
                                                @Part("customerCredentials") RequestBody customerCredentials,
                                                @Part("customerFirstName") RequestBody customerFirstName,
                                                @Part("customerLastName") RequestBody customerLastName,
                                                @Part("customerDateOfBirth") RequestBody customerDateOfBirth);
                                                */



    @Multipart
    @POST("farmerprint/{id}")
    Call<ResponseBody> updateFingerPrint(@Path("id") int farmerId,
                                         @Part("f_print") RequestBody fingerprintName);

    @Multipart
    @POST("addimage")
    Call<ResponseBody> upload(
            @Part MultipartBody.Part pimage, @Part("pimage") RequestBody name
    );

    @GET("cropmonitorbytfs/{id}")
    Call<MonitoringLister> getMonitoredByTfs(@Path("id") int tfsId);

    @GET("inputByTfs/{id}")
    Call<InputeHistoryLister> getInputHistory(@Path("id") int tfsId);

    @GET("harvestByTfs/{id}")
    Call<HarvestHistoryLister> getHarvestHistory(@Path("id") int tfsId);

    @GET("messageByTfs/{tfsid}")
    Call<MessageList> getMessageByTFS(@Path("tfsid") int tfsId);

    @POST("markRead/{msgid}")
    Call<ResponseBody> getMarkResponse(@Path("msgid") int msgId);

    @GET("farmstats")
    Call<FarmStatList> getStat();

    @Multipart
    @POST("userlogin/")
    Call<LoginPojo> login(@Part("email") RequestBody email, @Part("password") RequestBody password);



    @Multipart
    @POST("user")
    Call<ResponseBody> addNewUser(@Part("email") RequestBody email, @Part("password") RequestBody password,
                                  @Part("firstname") RequestBody firstname, @Part("surname") RequestBody surname);

    @GET("user/{id}")
    Call<TFS_userPojo> getTFSInfoById(@Path("id") int id);

    @Multipart
    @POST("farmUpdate")
    Call<ResponseBody> sendFarmUpdate(@Part("tfs_id") int tfs_id,
                                      @Part("farm_groupid") int farmgroupId,
                                      @Part("subject") RequestBody subject,
                                      @Part("comment") RequestBody comment,
                                      @Part("farm_image") RequestBody farm_image,
                                      @Part("farm_video") RequestBody farm_video,
                                      @Part MultipartBody.Part updatevideo,
                                      @Part MultipartBody.Part pimage);


    @Multipart
    @POST("flagfarmer/{id}")
    Call<ResponseBody> sendFlagFarmer(@Path("id") int farmerKey, @Part("flag_comment") RequestBody comment);

    @GET("farmgroup/{id}")
    Call<FarmGroupSinglePojo> getFarmGroupById(@Path("id") int id);

    @GET("farmersearch/{search}")
    Call<FarmerListPojo> getFarmerList(@Path("search") String search);

    @GET("farmers")
    Call<FarmerListPojo> getAllFarmers();

    @GET("farmgroupsearch/{search}")
    Call<FarmGroupListPojo> getListOfFarmGroupByKeyword(@Path("search") String search);

    @GET("banks")
    Call<GetBanksListPojo> getAllBanks();

    @GET("farmlocation")
    Call<FarmLocationListPojo> getAllFarmLocations();
    @Multipart
    @POST("input")
    Call<ResponseBody> addInputInventory(@Part("user_id") int userID, @Part("crop_id") int cropId,
                                         @Part("input_name") RequestBody inputName,
                                         @Part("total_quantity") int totalQuanity,
                                         @Part("input_unit") RequestBody inputUnit,
                                         @Part("seller_name") RequestBody sellerName);
    @GET("inputs")
    Call<InventoryList> getAllInputInventory();

    @GET("input/{id}")
    Call<InventoryPojo> getInventoryById(@Path("id") int id);

    @Multipart
    @POST("farmerinput/{id}")
    Call<ResponseBody> assignInputToFarmer(@Path("id") int id, @Part("farmer_id") int farmerId, @Part("user_assign_id") int tfs_id,
                                           @Part("farmer_input_id") int farmerInputId, @Part("farmer_input_quantity") int inputQuantity,
                                           @Part("planting_stage") RequestBody plantingStage);
    @Multipart
    @POST("monitor/{id}")
    Call<ResponseBody> doMonitor(@Path("id") int id, @Part("user_monitor_id") int tfs_id, @Part("farmer_monitor_id") int farmerId,
                                 @Part("crop_monitor_type")int  cropId, @Part("farmgroup_monitor_id") int farmGroupId,
                                 @Part("date_of_visit") RequestBody dateOfVisit, @Part("monitor_comment") RequestBody comment);

    @Multipart
    @POST("cropmonitor")
    Call<ResponseBody> doCropMonitor(@Part("tfs_user_id") int tfs_user_id, @Part("farmer_id") int farmer_id,
                                     @Part("crop_type") int cropTypeId, @Part("farming_stage") RequestBody farming_stage,
                                     @Part("q1") int landFertile, @Part("q2") int flood_and_disaster,
                                     @Part("q3") int external_threats,

                                     @Part MultipartBody.Part pimage,
                                     @Part("photo_string") RequestBody pro_image,

                                     @Part("q6") int signs_of_scratching,
                                     @Part("q7") int regular_feed_habit, @Part("q8") int regular_growth_habit,
                                     @Part("q9") int good_appereance, @Part("age_of_bird") RequestBody age_of_bird,
                                     @Part("weight_of_birds") RequestBody weight_of_birds, @Part("poultry_state") RequestBody poultry_state,
                                     @Part("mon_comment") RequestBody mon_comment,
                                    @Part("q4") int q4, @Part("q5") int q5);

    @Multipart
    @POST("assignLand/{id}")
    Call<ResponseBody> assignLand(@Path("id") int id, @Part("user_assign_land_id") int user_assign_land_id,
                                  @Part("farmer_assign_land_id") int farmer_assign_land_id, @Part("land_area") RequestBody land_area,
                                  @Part("latlonga") RequestBody latlonga,
                                  @Part("latlongb") RequestBody latlongb,
                                  @Part("latlongc") RequestBody latlongc,
                                  @Part("latlongd") RequestBody latlongd);

    @GET("localgovt")
    Call<LocalGovtListPojo> getAllLocalGov();

    @GET("states")

    Call<StateListPojo> getAllStates();

    @GET("crops")
    Call<CropListPojo> getAllCrops();



    @GET("farmersByGroup/{id}")
    Call<FarmerListPojo> getAllFarmerInAFarmGroup(@Path("id") int id);

    @GET("farmgroup")
    Call<FarmGroupListPojo> getAllFarmGroups();

    @Multipart
    @POST("harvestCollection")
    Call<ResponseBody> sendHarvestCollection(@Part("user_collect_id") int user_collect_id,
                                             @Part("farmer_collect_id") int farmer_collect_id,
                                             @Part("weight_of_bird") RequestBody weight_of_bird,
                                             @Part("num_of_mortality") int num_of_mortality,
                                             @Part("crop_type") RequestBody crop_type,
                                             @Part("quantity") int quantity,
                                             @Part("quantity_unit") RequestBody quantity_unit,
                                             @Part("comment") RequestBody comment);

    @Multipart
    @POST("farmgroup")
    Call<ResponseBody> addNewGroup(@Part("group_name") RequestBody groupName,
                                   @Part("gr_crop_type")RequestBody cropType,
                                   @Part("cycle_duration")RequestBody cycleDuraton,
                                   @Part("land_area_assign") int landAssignArea,
                                   @Part("land_area_unit")RequestBody unit,
                                   @Part("gr_location_id") int location_id);

    @GET("inputByFarmer/{id}")
    Call<InputListPojo> getFarmersInputById(@Path("id") int id);

    @GET("farmers/{id}")
    Call<ListFullfarmer> getOneFarmerInfo(@Path("id") int id);

    @Multipart
    @POST("farmers")
    Call<AddFarmerResponsePojo> addFarmer(
            @Part("user_add_id") int tfs_id,
            @Part("fname") RequestBody fname,
            @Part("sname") RequestBody sname,
            @Part("dob") RequestBody dob,
            @Part("state_id") int stateId,
            @Part("local_id") int local_id,
            @Part("farm_group_id") int farm_group_id,
            @Part("farm_location_id") int farm_location_id,
            @Part("bank_id") int bank_id,
            @Part("gender") RequestBody gender,
            @Part("phone") RequestBody phone,
            @Part("marital_status") RequestBody marital_status,
            @Part("number_of_dependants") int number_of_dependant,
            @Part("add_labour") int add_labour,
            @Part("crop_proficiency") RequestBody crop_proficiency,
            @Part("acct_number") RequestBody acct_number,
            @Part("land_area_farmed") RequestBody land_area_farmed,
            @Part("years_of_experience") int years_of_experience,
            @Part("income_range") RequestBody income_range,
            @Part("previous_training") int previous_training,
            @Part("pro_image") RequestBody proimage,
            @Part("comment") RequestBody comment,
            @Part MultipartBody.Part pimage,
            @Part("access_pin") int access_pin);

    @Multipart
    @POST("updatefarmer/{id}")
    Call<ResponseBody>updateFarmer(
            @Path("id") int id,
            @Part("user_add_id") int tfs_id,
            @Part("fname") RequestBody fname,
            @Part("sname") RequestBody sname,
            @Part("dob") RequestBody dob,
            @Part("state_id") int stateId,
            @Part("local_id") int local_id,
            @Part("farm_group_id") int farm_group_id,
            @Part("farm_location_id") int farm_location_id,
            @Part("bank_id") int bank_id,
            @Part("gender") RequestBody gender,
            @Part("phone") RequestBody phone,
            @Part("marital_status") RequestBody marital_status,
            @Part("number_of_dependants") int number_of_dependant,
            @Part("add_labour") int add_labour,
            @Part("crop_proficiency") RequestBody crop_proficiency,
            @Part("acct_number") RequestBody acct_number,
            @Part("land_area_farmed") RequestBody land_area_farmed,
            @Part("years_of_experience") int years_of_experience,
            @Part("income_range") RequestBody income_range,
            @Part("previous_training") int previous_training,
            @Part("comment") RequestBody comment);



    //@Multipart
    //@POST("marktrained")
    //Call<ResponseBody> markTrained





}
