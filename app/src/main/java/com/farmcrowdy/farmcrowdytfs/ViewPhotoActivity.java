package com.farmcrowdy.farmcrowdytfs;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

public class ViewPhotoActivity extends AppCompatActivity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.view_photo_activity);
        findViewById(R.id.fromBack).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        ImageView imageView = findViewById(R.id.previewImage);
        String imageLink = getIntent().getExtras().getString("data");
        Picasso.with(this).load(imageLink).placeholder(R.drawable.ic_burst_mode_grey_24dp).into(imageView);


    }
}
