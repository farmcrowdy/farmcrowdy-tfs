package com.farmcrowdy.farmcrowdytfs.viewVideo;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.VideoView;

import com.farmcrowdy.farmcrowdytfs.R;

public class PreviewVideo extends AppCompatActivity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.preview_video);
        String path = getIntent().getExtras().getString("data");
        VideoView videoView = findViewById(R.id.videoView);
        videoView.setVideoPath(path);
        videoView.start();
    }
}
