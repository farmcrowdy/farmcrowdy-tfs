package com.farmcrowdy.farmcrowdytfs.signUp;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.farmcrowdy.farmcrowdytfs.NoPhotoInterface;
import com.farmcrowdy.farmcrowdytfs.R;
import com.rafakob.floatingedittext.FloatingEditText;

/**
 * Created by Oluwatobi on 5/30/2018.
 */

public class SignupFragment extends Fragment implements NoPhotoInterface {
    View view;
    ProgressDialog progressDialog;
    SignUpPresenter signUpPresenter;
    FloatingEditText email,firstname,surname,password;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.signup_fragment,container,false);
         email = view.findViewById(R.id.email_editText);
         firstname = view.findViewById(R.id.fullname_editText);
          surname = view.findViewById(R.id.lastname_editText);
         password = view.findViewById(R.id.password_editText);
        Button signUp = (Button)view.findViewById(R.id.signup_id);
        progressDialog = new ProgressDialog(getContext());
        progressDialog.setMessage("Registering technical field specialist");
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);

        signUpPresenter = new SignupPresenterImpl(this);
        signUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!TextUtils.isEmpty(firstname.getText()) || !TextUtils.isEmpty(email.getText()) ||
                        !TextUtils.isEmpty(surname.getText()) || !TextUtils.isEmpty(password.getText())){
                    progressDialog.show();
                    signUpPresenter.doRegister(email.getText().toLowerCase().trim(),password.getText(),firstname.getText(),surname.getText());

                }
                else {
                    Toast.makeText(getContext(), "All fields are compulsory to fill", Toast.LENGTH_SHORT).show();

                }
            }
        });
        return view;
    }

    @Override
    public void successful() {
        email.setText("");
        password.setText("");
        firstname.setText("");
        surname.setText("");

        progressDialog.dismiss();
        progressDialog.cancel();

        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle("Successful");
        builder.setMessage("Successfully registered, go and login now");
        builder.show();
    }

    @Override
    public void failure(String error) {
        if(error.contains("No address")) error = "No internet connection found";
        progressDialog.dismiss();
        progressDialog.cancel();

        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle("Login failed");
        builder.setMessage(error);
        builder.show();
    }

    @Override
    public void uploadPhotoSuccessful(String url) {

    }
}
