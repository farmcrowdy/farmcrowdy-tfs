package com.farmcrowdy.farmcrowdytfs.farmUpdates;

import android.content.Context;

import okhttp3.RequestBody;
import retrofit2.http.Part;

/**
 * Created by Oluwatobi on 5/25/2018.
 */

public interface FarmUpdatePresenter  {
    void sendFarmUpdate(int farmgroupId, String subject, String comment,String farm_image,String farm_video);
    void uploadPhoto(Context context,String path);

}
