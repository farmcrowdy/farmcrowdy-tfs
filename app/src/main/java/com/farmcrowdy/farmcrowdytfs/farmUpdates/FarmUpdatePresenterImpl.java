package com.farmcrowdy.farmcrowdytfs.farmUpdates;

import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Toast;

import com.farmcrowdy.farmcrowdytfs.ApplicationInstance;
import com.farmcrowdy.farmcrowdytfs.MyEndpoint;
import com.farmcrowdy.farmcrowdytfs.WithPhotoInterface;
import com.farmcrowdy.farmcrowdytfs.engine.LoadEffecient;
import com.farmcrowdy.farmcrowdytfs.engine.TinyDB;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Date;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Oluwatobi on 5/25/2018.
 */

public class FarmUpdatePresenterImpl implements FarmUpdatePresenter {
    WithPhotoInterface withPhotoInterface;
    String fileMinName="", videoName="";
    RequestBody fileMinNameBody, videoNameBody;

    public FarmUpdatePresenterImpl(WithPhotoInterface withPhotoInterface) {
        this.withPhotoInterface = withPhotoInterface;
    }

    private MultipartBody.Part getPhotoPart(String filePath) {
        Bitmap high = null;
        try {
            high = new LoadEffecient().decodeSampledBitmapFromFile(filePath, LoadEffecient.maxHeight, LoadEffecient.maxWidth);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        // Bitmap low = new LoadEffecient().decodeSampledBitmapFromFile(filePath, 80.0f, 80.0f);
        File file = new LoadEffecient().saveLocallyFromBitmapToTempFile(high, "bitmapHigh");
        RequestBody requestFile =
                RequestBody.create(
                        MediaType.parse("*/*"), file
                );
        fileMinName = "abaPhoto"+new Date().getTime()+".jpg";
        this.fileMinNameBody = RequestBody.create(MediaType.parse("text/plain"), fileMinName);

        MultipartBody.Part body =
                MultipartBody.Part.createFormData("pimage", fileMinName, requestFile);
        return body;
    }
    private MultipartBody.Part getVideoPart(String filePath) {

        File file = new File(filePath);
        RequestBody requestFile =
                RequestBody.create(
                        MediaType.parse("video/*"), file
                );
        videoName = "20percentTestingVideo"+new Date().getTime()+".mp4";
        this.videoNameBody = RequestBody.create(MediaType.parse("text/plain"), videoName);

        MultipartBody.Part body =
                MultipartBody.Part.createFormData("updatevideo", videoName, requestFile);
        return body;
    }

    @Override
    public void sendFarmUpdate(int farmgroupId, String subject, String comment, String filePath, String videoPath) {
        MultipartBody.Part body = null;
        MultipartBody.Part videoBody  = null;

       if(!TextUtils.isEmpty(filePath)){
            body= getPhotoPart(filePath);
       }
        if(!TextUtils.isEmpty(videoPath)) {
          videoBody  = getVideoPart(videoPath);
        }
        MyEndpoint myEndpoint = ApplicationInstance.getRetrofit().create(MyEndpoint.class);
        RequestBody subjectRequest  =  RequestBody.create(MediaType.parse("text/plain"), subject);
        RequestBody commentRequest  =  RequestBody.create(MediaType.parse("text/plain"), comment);
        RequestBody farmImageRequest  =  RequestBody.create(MediaType.parse("text/plain"), fileMinName);
        RequestBody farmVideoRequest = RequestBody.create(MediaType.parse("text/plain"), videoName);
        int tfs_id = new TinyDB(ApplicationInstance.getContext()).getInt("user_id", 1);

        Call<ResponseBody> bodyCall = myEndpoint.sendFarmUpdate(tfs_id, farmgroupId, subjectRequest,commentRequest, farmImageRequest,farmVideoRequest,videoBody, body);
        bodyCall.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if(response.isSuccessful()){
                    withPhotoInterface.successful(response.message());
                }
                else {
                    withPhotoInterface.failure(response.message());
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                withPhotoInterface.failure(t.getMessage());
            }
        });
    }

    @Override
    public void uploadPhoto(final Context context, String filePath) {
        Log.d("DEMN","SEEN HERE 1");
        try {

            Bitmap high = new LoadEffecient().decodeSampledBitmapFromFile(filePath, LoadEffecient.maxHeight, LoadEffecient.maxWidth);
            // Bitmap low = new LoadEffecient().decodeSampledBitmapFromFile(filePath, 80.0f, 80.0f);
            File file = new LoadEffecient().saveLocallyFromBitmapToTempFile(high, "bitmapHigh");
            //  File forLow = new LoadEffecient().saveLocallyFromBitmapToTempFile(low, "bitmapLow");
            Uri fileUri = Uri.fromFile(file);
            if(!file.exists()){
                withPhotoInterface.failure("not existing");
                return;
            }
            //  final Uri fileUriLow = Uri.fromFile(forLow);
            //Upload to server and get Link

           // withPhotoInterface.uploadPhotoSuccessful("high", "low");

            RequestBody requestFile =
                    RequestBody.create(
                            MediaType.parse("*/*"),
                            file
                    );

            Log.d("DEMN","SEEN HERE 2");
            // MultipartBody.Part is used to send also the actual file name
            MultipartBody.Part body =
                    MultipartBody.Part.createFormData("pimage", "zipper.jpg", requestFile);
            MyEndpoint myEndpoint = ApplicationInstance.getRetrofit().create(MyEndpoint.class);
            RequestBody name = RequestBody.create(MediaType.parse("text/plain"), "upload_test");
            Call<ResponseBody> call = myEndpoint.upload(body,requestFile);
            call.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call,
                                       Response<ResponseBody> response) {
                    withPhotoInterface.failure(response.message());
                    Toast.makeText(context, "Seen out"+response.code(), Toast.LENGTH_SHORT).show();
                    Log.d("DEMN",response.message());
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    Log.d("DEMN", t.getMessage());
                    Toast.makeText(context, "Seen out"+t.getMessage(), Toast.LENGTH_SHORT).show();
                    withPhotoInterface.failure(t.getMessage());
                }
            });
        }
        catch (Exception e){
            Log.d("DEMN", "exception at "+e.getMessage());
            Toast.makeText(context, "Seen out"+e.getMessage(), Toast.LENGTH_SHORT).show();
            withPhotoInterface.failure(e.getMessage());
        }
    }
}
