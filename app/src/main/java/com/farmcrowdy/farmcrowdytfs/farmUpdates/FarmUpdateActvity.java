package com.farmcrowdy.farmcrowdytfs.farmUpdates;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.text.format.Formatter;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import com.farmcrowdy.farmcrowdytfs.ApplicationInstance;
import com.farmcrowdy.farmcrowdytfs.DirectPojo.FarmGroupPojo;
import com.farmcrowdy.farmcrowdytfs.ListPojo.FarmGroupListPojo;
import com.farmcrowdy.farmcrowdytfs.MyEndpoint;
import com.farmcrowdy.farmcrowdytfs.R;
import com.farmcrowdy.farmcrowdytfs.WithPhotoInterface;
import com.farmcrowdy.farmcrowdytfs.addFarmer.OneAddFarmer;
import com.farmcrowdy.farmcrowdytfs.engine.LoadEffecient;
import com.farmcrowdy.farmcrowdytfs.silicompressorr.SiliCompressor;
import com.farmcrowdy.farmcrowdytfs.updateProfile.UpdateProfileActivity;
import com.farmcrowdy.farmcrowdytfs.viewVideo.PreviewVideo;
import com.github.tcking.giraffecompressor.GiraffeCompressor;
import com.rafakob.floatingedittext.FloatingEditText;
import com.raizlabs.android.dbflow.sql.language.Delete;
import com.raizlabs.android.dbflow.sql.language.SQLite;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.io.File;
import java.io.FileNotFoundException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;

/**
 * Created by Oluwatobi on 5/30/2018.
 */

public class FarmUpdateActvity extends AppCompatActivity implements WithPhotoInterface {
    String imageList="";
    TextView pickPhotoImage;
    FarmUpdatePresenter presenter;
    ProgressDialog progressDialog;
    List<FarmGroupPojo> groupList;
    List<String> groupListString;
    List<Integer> groupListInteger;
    int chosenId;
    static final int REQUEST_VIDEO_CAPTURE = 1;
    FloatingEditText subjectEdit, commentEdit;
    String videoPath = "";
    private static final int SELECT_VIDEO = 4;
    private static final int RECORD_REQUEST_CODE = 401;
    TextView mb;

    Spinner chooseFarmGroup;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

       // mListener = (OnVideoRecorderListener) this;
        setContentView(R.layout.farm_update_activity);

        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitleTextColor(Color.WHITE);
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        presenter = new FarmUpdatePresenterImpl(this);


        toolbar.setTitleTextColor(Color.parseColor("#ffffff"));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        subjectEdit = (FloatingEditText)findViewById(R.id.subject_editText);
        commentEdit =  (FloatingEditText)findViewById(R.id.comment_editText);
        mb = findViewById(R.id.mb);

         chooseFarmGroup= (Spinner)findViewById(R.id.update_farm_name_spinner);
        pickPhotoImage = findViewById(R.id.takePhoto);
        Button sendAction = (Button)findViewById(R.id.sendAction);
        pickPhotoImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CropImage.activity()
                        .setGuidelines(CropImageView.Guidelines.ON)
                        .start(FarmUpdateActvity.this);
            }
        });

        findViewById(R.id.takeVideo).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showVideoStuffDialog();
            }
        });


                doAddings();

                ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, groupListString);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        chooseFarmGroup.setAdapter(dataAdapter);

        chooseFarmGroup.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                chosenId = groupListInteger.get(i);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });



        sendAction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(chosenId !=0){
                    if(!TextUtils.isEmpty(subjectEdit.getText()) && !TextUtils.isEmpty(commentEdit.getText())) {

                       // Toast.makeText(FarmUpdateActvity.this, "Seen out"+imageList, Toast.LENGTH_SHORT).show();
                        File f = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/" + getPackageName() + "/media/videos");
                        if (f.mkdirs() || f.isDirectory()) {
                            //Toast.makeText(FarmUpdateActvity.this, "Video is ready for compression", Toast.LENGTH_LONG).show();
                            progressDialog = new ProgressDialog(FarmUpdateActvity.this);
                            progressDialog.setMessage("Compression in progress");
                            progressDialog.setCancelable(false);
                            progressDialog.setCanceledOnTouchOutside(false);
                           try{
                               progressDialog.show();
                           }
                           catch (Exception e) {

                           }
                            if(!TextUtils.isEmpty(videoPath)) new VideoCompressAsyncTask(FarmUpdateActvity.this).execute(videoPath, f.getPath());
                            else {
                                presenter.sendFarmUpdate(chosenId, subjectEdit.getText(), commentEdit.getText(), imageList,"");
                            }

                        }
                        else {
                            Toast.makeText(FarmUpdateActvity.this, "Cant find environment", Toast.LENGTH_LONG).show();
                        }
                    }
                    else {
                        Toast.makeText(FarmUpdateActvity.this, "Please fill in topic and narration", Toast.LENGTH_SHORT).show();
                    }
                }
                else {
                    Toast.makeText(FarmUpdateActvity.this, "Please select farm group",Toast.LENGTH_SHORT).show();
                }


            }
        });
        refreshGroupList();
    }

    protected void makeRequest() {
        ActivityCompat.requestPermissions(this,
                new String[]{Manifest.permission.CAPTURE_VIDEO_OUTPUT},
                RECORD_REQUEST_CODE);
    }



    void showVideoStuffDialog() {
        AlertDialog.Builder builderSingle = new AlertDialog.Builder(this);
        //builderSingle.setIcon(R.drawable.ic_launcher);
        builderSingle.setTitle("Select one:-");

        final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this, android.R.layout.select_dialog_singlechoice);
        arrayAdapter.add("Start a new video recording");
        arrayAdapter.add("Select video from gallery");

        builderSingle.setNegativeButton("cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        builderSingle.setAdapter(arrayAdapter, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which){
                    case 0: startForRecording();
                        break;
                    case 1:chooseVideoFromGallery();
                        break;
                }

                }
        });

        builderSingle.show();
    }

    void startForRecording() {
        Intent takeVideoIntent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
        if (takeVideoIntent.resolveActivity(getPackageManager()) != null) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (checkSelfPermission(Manifest.permission.CAMERA)
                        != PackageManager.PERMISSION_GRANTED) {
                    requestPermissions(new String[]{Manifest.permission.CAMERA},
                            RECORD_REQUEST_CODE);
                }
                else startActivityForResult(takeVideoIntent, REQUEST_VIDEO_CAPTURE);
            }
            else startActivityForResult(takeVideoIntent, REQUEST_VIDEO_CAPTURE);
        }
        else Toast.makeText(this, "This phone seems not have a video recording capability", Toast.LENGTH_SHORT).show();
    }
    void chooseVideoFromGallery() {
        Intent i = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Video.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(i, SELECT_VIDEO);
    }
    private void doAddings(){
        groupList = SQLite.select().
                from(FarmGroupPojo.class).queryList();

        groupListString = new ArrayList<>();
        groupListInteger = new ArrayList<>();

        for(int i=0; i<groupList.size(); i++){
            groupListString.add(groupList.get(i).getGroup_name());
            groupListInteger.add(groupList.get(i).getGroup_id());
        }


    }
    @Override
    public void successful(String message) {
        progressDialog.dismiss();
        progressDialog.cancel();
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Farm update sent successfully");
        builder.setTitle("Successful");
        builder.setCancelable(false);
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                finish();
            }
        });
        builder.show();

    }

    @Override
    public void failure(String error) {
        if(error.contains("No address")) error = "No internet connection found";
        progressDialog.dismiss();
        progressDialog.cancel();
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Due to "+error);
        builder.setTitle("Failed");
        builder.show();

    }

    @Override
    public void uploadPhotoSuccessful(String urlHigh, String urlLow) {


    }




    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case RECORD_REQUEST_CODE:
                Intent takeVideoIntent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
                startActivityForResult(takeVideoIntent, REQUEST_VIDEO_CAPTURE);
                break;
            case REQUEST_VIDEO_CAPTURE:
                if(resultCode ==RESULT_OK){
                    videoPath = data.getData().toString();
                    String sPath = getPath(data.getData());
                    String filename = sPath.substring(sPath.lastIndexOf("/")+1);

                    mb.setText("Video:"+filename);
                }
                else {
                    Toast.makeText(FarmUpdateActvity.this, "Video cancelled", Toast.LENGTH_SHORT).show();
                }
                break;

            case SELECT_VIDEO:
                if(data == null) Toast.makeText(FarmUpdateActvity.this, "Couldn't find the video path, please try again", Toast.LENGTH_SHORT).show();
                else {
                    videoPath = data.getData().toString();
                    String sPath = getPath(data.getData());
                    File imageFile = new File(sPath);
                    String filename = sPath.substring(sPath.lastIndexOf("/")+1);

                    mb.setText("Video: "+filename);
                }
                break;

            case CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE:

                    CropImage.ActivityResult result = CropImage.getActivityResult(data);
                    if (resultCode == RESULT_OK) {
                        Uri resultUri = result.getUri();
                        imageList = resultUri.getPath();
                        if (!imageList.equals("empty")) {
                            Bitmap high;
                            try {
                                high = new LoadEffecient().decodeSampledBitmapFromFile(imageList, 200, 200);
                                File file = new LoadEffecient().saveLocallyFromBitmapToTempFile(high, "bitmapHigh");
                                String filename = imageList.substring(imageList.lastIndexOf("/")+1);
                                ImageView imageView = findViewById(R.id.sds);
                                imageView.setVisibility(View.VISIBLE);
                                imageView.setImageURI(Uri.parse(file.getPath()));
                                mb.setText("Picture: "+filename);

                            } catch (FileNotFoundException e) {
                                e.printStackTrace();
                            }
                        }
                    } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                        Exception error = result.getError();
                    }

                break;

            default:
                super.onActivityResult(requestCode, resultCode, data);
                break;
        }
    }
    public String getPath(Uri uri) {
        String[] projection = { MediaStore.Images.Media.DATA };
        Cursor cursor = getContentResolver().query(uri, projection, null, null, null);
        if (cursor == null) return null;
        int column_index =             cursor.getColumnIndexOrThrow(MediaStore.Video.Media.DATA);
        cursor.moveToFirst();
        String s=cursor.getString(column_index);
        cursor.close();
        return s;
    }

    class VideoCompressAsyncTask extends AsyncTask<String, String, String> {

        Context mContext;

        public VideoCompressAsyncTask(Context context) {
            mContext = context;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected String doInBackground(String... paths) {
            String filePath = null;
            try {

                filePath = SiliCompressor.with(mContext).compressVideo(Uri.parse(paths[0]), paths[1]);

            } catch (URISyntaxException e) {
                e.printStackTrace();
            }
            return filePath;

        }


        @Override
        protected void onPostExecute(String compressedFilePath) {
            super.onPostExecute(compressedFilePath);
            progressDialog.setMessage("Video compression completed, uploading data");
            File imageFile = new File(compressedFilePath);
            float length = imageFile.length() / 1024f; // Size in KB
            String value;
            if (length >= 1024)
                value = length / 1024f + " MB";
            else
                value = length + " KB";
            presenter.sendFarmUpdate(chosenId, subjectEdit.getText(), commentEdit.getText(), imageList,compressedFilePath);


        }
    }
    void refreshGroupList(){
        MyEndpoint myEndpoint = ApplicationInstance.getRetrofit().create(MyEndpoint.class);
        Call<FarmGroupListPojo> farmGroupListPojoCall = myEndpoint.getAllFarmGroups();
        farmGroupListPojoCall.enqueue(new Callback<FarmGroupListPojo>() {
            @Override
            public void onResponse(Call<FarmGroupListPojo> call, Response<FarmGroupListPojo> response) {
                //save locally

                if(response.isSuccessful()){
                    Delete.table(FarmGroupPojo.class);
                    FarmGroupPojo farmGroupPojo2  = new FarmGroupPojo();
                    farmGroupPojo2.setGroup_id(0);
                    farmGroupPojo2.setGroup_name("Select farm group");
                    farmGroupPojo2.save();

                    for(FarmGroupPojo farmGroupPojo: response.body().getMessage()){
                        farmGroupPojo2.setGroup_id(farmGroupPojo.getGroup_id());
                        farmGroupPojo2.setGroup_name(farmGroupPojo.getGroup_name());
                        farmGroupPojo2.save();
                    }
                    List<FarmGroupPojo> fgList = SQLite.select().from(FarmGroupPojo.class).queryList();
                    groupListString = new ArrayList<>();
                    groupListInteger = new ArrayList<>();
                    for(int i=0; i<fgList.size(); i++){
                            groupListString.add(fgList.get(i).getGroup_name());
                            groupListInteger.add(fgList.get(i).getGroup_id());




                    }

                    ArrayAdapter<String> dataAdapteFarmGroup = new ArrayAdapter<String>(FarmUpdateActvity.this,
                            android.R.layout.simple_spinner_item, groupListString);
                    dataAdapteFarmGroup.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    chooseFarmGroup.setAdapter(dataAdapteFarmGroup);
                }
                else {
                    //Delete all stuff in localdatabase
                }
            }

            @Override
            public void onFailure(Call<FarmGroupListPojo> call, Throwable t) {

            }
        });

    }

    @Override

    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == RECORD_REQUEST_CODE) {

            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Intent takeVideoIntent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
                startActivityForResult(takeVideoIntent, REQUEST_VIDEO_CAPTURE);
                //Toast.makeText(this, "camera permission granted", Toast.LENGTH_LONG).show();

            } else {

                Toast.makeText(this, "camera permission denied", Toast.LENGTH_LONG).show();

            }

        }
    }

}
