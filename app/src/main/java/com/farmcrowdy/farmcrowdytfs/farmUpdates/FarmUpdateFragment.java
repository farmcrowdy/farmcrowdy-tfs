package com.farmcrowdy.farmcrowdytfs.farmUpdates;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Spinner;


import com.farmcrowdy.farmcrowdytfs.R;
import com.rafakob.floatingedittext.FloatingEditText;

import java.util.List;

/**
 * Created by Oluwatobi on 5/28/2018.
 */

public class FarmUpdateFragment extends Fragment {
    View view;
    ImageView pickPhotoImage;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
       view = inflater.inflate(R.layout.farm_fragment_layout, container,false);
        FloatingEditText subjectEdit  = (FloatingEditText)view.findViewById(R.id.subject_editText);
        FloatingEditText commentEdit =  (FloatingEditText)view.findViewById(R.id.comment_editText);
        Spinner chooseFarmGroup = (Spinner)view.findViewById(R.id.update_farm_name_spinner);
        pickPhotoImage = (ImageView)view.findViewById(R.id.takePhoto);
        pickPhotoImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });
        return view;
    }

}
