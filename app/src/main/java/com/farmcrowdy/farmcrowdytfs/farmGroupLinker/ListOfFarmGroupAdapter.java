package com.farmcrowdy.farmcrowdytfs.farmGroupLinker;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.farmcrowdy.farmcrowdytfs.DirectPojo.FarmGroupPojo;
import com.farmcrowdy.farmcrowdytfs.R;
import com.farmcrowdy.farmcrowdytfs.onlyFarmer.JustFarmerActivity;

import java.util.List;

/**
 * Created by Oluwatobi on 6/1/2018.
 */

public class ListOfFarmGroupAdapter extends RecyclerView.Adapter<ListOfFarmGroupAdapter.ListOfGroupsViewHolder> {
    List<FarmGroupPojo> farmGroupPojoList;
    Context context;

    public ListOfFarmGroupAdapter(List<FarmGroupPojo> farmGroupPojoList, Context context) {
        this.farmGroupPojoList = farmGroupPojoList;
        this.context = context;
    }


    @Override
    public ListOfGroupsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.farm_group_list_items,parent,false);
        return new ListOfGroupsViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ListOfGroupsViewHolder holder, final int position) {
        holder.groupName.setText(farmGroupPojoList.get(position).getGroup_name());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent  = new Intent(context, JustFarmerActivity.class);
                intent.putExtra("label", farmGroupPojoList.get(position).getGroup_name());
                intent.putExtra("id", farmGroupPojoList.get(position).getGroup_id());
               context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        if(farmGroupPojoList!=null) return farmGroupPojoList.size();
        else return 0;
    }

    public class ListOfGroupsViewHolder extends RecyclerView.ViewHolder{
        TextView groupName;
        public ListOfGroupsViewHolder(View itemView) {
            super(itemView);
            groupName =(TextView) itemView.findViewById(R.id.farm_group_name_id);
        }
    }
}
