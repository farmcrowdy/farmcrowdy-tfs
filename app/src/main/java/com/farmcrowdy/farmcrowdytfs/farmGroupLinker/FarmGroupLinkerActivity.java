package com.farmcrowdy.farmcrowdytfs.farmGroupLinker;

import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.farmcrowdy.farmcrowdytfs.DirectPojo.CropPojo;
import com.farmcrowdy.farmcrowdytfs.DirectPojo.FarmLocationPojo;
import com.farmcrowdy.farmcrowdytfs.ListPojo.FarmGroupListPojo;
import com.farmcrowdy.farmcrowdytfs.R;
import com.raizlabs.android.dbflow.sql.language.SQLite;

import java.util.List;

/**
 * Created by Oluwatobi on 6/1/2018.
 */

public class FarmGroupLinkerActivity extends AppCompatActivity implements FarmGroupInterface {
    Spinner sorBySpinner;
    FarmGroupPresenter farmGroupPresenter;
    ProgressBar progressBar;
    TextView callback;
     ArrayAdapter<String> arrayAdapter;
    AlertDialog.Builder builderSingle;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.farm_group_layout);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setTitleTextColor(Color.WHITE);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        sorBySpinner = (Spinner) findViewById(R.id.sortby_spinner);
        farmGroupPresenter = new FarmGroupPresenterImpl(this);
        callback = (TextView)findViewById(R.id.callback);
        progressBar = (ProgressBar)findViewById(R.id.grgProgress);
        startSearchMock();
        farmGroupPresenter.sortBy("null");
        sorBySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if(i!=0){
                    showDaig(i);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

    }

    private void startSearchMock(){
        progressBar.setVisibility(View.VISIBLE);
        callback.setText("Getting information");
        callback.setVisibility(View.VISIBLE);

    }
    private void endSearchMock(){
        progressBar.setVisibility(View.GONE);
        callback.setVisibility(View.GONE);
    }
    private void negativeFeedback(){
        progressBar.setVisibility(View.GONE);
        callback.setText("No farmer is in this group yet");
    }

    private void showDaig(int pos){

        if(pos ==1) {
            startSearchMock();
            //Toast.makeText(FarmGroupLinkerActivity.this, "meme "+strName, Toast.LENGTH_SHORT).show();
            farmGroupPresenter.sortBy("null");
            return;
        }

        builderSingle = new AlertDialog.Builder(FarmGroupLinkerActivity.this);
        builderSingle.setTitle("Select one:-");

        arrayAdapter = new ArrayAdapter<>(this, android.R.layout.select_dialog_singlechoice);
        switch (pos) {

            case 2:
                List<CropPojo> cropPojoList = SQLite.select().
                        from(CropPojo.class).queryList();
                for(int i=0; i<cropPojoList.size(); i++){
                    arrayAdapter.add(cropPojoList.get(i).getCrop_name());
                }
                break;
            case 3:
                arrayAdapter.add("2017");
                arrayAdapter.add("2018");
                break;

        }


        builderSingle.setNegativeButton("cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        builderSingle.setAdapter(arrayAdapter, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String strName = arrayAdapter.getItem(which);
                startSearchMock();
                //Toast.makeText(FarmGroupLinkerActivity.this, "meme "+strName, Toast.LENGTH_SHORT).show();
                farmGroupPresenter.sortBy(strName);
            }
        });
        builderSingle.show();

    }

    @Override
    public void getResult(FarmGroupListPojo farmGroupListPojo) {
    endSearchMock();
        RecyclerView recyclerView = findViewById(R.id.farm_group_recyclerview);
        recyclerView.setVisibility(View.VISIBLE);
        LinearLayoutManager linearLayoutManager  = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setHasFixedSize(true);
        ListOfFarmGroupAdapter listOfFarmGroupAdapter = new ListOfFarmGroupAdapter(farmGroupListPojo.getMessage(), this);
        recyclerView.setAdapter(listOfFarmGroupAdapter);
    }

    @Override
    public void failureRes(String error) {
    negativeFeedback();
    }
}
