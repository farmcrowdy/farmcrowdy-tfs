package com.farmcrowdy.farmcrowdytfs.farmGroupLinker;

/**
 * Created by Oluwatobi on 5/25/2018.
 */

public interface FarmGroupPresenter {
    void sortBy(String query);
}
