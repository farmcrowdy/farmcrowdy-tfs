package com.farmcrowdy.farmcrowdytfs.farmGroupLinker;

import com.farmcrowdy.farmcrowdytfs.ListPojo.FarmGroupListPojo;

import java.util.ArrayList;

/**
 * Created by Oluwatobi on 6/1/2018.
 */

public interface FarmGroupInterface {
    void getResult(FarmGroupListPojo farmGroupListPojo);
    void failureRes(String error);
}
