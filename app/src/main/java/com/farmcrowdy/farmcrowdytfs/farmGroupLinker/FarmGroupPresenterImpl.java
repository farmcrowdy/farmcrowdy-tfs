package com.farmcrowdy.farmcrowdytfs.farmGroupLinker;

import com.farmcrowdy.farmcrowdytfs.ApplicationInstance;
import com.farmcrowdy.farmcrowdytfs.ListPojo.FarmGroupListPojo;
import com.farmcrowdy.farmcrowdytfs.MyEndpoint;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Oluwatobi on 6/1/2018.
 */

public class FarmGroupPresenterImpl implements FarmGroupPresenter {
    FarmGroupInterface farmGroupInterface;

    public FarmGroupPresenterImpl(FarmGroupInterface farmGroupInterface) {
        this.farmGroupInterface = farmGroupInterface;
    }


    @Override
    public void sortBy(String query) {
    if(query.equals("null"))doInitial();
        else doSort(query);

    }
    private void doInitial() {
        MyEndpoint myEndpoint = ApplicationInstance.getRetrofit().create(MyEndpoint.class);
        Call<FarmGroupListPojo> call= myEndpoint.getAllFarmGroups();
        call.enqueue(new Callback<FarmGroupListPojo>() {
            @Override
            public void onResponse(Call<FarmGroupListPojo> call, Response<FarmGroupListPojo> response) {
                if(response.isSuccessful()){
                    farmGroupInterface.getResult(response.body());
                }
                else {
                    farmGroupInterface.failureRes(response.message());
                }
            }

            @Override
            public void onFailure(Call<FarmGroupListPojo> call, Throwable t) {
                farmGroupInterface.failureRes(t.getMessage());
            }
        });
    }
    private void doSort(String query){
        MyEndpoint myEndpoint = ApplicationInstance.getRetrofit().create(MyEndpoint.class);
        Call<FarmGroupListPojo> call= myEndpoint.getListOfFarmGroupByKeyword(query);
        call.enqueue(new Callback<FarmGroupListPojo>() {
            @Override
            public void onResponse(Call<FarmGroupListPojo> call, Response<FarmGroupListPojo> response) {
                if(response.isSuccessful()){
                    farmGroupInterface.getResult(response.body());
                }
                else {
                    farmGroupInterface.failureRes(response.message());
                }
            }

            @Override
            public void onFailure(Call<FarmGroupListPojo> call, Throwable t) {
                farmGroupInterface.failureRes(t.getMessage());
            }
        });
    }
}
