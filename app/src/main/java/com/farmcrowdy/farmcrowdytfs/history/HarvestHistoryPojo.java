package com.farmcrowdy.farmcrowdytfs.history;

public class HarvestHistoryPojo {
    String farmername,crop_type,quantity_unit,date_collected;
    int quantity;

    public String getFarmername() {
        return farmername;
    }

    public String getCrop_type() {
        return crop_type;
    }

    public String getQuantity_unit() {
        return quantity_unit;
    }

    public String getDate_collected() {
        return date_collected;
    }

    public int getQuantity() {
        return quantity;
    }
}
