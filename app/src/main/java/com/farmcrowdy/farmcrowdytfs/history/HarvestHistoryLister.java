package com.farmcrowdy.farmcrowdytfs.history;

import java.util.ArrayList;

public class HarvestHistoryLister {
    String status;
    ArrayList<HarvestHistoryPojo> message;

    public String getStatus() {
        return status;
    }

    public ArrayList<HarvestHistoryPojo> getMessage() {
        return message;
    }
}
