package com.farmcrowdy.farmcrowdytfs.history;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.farmcrowdy.farmcrowdytfs.R;

public class HistoryActivity extends AppCompatActivity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.history_layout);

        findViewById(R.id.backFrom).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        ViewPager viewPager = findViewById(R.id.history_pager);
        TabLayout tabLayout = findViewById(R.id.history_tab);
        viewPager.setAdapter(new HistoryPager(getSupportFragmentManager()));
        tabLayout.setupWithViewPager(viewPager);
    }
    private class HistoryPager extends FragmentPagerAdapter {

        public HistoryPager(FragmentManager fm) {
            super(fm);
        }
        String [] tabTitles = {"Input disbursed", "Harvest collected", "Farm monitored"};

        @Nullable
        @Override
        public CharSequence getPageTitle(int position) {
            return  tabTitles[position];
        }

        @Override
        public Fragment getItem(int i) {
            Fragment fragment = new Fragment();
            switch (i) {
                case 0: fragment = new InputDisbursedFragment();
                break;
                case 1: fragment = new HarvestCollectedFragment();
                break;
                case 2: fragment = new FarmMonitoredHistoryFragment();
                break;
            }
            return fragment;
        }

        @Override
        public int getCount() {
            return 3;
        }
    }
}
