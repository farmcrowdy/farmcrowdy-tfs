package com.farmcrowdy.farmcrowdytfs.history;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.farmcrowdy.farmcrowdytfs.R;
import com.github.curioustechizen.ago.RelativeTimeTextView;

public class HarvestHistoryAdapter extends RecyclerView.Adapter<HarvestHistoryAdapter.HarvestHistoryViewHolder> {
    HarvestHistoryLister inputeHistoryLister;
    Context context;

    public HarvestHistoryAdapter(HarvestHistoryLister inputeHistoryLister, Context context) {
        this.inputeHistoryLister = inputeHistoryLister;
        this.context = context;
    }

    @NonNull
    @Override
    public HarvestHistoryViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.monitoring_history_items, viewGroup, false);
        return new HarvestHistoryViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull HarvestHistoryViewHolder harvestHistoryViewHolder, final int i) {
    harvestHistoryViewHolder.farmer_name.setText(inputeHistoryLister.getMessage().get(i).getFarmername());
    harvestHistoryViewHolder.farmGroup.setText(inputeHistoryLister.getMessage().get(i).getCrop_type());
    harvestHistoryViewHolder.farmType.setText(inputeHistoryLister.getMessage().get(i).getQuantity()+" "+inputeHistoryLister.getMessage().get(i).getQuantity_unit() );
    harvestHistoryViewHolder.monDate.setText(inputeHistoryLister.getMessage().get(i).getDate_collected());
    harvestHistoryViewHolder.callImage.setVisibility(View.GONE);

    }

    @Override
    public int getItemCount() {
        return inputeHistoryLister.getMessage().size();
    }

    public class HarvestHistoryViewHolder extends RecyclerView.ViewHolder {
        TextView farmer_name, farmType, farmGroup;
        RelativeTimeTextView monDate;
        ImageView callImage;
        public HarvestHistoryViewHolder(@NonNull View itemView) {
            super(itemView);
            farmer_name = itemView.findViewById(R.id.mon_farmer_name);
            farmType = itemView.findViewById(R.id.mon_farm_type);
            farmGroup = itemView.findViewById(R.id.mon_farm_group);
            monDate = itemView.findViewById(R.id.mon_date);
            callImage = itemView.findViewById(R.id.caller);
        }
    }
}
