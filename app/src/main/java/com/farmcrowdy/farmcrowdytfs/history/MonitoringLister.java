package com.farmcrowdy.farmcrowdytfs.history;

import java.util.ArrayList;

public class MonitoringLister {
    String status;
    ArrayList<MonitoredHistoryPojo> message;

    public String getStatus() {
        return status;
    }

    public ArrayList<MonitoredHistoryPojo> getMessage() {
        return message;
    }
}
