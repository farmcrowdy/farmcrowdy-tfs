package com.farmcrowdy.farmcrowdytfs.history;

public class MonitoredHistoryPojo {
             int farmer_id;
             int crop_type;
             int q1, q2, q3, q6, q7, q8, q9,q4, q5;
             String age_of_bird, weight_of_birds, photo_string, mon_comment, mon_date, farmername, farming_stage;

    public int getFarmer_id() {
        return farmer_id;
    }

    public int getCrop_type() {
        return crop_type;
    }

    public int getQ1() {
        return q1;
    }

    public int getQ2() {
        return q2;
    }

    public int getQ3() {
        return q3;
    }

    public int getQ6() {
        return q6;
    }

    public int getQ7() {
        return q7;
    }

    public int getQ8() {
        return q8;
    }

    public int getQ9() {
        return q9;
    }

    public int getQ4() {
        return q4;
    }

    public int getQ5() {
        return q5;
    }

    public String getAge_of_bird() {
        return age_of_bird;
    }

    public String getWeight_of_birds() {
        return weight_of_birds;
    }

    public String getPhoto_string() {
        return photo_string;
    }

    public String getMon_comment() {
        return mon_comment;
    }

    public String getMon_date() {
        return mon_date;
    }

    public String getFarmername() {
        return farmername;
    }

    public String getFarming_stage() {
        return farming_stage;
    }
}
