package com.farmcrowdy.farmcrowdytfs.history;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.farmcrowdy.farmcrowdytfs.ApplicationInstance;
import com.farmcrowdy.farmcrowdytfs.MyEndpoint;
import com.farmcrowdy.farmcrowdytfs.R;
import com.farmcrowdy.farmcrowdytfs.engine.TinyDB;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HarvestCollectedFragment extends Fragment {
    RecyclerView recyclerView;
    ProgressBar progressBar;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.input_disbursed_layout, container, false);

        progressBar = view.findViewById(R.id.tbt);
        recyclerView = view.findViewById(R.id.inputDisbursedRecyclerId);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setHasFixedSize(true);
        checkHarvests();
        return view;
    }

    void checkHarvests() {
        MyEndpoint myEndpoint = ApplicationInstance.getRetrofit().create(MyEndpoint.class);
        int tfs_id = new TinyDB(ApplicationInstance.getContext()).getInt("user_id", 1);
        Call<HarvestHistoryLister> listerCall = myEndpoint.getHarvestHistory(tfs_id);
        listerCall.enqueue(new Callback<HarvestHistoryLister>() {
            @Override
            public void onResponse(Call<HarvestHistoryLister> call, Response<HarvestHistoryLister> response) {
                if(response.isSuccessful()) {
                    recyclerView.setAdapter(new HarvestHistoryAdapter(response.body(), getContext()));
                    progressBar.setVisibility(View.GONE);
                    recyclerView.setVisibility(View.VISIBLE);
                }
                else {
                    Log.d("FailureF", "due to: "+response.message());
                }

            }

            @Override
            public void onFailure(Call<HarvestHistoryLister> call, Throwable t) {
                Log.d("FailureF", "due to: "+t.getMessage());
            }
        });
    }
}
