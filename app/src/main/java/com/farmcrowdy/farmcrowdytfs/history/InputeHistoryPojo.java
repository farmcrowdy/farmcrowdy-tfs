package com.farmcrowdy.farmcrowdytfs.history;

public class InputeHistoryPojo {
    String farmername, mobile,date,planting_stage, quantity_unit;
    int farmer_input_quantity,farmer_input_id;

    public String getFarmername() {
        return farmername;
    }

    public String getMobile() {
        return mobile;
    }

    public int getFarmer_input_quantity() {
        return farmer_input_quantity;
    }

    public String getPlanting_stage() {
        return planting_stage;
    }

    public int getFarmer_input_id() {
        return farmer_input_id;
    }

    public String getDate() {
        return date;
    }

    public String getQuantity_unit() {
        return quantity_unit;
    }
}
