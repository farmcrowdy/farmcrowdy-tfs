package com.farmcrowdy.farmcrowdytfs.history;

import java.util.ArrayList;

public class InputeHistoryLister  {
    String status;
    ArrayList<InputeHistoryPojo> message;

    public String getStatus() {
        return status;
    }

    public ArrayList<InputeHistoryPojo> getMessage() {
        return message;
    }
}
