package com.farmcrowdy.farmcrowdytfs.history;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.farmcrowdy.farmcrowdytfs.R;
import com.github.curioustechizen.ago.RelativeTimeTextView;

public class InputeDisbursedAdapter extends RecyclerView.Adapter<InputeDisbursedAdapter.InputeViewHolder> {
    InputeHistoryLister inputeHistoryLister;
    Context context;

    public InputeDisbursedAdapter(InputeHistoryLister inputeHistoryLister, Context context) {
        this.inputeHistoryLister = inputeHistoryLister;
        this.context = context;
    }

    @NonNull
    @Override
    public InputeViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.monitoring_history_items, viewGroup, false);
        return new InputeViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull InputeViewHolder inputeViewHolder, final int i) {
        inputeViewHolder.farmer_name.setText(inputeHistoryLister.getMessage().get(i).getFarmername());
        inputeViewHolder.farmGroup.setText(inputeHistoryLister.getMessage().get(i).getPlanting_stage());
        inputeViewHolder.farmType.setText(inputeHistoryLister.getMessage().get(i).getFarmer_input_quantity() + " units of input");
        inputeViewHolder.monDate.setText(inputeHistoryLister.getMessage().get(i).getDate());
        inputeViewHolder.callImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent callIntent = new Intent(Intent.ACTION_CALL);
                callIntent.setData(Uri.parse("tel:" + inputeHistoryLister.getMessage().get(i).getMobile()));
                if (ActivityCompat.checkSelfPermission(context, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                    // TODO: Consider calling
                    //    ActivityCompat#requestPermissions
                    // here to request the missing permissions, and then overriding
                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                    //                                          int[] grantResults)
                    // to handle the case where the user grants the permission. See the documentation
                    // for ActivityCompat#requestPermissions for more details.
                    Toast.makeText(context, "Permission not graneted", Toast.LENGTH_SHORT).show();
                }
                else {
                    {
                        try{
                            context.startActivity(callIntent);
                        }
                        catch (Exception e) {
                            Toast.makeText(context, "Phone number not given", Toast.LENGTH_SHORT).show();
                        }
                    }
                }
        }
    });


    }

    @Override
    public int getItemCount() {
        return inputeHistoryLister.getMessage().size();
    }

    public class InputeViewHolder extends RecyclerView.ViewHolder {
        TextView farmer_name, farmType, farmGroup;
        RelativeTimeTextView monDate;
        ImageView callImage;
        public InputeViewHolder(@NonNull View itemView) {
            super(itemView);
            farmer_name = itemView.findViewById(R.id.mon_farmer_name);
            farmType = itemView.findViewById(R.id.mon_farm_type);
            farmGroup = itemView.findViewById(R.id.mon_farm_group);
            monDate = itemView.findViewById(R.id.mon_date);
            callImage = itemView.findViewById(R.id.caller);
        }
    }
}
