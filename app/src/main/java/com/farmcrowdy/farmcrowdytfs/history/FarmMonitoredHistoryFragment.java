package com.farmcrowdy.farmcrowdytfs.history;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.farmcrowdy.farmcrowdytfs.ApplicationInstance;
import com.farmcrowdy.farmcrowdytfs.MyEndpoint;
import com.farmcrowdy.farmcrowdytfs.R;
import com.farmcrowdy.farmcrowdytfs.engine.TinyDB;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

public class FarmMonitoredHistoryFragment extends Fragment {
    RecyclerView recyclerView;
    ProgressBar progressBar;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.input_disbursed_layout, container, false);

        progressBar = view.findViewById(R.id.tbt);
        recyclerView = view.findViewById(R.id.inputDisbursedRecyclerId);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setHasFixedSize(true);
        bringInInfo();
        return view;
    }

    @SuppressLint("CheckResult")
    private void bringInInfo() {
        Observable<MonitoringLister> monitoringListerObservable = Observable.create(new Observable.OnSubscribe<MonitoringLister>() {
            @Override
            public void call(final Subscriber<? super MonitoringLister> subscriber) {
                int tfs_id = new TinyDB(ApplicationInstance.getContext()).getInt("user_id", 1);
                MyEndpoint myEndpoint = ApplicationInstance.getRetrofit().create(MyEndpoint.class);
                Call<MonitoringLister> monitoringListerCall = myEndpoint.getMonitoredByTfs(tfs_id);
                monitoringListerCall.enqueue(new Callback<MonitoringLister>() {
                    @Override
                    public void onResponse(Call<MonitoringLister> call, Response<MonitoringLister> response) {
                        if(response.isSuccessful()) {
                            subscriber.onNext(response.body());
                            subscriber.onCompleted();
                        }
                        else  {
                            subscriber.onNext(null);
                            subscriber.onCompleted();
                        }
                    }

                    @Override
                    public void onFailure(Call<MonitoringLister> call, Throwable t) {
                        Log.d("EXCUSE", "error because of "+t.getMessage());
                        subscriber.onNext(null);
                        subscriber.onCompleted();
                    }
                });
            }
        });



        monitoringListerObservable.subscribeOn(Schedulers.newThread()).observeOn(AndroidSchedulers.mainThread()).subscribe(new Action1<MonitoringLister>() {
            @Override
            public void call(MonitoringLister monitoringLister) {
                if(monitoringLister !=null) {
                    recyclerView.setAdapter(new MonitoringAdapter(monitoringLister));
                    progressBar.setVisibility(View.GONE);
                    recyclerView.setVisibility(View.VISIBLE);
                }
                else {
                    Toast.makeText(getContext(), "Error due to ", Toast.LENGTH_SHORT).show();
                }
            }
        });

}



}
