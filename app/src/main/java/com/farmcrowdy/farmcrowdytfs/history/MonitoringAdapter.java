package com.farmcrowdy.farmcrowdytfs.history;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.farmcrowdy.farmcrowdytfs.R;
import com.farmcrowdy.farmcrowdytfs.engine.GetDataInfo;
import com.github.curioustechizen.ago.RelativeTimeTextView;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class MonitoringAdapter extends RecyclerView.Adapter<MonitoringAdapter.MonitoringViewHolder> {
    MonitoringLister monitoringLister;

    public MonitoringAdapter(MonitoringLister monitoringLister) {
        this.monitoringLister = monitoringLister;
    }

    @NonNull
    @Override
    public MonitoringViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.monitoring_history_items, viewGroup, false);
        return new MonitoringViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MonitoringViewHolder monitoringViewHolder, int i) {
    MonitoredHistoryPojo monitoredHistoryPojo = monitoringLister.getMessage().get(i);
    monitoringViewHolder.farmer_name.setText(monitoredHistoryPojo.getFarmername());

    String cropType = new GetDataInfo().getCropNameFromId(monitoredHistoryPojo.getCrop_type());
    if(!cropType.equals("Select farm type")) monitoringViewHolder.farmType.setText(cropType);
    else  monitoringViewHolder.farmType.setText("Cassava");
    monitoringViewHolder.farmGroup.setText(monitoredHistoryPojo.getFarming_stage());
        SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        SimpleDateFormat f2 = new SimpleDateFormat("yyyy");
        try {
            Date ff = f.parse(monitoredHistoryPojo.getMon_date());

            //monitoringViewHolder.monDate.setReferenceTime(f.parse(monitoredHistoryPojo.getMon_date()).getTime());
            DateFormat dateTimeInstance = SimpleDateFormat.getDateTimeInstance();
           // System.out.println(dateTimeInstance.format(Calendar.getInstance().getTime()));
            monitoringViewHolder.monDate.setText(dateTimeInstance.format(f.parse(monitoredHistoryPojo.getMon_date())));
        } catch (ParseException e) {
            e.printStackTrace();
            monitoringViewHolder.monDate.setText(monitoredHistoryPojo.getMon_date());
        }
        monitoringViewHolder.caller.setVisibility(View.GONE);


    }

    @Override
    public int getItemCount() {
        if(monitoringLister.getMessage() !=null) return monitoringLister.getMessage().size();
        return 0;
    }

    public class MonitoringViewHolder extends RecyclerView.ViewHolder {

        TextView farmer_name, farmType, farmGroup;
        RelativeTimeTextView monDate;
        ImageView caller;
        public MonitoringViewHolder(@NonNull View itemView) {
            super(itemView);
            farmer_name = itemView.findViewById(R.id.mon_farmer_name);
            farmType = itemView.findViewById(R.id.mon_farm_type);
            farmGroup = itemView.findViewById(R.id.mon_farm_group);
            monDate = itemView.findViewById(R.id.mon_date);
            caller = itemView.findViewById(R.id.caller);
        }
    }
}
