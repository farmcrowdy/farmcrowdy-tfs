package com.farmcrowdy.farmcrowdytfs.messages;

import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.MediaController;
import android.widget.ProgressBar;
import android.widget.Toast;
import android.widget.VideoView;

import com.farmcrowdy.farmcrowdytfs.R;

public class WatchVideoActivity extends AppCompatActivity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.watch_video_layout);
        String videoName = getIntent().getExtras().getString("data");
        final String videoLink = "https://www.farmcrowdy.com/farmapp/assets/images/messagemedia/"+videoName;
        VideoView videoView = findViewById(R.id.myVideoPreview);
        MediaController mediaController= new MediaController(this);
        mediaController.setAnchorView(videoView);
        final ProgressBar ppr = findViewById(R.id.ppr);

        //specify the location of media file
        Uri uri=Uri.parse(videoLink);

        //Setting MediaController and URI, then starting the videoView
        videoView.setMediaController(mediaController);
        videoView.setVideoURI(uri);
        videoView.requestFocus();
        videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mediaPlayer) {
                ppr.setVisibility(View.GONE);
               //Toast.makeText(WatchVideoActivity.this, videoLink, Toast.LENGTH_SHORT).show();
            }
        });
        videoView.start();

    }
}
