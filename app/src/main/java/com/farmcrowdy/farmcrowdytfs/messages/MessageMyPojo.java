package com.farmcrowdy.farmcrowdytfs.messages;

import com.farmcrowdy.farmcrowdytfs.engine.MyDatabase;
import com.raizlabs.android.dbflow.annotation.Column;
import com.raizlabs.android.dbflow.annotation.PrimaryKey;
import com.raizlabs.android.dbflow.annotation.Table;
import com.raizlabs.android.dbflow.structure.BaseModel;
@Table(database = MyDatabase.class)
public class MessageMyPojo extends BaseModel {
    @Column
    @PrimaryKey
    int msg_id;
    @Column
    String message_text;
    @Column
    String msg_videolink;
    @Column
    String msg_photolink;
    @Column
    String msg_audiolink;
    @Column
    String msg_date;
    @Column
    String farmername;
    @Column
    String mobile;
    @Column
    String farm_group;
    @Column
    String gender;
    @Column
    String propix;
    @Column
    int msg_read;

    public int getMsg_id() {
        return msg_id;
    }

    public void setMsg_id(int msg_id) {
        this.msg_id = msg_id;
    }

    public String getMessage_text() {
        return message_text;
    }

    public void setMessage_text(String message_text) {
        this.message_text = message_text;
    }

    public String getMsg_videolink() {
        return msg_videolink;
    }

    public void setMsg_videolink(String msg_videolink) {
        this.msg_videolink = msg_videolink;
    }

    public String getMsg_photolink() {
        return msg_photolink;
    }

    public void setMsg_photolink(String msg_photolink) {
        this.msg_photolink = msg_photolink;
    }

    public String getMsg_audiolink() {
        return msg_audiolink;
    }

    public void setMsg_audiolink(String msg_audiolink) {
        this.msg_audiolink = msg_audiolink;
    }

    public String getMsg_date() {
        return msg_date;
    }

    public void setMsg_date(String msg_date) {
        this.msg_date = msg_date;
    }

    public String getFarmername() {
        return farmername;
    }

    public void setFarmername(String farmername) {
        this.farmername = farmername;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getFarm_group() {
        return farm_group;
    }

    public void setFarm_group(String farm_group) {
        this.farm_group = farm_group;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public int getMsg_read() {
        return msg_read;
    }

    public String getPropix() {
        return propix;
    }

    public void setPropix(String propix) {
        this.propix = propix;
    }

    public void setMsg_read(int msg_read) {
        this.msg_read = msg_read;
    }
}
