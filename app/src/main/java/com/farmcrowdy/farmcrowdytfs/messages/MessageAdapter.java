package com.farmcrowdy.farmcrowdytfs.messages;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.farmcrowdy.farmcrowdytfs.DirectPojo.MessageList;
import com.farmcrowdy.farmcrowdytfs.DirectPojo.MessagePojo;
import com.farmcrowdy.farmcrowdytfs.R;
import com.github.curioustechizen.ago.RelativeTimeTextView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import de.hdodenhof.circleimageview.CircleImageView;

public class MessageAdapter extends RecyclerView.Adapter<MessageAdapter.MessageViewHolder> {
    Context context;
    MessageList messagePojoArrayList;

    public MessageAdapter(Context context, MessageList messagePojos) {
        this.context = context;
        this.messagePojoArrayList = messagePojos;
        Log.d("deepAgain", "id is: "+messagePojoArrayList.getMessage().get(4).getMsg_id());
    }
    @NonNull
    @Override
    public MessageViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.message_list_items, viewGroup, false);
        return new MessageViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MessageViewHolder messageViewHolder, final int i) {
        messageViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, MessageFullReadActivity.class);
                intent.putExtra("data", messagePojoArrayList.getMessage().get(i).getMsg_id());
                Log.d("deepAgainer", "id is: "+messagePojoArrayList.getMessage().get(i).getMsg_id());
                context.startActivity(intent);
            }
        });
        messageViewHolder.chatName.setText(messagePojoArrayList.getMessage().get(i).getFarmername());
        if(!messagePojoArrayList.getMessage().get(i).getMessage_text().isEmpty()) messageViewHolder.chatBrief.setText(messagePojoArrayList.getMessage().get(i).getMessage_text());
        SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        try {
            Date d = f.parse(messagePojoArrayList.getMessage().get(i).getMsg_date());
            long milliseconds = d.getTime();
            messageViewHolder.relativeTimeTextView.setReferenceTime(milliseconds);
        } catch (ParseException e) {
            e.printStackTrace();
            messageViewHolder.relativeTimeTextView.setText(e.getMessage());
        }
        if(messagePojoArrayList.getMessage().get(i).getMsg_read() == 1)messageViewHolder.status.setVisibility(View.GONE);
        if (messageViewHolder.getAdapterPosition() == i) {
            if (messagePojoArrayList.getMessage().get(i).getPropix() != null) {

                if (!messagePojoArrayList.getMessage().get(i).getPropix().equals("noimage.png")) {
                    String link = "https://www.farmcrowdy.com/farmapp/assets/images/messagepix/" + messagePojoArrayList.getMessage().get(i).getPropix();
                    Glide.with(context)
                            .load(link)
                            .into(messageViewHolder.dpa);
                }
            }
        }
        else {
            Glide.with(context).clear(messageViewHolder.dpa);
            messageViewHolder.dpa.setImageResource(R.drawable.ic_account_circle_lime_24dp);
        }



    }

    @Override
    public int getItemCount() {
        if(messagePojoArrayList !=null) return messagePojoArrayList.getMessage().size();
        else return 0;
    }

    public class MessageViewHolder extends RecyclerView.ViewHolder {
        TextView chatName, chatBrief, status;
        RelativeTimeTextView relativeTimeTextView;
        CircleImageView dpa;

        public MessageViewHolder(@NonNull View itemView) {
            super(itemView);
            chatName = itemView.findViewById(R.id.chat_name);
            chatBrief = itemView.findViewById(R.id.content_brief);
            relativeTimeTextView = itemView.findViewById(R.id.chat_time);
            status = itemView.findViewById(R.id.status);
            dpa = itemView.findViewById(R.id.dpa);
        }

    }
}
