package com.farmcrowdy.farmcrowdytfs.messages;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.jean.jcplayer.model.JcAudio;
import com.example.jean.jcplayer.view.JcPlayerView;
import com.farmcrowdy.farmcrowdytfs.ApplicationInstance;
import com.farmcrowdy.farmcrowdytfs.DirectPojo.MessagePojo;
import com.farmcrowdy.farmcrowdytfs.MyEndpoint;
import com.farmcrowdy.farmcrowdytfs.R;
import com.raizlabs.android.dbflow.sql.language.SQLite;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MessageFullReadActivity extends AppCompatActivity{
    TextView title, message;
    Button photoPreview,audioPreview, videoPreview, caller;
    ImageView mediaPhoto;
    JcPlayerView jcPlayerView;
    MessageMyPojo messagePojo;
    private static final int REQUEST_PHONE_CALL = 300;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.message_full_layout);

        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitleTextColor(Color.WHITE);
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        title = findViewById(R.id.chatName);
        message = findViewById(R.id.chatMessage);
        photoPreview = findViewById(R.id.photoPreview);
        audioPreview = findViewById(R.id.audioPreview);
        videoPreview = findViewById(R.id.videoPreview);
        mediaPhoto = findViewById(R.id.message_media1);
        caller = findViewById(R.id.callFarmer);
        jcPlayerView = findViewById(R.id.jcplayer);



         int msgID = getIntent().getExtras().getInt("data",1);
         messagePojo = SQLite.select().
                 from(MessageMyPojo.class).where(MessageMyPojo_Table.msg_id.is(msgID)).querySingle();

        Log.d("DATBER", "For theser: "+messagePojo.getMsg_videolink());
        title.setText(messagePojo.getFarmername());
        message.setText(messagePojo.getMessage_text());

        markReadReceipt(messagePojo.getMsg_read(), messagePojo.getMsg_id());
        Log.d("DATBER", "Another: "+messagePojo.getMsg_id());

        if(!messagePojo.getMsg_photolink().isEmpty())   {
            if(!messagePojo.getMsg_photolink().equals("empty")) {
                photoPreview.setVisibility(View.VISIBLE);
                String link = "https://www.farmcrowdy.com/farmapp/assets/images/messagepix/"+messagePojo.getMsg_photolink();
                Picasso.with(this).load(link).placeholder(R.drawable.image_placeholder).into(mediaPhoto);
             //   mediaPhoto.setImageURI();
            }

        }
        if(!messagePojo.getMsg_videolink().isEmpty())  {
            if(!TextUtils.equals(messagePojo.getMsg_videolink(), "empty")){
                videoPreview.setVisibility(View.VISIBLE);
                videoPreview.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                    }
                });
            }

        }
        photoPreview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mediaPhoto.setVisibility(View.VISIBLE);
            }
        });

        if(!messagePojo.getMsg_audiolink().isEmpty()) {
            if (!messagePojo.getMsg_audiolink().equals("empty")) {
                audioPreview.setVisibility(View.VISIBLE);
            }
        }
        audioPreview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                jcPlayerView.setVisibility(View.VISIBLE);
                ArrayList<JcAudio> jcAudios = new ArrayList<>();
                String link = "https://www.farmcrowdy.com/farmapp/assets/images/messagemedia/"+messagePojo.getMsg_audiolink();
                jcAudios.add(JcAudio.createFromURL(messagePojo.getFarmername()+"'s message to you",link));
                jcPlayerView.initPlaylist(jcAudios, null);
            }
        });

        if(!messagePojo.getMsg_videolink().isEmpty()) {
            if (!messagePojo.getMsg_videolink().equals("empty")) {
                videoPreview.setVisibility(View.VISIBLE);
            }
        }
        videoPreview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            Intent intent = new Intent(MessageFullReadActivity.this, WatchVideoActivity.class);
            intent.putExtra("data", messagePojo.getMsg_videolink());
            startActivity(intent);
            }
        });

        caller.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    Intent callIntent = new Intent(Intent.ACTION_CALL);
                    callIntent.setData(Uri.parse("tel:" + messagePojo.getMobile()));
                    if (ActivityCompat.checkSelfPermission(MessageFullReadActivity.this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                        // TODO: Consider calling
                        //    ActivityCompat#requestPermissions
                        // here to request the missing permissions, and then overriding
                        //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                        //                                          int[] grantResults)
                        // to handle the case where the user grants the permission. See the documentation
                        // for ActivityCompat#requestPermissions for more details.
                        ActivityCompat.requestPermissions(MessageFullReadActivity.this, new String[]{Manifest.permission.CALL_PHONE},REQUEST_PHONE_CALL);
                        return;

                    }
                    startActivity(callIntent);
                } catch (ActivityNotFoundException activityException) {
                    Log.e("Calling a Phone Number", "Call failed", activityException);
                }
            }
        });





    }

    @SuppressLint("MissingPermission")
    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case REQUEST_PHONE_CALL: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Intent callIntent = new Intent(Intent.ACTION_CALL);
                    callIntent.setData(Uri.parse("tel:" + messagePojo.getMobile()));
                    startActivity(callIntent);
                }
                else {
                    Toast.makeText(this, "Please, grant permission to be able to call farmer", Toast.LENGTH_SHORT).show();
                }
                return;
            }
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        jcPlayerView.kill();
    }
    void markReadReceipt(int status, int mesgId) {
        if(status==0) {
        MyEndpoint myEndpoint = ApplicationInstance.getRetrofit().create(MyEndpoint.class);
        Call<ResponseBody>  caller = myEndpoint.getMarkResponse(mesgId);
        caller.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
            if(!response.isSuccessful()) {
                Toast.makeText(MessageFullReadActivity.this, "Read receipt error due to "+response.message(), Toast.LENGTH_SHORT).show();
            Log.d("RECEIPT","Read receipt error due to"+response.message());
            }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Toast.makeText(MessageFullReadActivity.this, "Read receipt error due to "+t.getMessage(), Toast.LENGTH_SHORT).show();
                Log.d("RECEIPT","Read receipt error due to"+t.getMessage());
            }
        });
        }

    }

}
