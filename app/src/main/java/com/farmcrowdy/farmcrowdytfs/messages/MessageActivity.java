package com.farmcrowdy.farmcrowdytfs.messages;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.farmcrowdy.farmcrowdytfs.ApplicationInstance;
import com.farmcrowdy.farmcrowdytfs.DirectPojo.FarmGroupPojo;
import com.farmcrowdy.farmcrowdytfs.DirectPojo.MessageList;
import com.farmcrowdy.farmcrowdytfs.DirectPojo.MessagePojo;
import com.farmcrowdy.farmcrowdytfs.MyEndpoint;
import com.farmcrowdy.farmcrowdytfs.R;
import com.farmcrowdy.farmcrowdytfs.engine.TinyDB;
import com.raizlabs.android.dbflow.sql.language.Delete;
import com.raizlabs.android.dbflow.sql.language.SQLite;

import java.util.ArrayList;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MessageActivity extends AppCompatActivity {
    RecyclerView recyclerView;
    ProgressBar ppr;
    TextView tsp;
    SwipeRefreshLayout swipeRefreshLayout;
    int  offlineLoaded = 0;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.message_main_layout);
        ppr = findViewById(R.id.ppr);
        tsp = findViewById(R.id.tsp);
        swipeRefreshLayout = findViewById(R.id.swipeMessage);

        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitleTextColor(Color.WHITE);
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        recyclerView  = findViewById(R.id.messageRecycler);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setHasFixedSize(true);
        loadLocallyFirst();
        doGetMessagesOnline();

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                doGetMessagesOnline();
            }
        });

        }
        private void doGetMessagesOnline() {
            MyEndpoint myEndpoint = ApplicationInstance.getRetrofit().create(MyEndpoint.class);
            int tfs_id = new TinyDB(ApplicationInstance.getContext()).getInt("user_id", 1);
            Call<MessageList> responseBodyCall = myEndpoint.getMessageByTFS(2);
            responseBodyCall.enqueue(new Callback<MessageList>() {
                @Override
                public void onResponse(Call<MessageList> call, Response<MessageList> response) {
                    if(swipeRefreshLayout.isRefreshing()) swipeRefreshLayout.setRefreshing(false);
                    if(response.body().getMessage().size() > 0) {
                        ppr.setVisibility(View.GONE);
                        swipeRefreshLayout.setVisibility(View.VISIBLE);
                        //recyclerView.setAdapter(new MessageAdapter(MessageActivity.this, response.body()));

                        saveOffline(response.body());
                    }
                    else {
                        tsp.setText("No messages yet.");
                    }

                }

                @Override
                public void onFailure(Call<MessageList> call, Throwable t) {
                    if(swipeRefreshLayout.isRefreshing()) swipeRefreshLayout.setRefreshing(false);
                    ppr.setVisibility(View.GONE);
                    Toast.makeText(MessageActivity.this, "Something went wrong due to:"+t.getMessage(), Toast.LENGTH_SHORT).show();
                    if(offlineLoaded == 0) tsp.setText("Please, check your internet connection and try again");
                }
            });

        }

        void saveOffline(MessageList messageList) {
            try{
                Delete.tables(MessageMyPojo.class);
                Log.d("DATBER", "Updated well");
            }
            catch (Exception e) {
                Log.d("DATBER", "My err: "+e.getMessage());
                e.printStackTrace();
            }

            for (MessageMyPojo messagePojo : messageList.getMessage()) {
                MessageMyPojo messagePojoMER = new MessageMyPojo();
                messagePojoMER.setMsg_id(messagePojo.getMsg_id());

                Log.d("deep", "id is called"+messagePojo.getMsg_id());

                messagePojoMER.setFarm_group(messagePojo.getFarm_group());
                messagePojoMER.setFarmername(messagePojo.getFarmername());
                messagePojoMER.setGender(messagePojo.getGender());

                messagePojoMER.setMessage_text(messagePojo.getMessage_text());
                messagePojoMER.setMobile(messagePojo.getMobile());
                messagePojoMER.setMsg_audiolink(messagePojo.getMsg_audiolink());
                messagePojoMER.setMsg_date(messagePojo.getMsg_date());
                messagePojoMER.setMsg_photolink(messagePojo.getMsg_photolink());
                messagePojoMER.setMsg_read(messagePojo.getMsg_read());
                messagePojoMER.setPropix(messagePojo.getPropix());
                messagePojoMER.setMsg_videolink(messagePojo.getMsg_videolink());
                messagePojoMER.save();
                Log.d("OFFLINE", "Messages saved offline");
            }

            loadLocallyFirst();





        }

        void loadLocallyFirst() {
        try {
            List<MessageMyPojo> messagePojoArrayList = SQLite.select().
                    from(MessageMyPojo.class).queryList();



            if (swipeRefreshLayout.isRefreshing()) swipeRefreshLayout.setRefreshing(false);
            ppr.setVisibility(View.GONE);
            swipeRefreshLayout.setVisibility(View.VISIBLE);
            ArrayList<MessageMyPojo> justArray = new ArrayList<>(messagePojoArrayList);
            MessageList messageList = new MessageList(justArray);
            recyclerView.setAdapter(new MessageAdapter(MessageActivity.this, messageList));
            offlineLoaded = 1;
            Log.d("OFFLINE", "Stuff loaded here now");
        }
        catch (Exception e) {
            e.printStackTrace();
            Log.d("OFFLINE", "Still giving while loading because "+e.getMessage());
        }
        }

    @Override
    protected void onPause() {
        super.onPause();

    }
}
