package com.farmcrowdy.farmcrowdytfs.addFarmer;

import android.graphics.Bitmap;
import android.net.Uri;
import android.os.StrictMode;
import android.util.Log;

import com.coremedia.iso.Hex;
import com.farmcrowdy.farmcrowdytfs.ApplicationInstance;
import com.farmcrowdy.farmcrowdytfs.DirectPojo.PagaResponsePojo;
import com.farmcrowdy.farmcrowdytfs.MyEndpoint;
import com.farmcrowdy.farmcrowdytfs.WithPhotoInterface;
import com.farmcrowdy.farmcrowdytfs.engine.LoadEffecient;
import com.farmcrowdy.farmcrowdytfs.engine.TinyDB;
import com.farmcrowdy.farmcrowdytfs.profile.FullFarmerInfoPojo;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.math.BigInteger;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SignatureException;
import java.util.Date;
import java.util.Formatter;
import java.util.Random;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

import okhttp3.Headers;
import okhttp3.Interceptor;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;
import retrofit2.http.Part;
import retrofit2.http.Path;

/**
 * Created by Oluwatobi on 5/25/2018.
 */

public class AddFarmerPresenterImpl implements AddFarmerPresenter {

    WithPhotoInterface withPhotoInterface;
    private static final String HMAC_SHA512 = "HmacSHA512";

    public AddFarmerPresenterImpl(WithPhotoInterface withPhotoInterface) {
        this.withPhotoInterface = withPhotoInterface;
    }

    @Override
    public void uploadPhoto(String filePath) {
        try {
            Bitmap high = new LoadEffecient().decodeSampledBitmapFromFile(filePath, LoadEffecient.maxHeight, LoadEffecient.maxWidth);
            File forHigh = new LoadEffecient().saveLocallyFromBitmapToTempFile(high, "bitmapHigh");
            Uri fileUri = Uri.fromFile(forHigh);
            RequestBody requestFile =
                    RequestBody.create(
                            MediaType.parse("*/*"), forHigh
                    );
            MultipartBody.Part body =
                    MultipartBody.Part.createFormData("pimage", "zipper.jpeg", requestFile);

        }
        catch (Exception e){

        }
    }


    @Override
    public void uploadFarmerDetails(final FullFarmerInfoPojo fullFarmerInfoPojo, String filePath) {
        try {
         Bitmap   high = new LoadEffecient().decodeSampledBitmapFromFile(filePath, LoadEffecient.maxHeight, LoadEffecient.maxWidth);
        File file = new LoadEffecient().saveLocallyFromBitmapToTempFile(high, "bitmapHigh");
        RequestBody requestFile = RequestBody.create(MediaType.parse("*"), file);


        String fileMinName = "abaPhoto"+new Date().getTime()+".jpg";

        MultipartBody.Part body =
                MultipartBody.Part.createFormData("pimage", fileMinName, requestFile);


        MyEndpoint myEndpoint = ApplicationInstance.getRetrofit().create(MyEndpoint.class);
        RequestBody fnameReq =  RequestBody.create(MediaType.parse("text/plain"), fullFarmerInfoPojo.getFname());
        RequestBody snameReq =  RequestBody.create(MediaType.parse("text/plain"), fullFarmerInfoPojo.getSname());
        RequestBody dobReq =  RequestBody.create(MediaType.parse("text/plain"), fullFarmerInfoPojo.getDob());
        RequestBody genderReq =  RequestBody.create(MediaType.parse("text/plain"), fullFarmerInfoPojo.getGender());
        RequestBody phoneReq =  RequestBody.create(MediaType.parse("text/plain"), fullFarmerInfoPojo.getPhone());
        RequestBody maritalStatusReq =  RequestBody.create(MediaType.parse("text/plain"), fullFarmerInfoPojo.getMarital_status());
        RequestBody cpReq =  RequestBody.create(MediaType.parse("text/plain"), fullFarmerInfoPojo.getCrop_proficiency());
        RequestBody incomeRangeReq =  RequestBody.create(MediaType.parse("text/plain"), fullFarmerInfoPojo.getIncome_range());
        RequestBody proImageReq =  RequestBody.create(MediaType.parse("text/plain"), fileMinName);
        RequestBody accuntReq =  RequestBody.create(MediaType.parse("text/plain"), String.valueOf(fullFarmerInfoPojo.getAcct_number()));
        RequestBody landBody =  RequestBody.create(MediaType.parse("text/plain"), fullFarmerInfoPojo.getLand_area_farmed());
        RequestBody commentReq =  RequestBody.create(MediaType.parse("text/plain"), fullFarmerInfoPojo.getComment());

        int tfs_id = new TinyDB(ApplicationInstance.getContext()).getInt("user_id", 1);
        Call<AddFarmerResponsePojo> call = myEndpoint.addFarmer(
                tfs_id, fnameReq,snameReq,dobReq,fullFarmerInfoPojo.getState_id(), fullFarmerInfoPojo.getLocal_id(),fullFarmerInfoPojo.getFarm_group_id(),
                fullFarmerInfoPojo.getFarm_location_id(), fullFarmerInfoPojo.getBank_id(), genderReq,phoneReq,maritalStatusReq,fullFarmerInfoPojo.getNumber_of_dependants(),
                fullFarmerInfoPojo.getAdd_labour(),cpReq,accuntReq, landBody,
                fullFarmerInfoPojo.getYears_of_experience(), incomeRangeReq,fullFarmerInfoPojo.getPrevious_training(), proImageReq,commentReq, body, fullFarmerInfoPojo.getAccessPin());
        call.enqueue(new Callback<AddFarmerResponsePojo>() {
            @Override
            public void onResponse(Call<AddFarmerResponsePojo> call, Response<AddFarmerResponsePojo> response) {
                if(response.isSuccessful()) {
                    withPhotoInterface.uploadPhotoSuccessful(null, null);
                    withPhotoInterface.successful(String.valueOf(response.body().getFarmerid()));
                    //doWalletRegistration(fullFarmerInfoPojo, response.body().getFarmerid() );
                }
                else {
                    Log.d("LIZZ", "due to "+response.code());
                    withPhotoInterface.failure(response.message());
                }
            }

            @Override
            public void onFailure(Call<AddFarmerResponsePojo> call, Throwable t) {
                withPhotoInterface.failure(t.getMessage());
            }
        });

        } catch (FileNotFoundException e) {
            e.printStackTrace();
            }

    }
    String sortPhoneNumberExtension(String initialNumber) {
        String finalString = "";
        if(initialNumber.substring(0,1).equals("0") ) {
            finalString = "+234"+initialNumber.substring(1);
        }
return  finalString;
    }
    String generateRandomReferenceNumber(String firstname) {
        String initial = "Farmcrowdy";
        int middle = new Random().nextInt(9999);
        String lastOne = firstname.substring(2);
        long timing = new Date().getTime();
        return String.valueOf(initial+middle+""+timing);
    }
    void doWalletRegistration(FullFarmerInfoPojo fullFarmerInfoPojo, final int farmerId) {
        try {
            String referenceNumber = generateRandomReferenceNumber(fullFarmerInfoPojo.getFname());
            String customerPrincipal = sortPhoneNumberExtension(fullFarmerInfoPojo.getPhone());
            String customerCredentials = "Farmcrowdy"+fullFarmerInfoPojo.getAccessPin();
            String customerFirstName = fullFarmerInfoPojo.getFname();
            String customerLastName = fullFarmerInfoPojo.getSname();
            String customerDateOfBirth = fullFarmerInfoPojo.getDobForPaga();

            String sBuilder = referenceNumber + customerPrincipal + customerCredentials + ApplicationInstance.hashKey;

            Log.d("HASH", "has is : "+ sBuilder);


            final String hmac = hashSHA512(sBuilder).toString();
            Log.d("HASH", "has is : "+ hmac);
            Gson gson = new GsonBuilder()
                    .setLenient()
                    .create();

            OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
            httpClient.addInterceptor(new Interceptor() {
                @Override
                public okhttp3.Response intercept(Interceptor.Chain chain) throws IOException {
                    Request original = chain.request();

                    // Request customization: add request headers
                    Request.Builder requestBuilder = original.newBuilder()
                            .addHeader("Content-Type", "application/json")
                            .addHeader("principal", ApplicationInstance.pagaPrincipal)
                            .addHeader("credentials",ApplicationInstance.pagaCredentials)
                            .addHeader("hash", hmac);

                    Request request = requestBuilder.build();

                    return chain.proceed(request);
                }
            });

            OkHttpClient client = httpClient.build();
            Retrofit retrofit = new Retrofit.Builder()
                    .client(client)
                    .addConverterFactory(ScalarsConverterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .baseUrl("https://mypaga.com/")
                    .build();



            MyEndpoint myEndpoint = retrofit.create(MyEndpoint.class);
            // Call<PagaResponsePojo>  caller = myEndpoint.buyAirtimePaga(new PagaBuyAirtime(referenceNumber, "en"));
            Call<PagaResponsePojo> caller = myEndpoint.registerFarmerOnPaga(new PagaAddBodyPojo(referenceNumber,customerPrincipal,customerCredentials,customerFirstName,customerLastName,customerDateOfBirth));
            caller.enqueue(new Callback<PagaResponsePojo>() {
                @Override
                public void onResponse(Call<PagaResponsePojo> call, Response<PagaResponsePojo> response) {
                    if(response.isSuccessful()) {
                        if(response.body().getResponseCode() == -1) {
                            withPhotoInterface.successful(String.valueOf(farmerId));
                        }
                        else {
                            withPhotoInterface.failure("Farmer registered, but mobile did not due to "+response.body().getMessage());
                        }

                    }
                    else  Log.d("pagatech", "not good still..."+response.message());
                    //withPhotoInterface.failure("error due to: "+ response.code());
                }

                @Override
                public void onFailure(Call<PagaResponsePojo> call, Throwable t) {

                }
            });
        }catch (Exception e) {
            e.printStackTrace();
            Log.e("Pagatech", "error exception due to "+e.getMessage());
        }


    }


    public static Object hashSHA512(String message) {

        try {
            String toReturn = null;
            try {
                MessageDigest digest = MessageDigest.getInstance("SHA-512");
                digest.reset();
                digest.update(message.getBytes("utf8"));
                toReturn = String.format("%040x", new BigInteger(1, digest.digest()));
            } catch (Exception e) {
                e.printStackTrace();
            }

            return toReturn;

        } catch(Exception e) {
            return null;
        }
    }
}
