package com.farmcrowdy.farmcrowdytfs.addFarmer;

public class AddFarmerResponsePojo {

    String success,message;
    int farmerid;

    public String getSuccess() {
        return success;
    }

    public String getMessage() {
        return message;
    }

    public int getFarmerid() {
        return farmerid;
    }
}
