package com.farmcrowdy.farmcrowdytfs.addFarmer;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.widget.AdapterView;
import android.widget.CompoundButton;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.Toast;

import com.farmcrowdy.farmcrowdytfs.DirectPojo.FingerprintDecidePojo;
import com.farmcrowdy.farmcrowdytfs.MainActivity;
import com.farmcrowdy.farmcrowdytfs.R;
import com.farmcrowdy.farmcrowdytfs.WithPhotoInterface;
import com.farmcrowdy.farmcrowdytfs.profile.FullFarmerInfoPojo;
import com.farmcrowdy.farmcrowdytfs.profile.ProfileActivity;
import com.fgtit.reader.BluetoothReaderTest;
import com.rafakob.floatingedittext.FloatingEditText;

import java.util.Random;

public class ThreeAddFarmer extends AppCompatActivity implements WithPhotoInterface {
    FloatingEditText landAreaFarmedEdit, yearsOfExperience,commentEdit, accessPinEdit;
    Spinner amountRangeSpinner;
    Switch hasPreviousTraniningSwitch;
    String amountRangeString;

    int previousTrainingInt, pinValue;
    AddFarmerPresenter presenter;

    ProgressDialog progressDialog;
    FullFarmerInfoPojo fullFarmerInfoPojo;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_farmer_step_three_layout);

        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitleTextColor(Color.WHITE);
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Uploading farmer's details");
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);
        presenter = new AddFarmerPresenterImpl(this);

        fullFarmerInfoPojo = getIntent().getExtras().getParcelable("data");
        landAreaFarmedEdit = findViewById(R.id.farmed_previously);
        yearsOfExperience = findViewById(R.id.years_of_experience_edit);
        commentEdit = findViewById(R.id.comment_editText);
        amountRangeSpinner = findViewById(R.id.income_range_spinner);
        accessPinEdit = findViewById(R.id.pin_editText);
        int generatedPIN = new Random().nextInt(9999);
        accessPinEdit.setText(String.valueOf(generatedPIN));
        accessPinEdit.setEnabled(false);
        hasPreviousTraniningSwitch = findViewById(R.id.previous_trained_switch);


        hasPreviousTraniningSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                previousTrainingInt = b ? 1 : 0;

            }
        });
        hasPreviousTraniningSwitch.setChecked(true);
        amountRangeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                amountRangeString = adapterView.getItemAtPosition(i).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        findViewById(R.id.fab).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(!TextUtils.isEmpty(commentEdit.getText()) && !TextUtils.isEmpty(yearsOfExperience.getText())
                        && !TextUtils.isEmpty(landAreaFarmedEdit.getText()) && TextUtils.getTrimmedLength(accessPinEdit.getText()) == 4  ) {
                    progressDialog.show();
                    fullFarmerInfoPojo.setPrevious_training(previousTrainingInt);
                    fullFarmerInfoPojo.setComment(commentEdit.getText());
                    fullFarmerInfoPojo.setYears_of_experience(Integer.valueOf(yearsOfExperience.getText()));
                    fullFarmerInfoPojo.setLand_area_farmed(landAreaFarmedEdit.getText());
                    fullFarmerInfoPojo.setIncome_range(amountRangeString);

                    fullFarmerInfoPojo.setAccessPin(Integer.valueOf(accessPinEdit.getText()));

                    presenter.uploadFarmerDetails(fullFarmerInfoPojo, fullFarmerInfoPojo.getPro_image());

                }
                else {
                    Toast.makeText(ThreeAddFarmer.this, "Enter some fields here, including 4 digits access pin", Toast.LENGTH_SHORT).show();
                }

            }
        });

    }


    @Override
    public void successful(final String farmerId) {
        progressDialog.dismiss();
        progressDialog.cancel();
        Toast.makeText(this, "Adding of farmer successful", Toast.LENGTH_SHORT).show();

        //Intent intent = new Intent(ThreeAddFarmer.this, BluetoothReaderTest.class);
        //Toast.makeText(ThreeAddFarmer.this, "farmer id is"+farmerId, Toast.LENGTH_SHORT).show();
        //intent.putExtra("data", new FingerprintDecidePojo(0, Integer.valueOf(farmerId), "reg",0));

        Intent intent = new Intent(ThreeAddFarmer.this, MainActivity.class);
        startActivity(intent);
        finishAffinity();



    }

    @Override
    public void failure(String error) {
        if(error.contains("No address")) error = "No internet connection found";
        progressDialog.dismiss();
        progressDialog.cancel();
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Due to "+error);
        builder.setTitle("Upload failed");
        builder.show();
    }

    @Override
    public void uploadPhotoSuccessful(String urlHigh, String urlLow) {
progressDialog.setMessage("Creating mobile wallet for "+fullFarmerInfoPojo.getFname());
    }
}
