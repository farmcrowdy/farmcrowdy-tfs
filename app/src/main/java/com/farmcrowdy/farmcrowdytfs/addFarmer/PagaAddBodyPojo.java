package com.farmcrowdy.farmcrowdytfs.addFarmer;

import okhttp3.RequestBody;
import retrofit2.http.Part;

public class PagaAddBodyPojo {
    String referenceNumber;
    String customerPrincipal;
    String  customerCredentials;
    String customerFirstName;
    String customerLastName;
    String  customerDateOfBirth;

    public PagaAddBodyPojo(String referenceNumber, String customerPrincipal, String customerCredentials, String customerFirstName, String customerLastName, String customerDateOfBirth) {
        this.referenceNumber = referenceNumber;
        this.customerPrincipal = customerPrincipal;
        this.customerCredentials = customerCredentials;
        this.customerFirstName = customerFirstName;
        this.customerLastName = customerLastName;
        this.customerDateOfBirth = customerDateOfBirth;
    }

}
