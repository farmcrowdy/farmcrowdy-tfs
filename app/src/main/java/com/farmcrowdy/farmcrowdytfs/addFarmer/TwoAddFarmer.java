package com.farmcrowdy.farmcrowdytfs.addFarmer;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.Toast;

import com.farmcrowdy.farmcrowdytfs.DirectPojo.BanksJojo;
import com.farmcrowdy.farmcrowdytfs.DirectPojo.CropPojo;
import com.farmcrowdy.farmcrowdytfs.R;
import com.farmcrowdy.farmcrowdytfs.profile.FullFarmerInfoPojo;
import com.rafakob.floatingedittext.FloatingEditText;
import com.raizlabs.android.dbflow.sql.language.SQLite;

import java.util.ArrayList;
import java.util.List;

public class TwoAddFarmer extends AppCompatActivity {
    Switch hasBankAccount;
    boolean itHasBank = true;
    Spinner listOfBanks;
    ArrayList<String> bankListString;
    ArrayList<Integer> bankListInt;
    int bank_selected;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_farmer_step_two_layout);
        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitleTextColor(Color.WHITE);
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        final FullFarmerInfoPojo fullFarmerInfoPojo = getIntent().getExtras().getParcelable("data");
        hasBankAccount = findViewById(R.id.hasBankAccountSpinner);
        hasBankAccount.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                itHasBank = b;
            }
        });
        listOfBanks = findViewById(R.id.banks_spinner);
        List<BanksJojo> groupList = SQLite.select().
                from(BanksJojo.class).queryList();

        bankListString = new ArrayList<>();
        bankListInt = new ArrayList<>();

        for(int i=0; i<groupList.size(); i++){
            bankListString.add(groupList.get(i).getBank_name());
            bankListInt.add(groupList.get(i).getBank_id());
        }
        ArrayAdapter<String> dataAdapterState = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, bankListString);
        dataAdapterState.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        listOfBanks.setAdapter(dataAdapterState);
        listOfBanks.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                bank_selected = bankListInt.get(i);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        final FloatingEditText accountNumberEdit = findViewById(R.id.account_number_editText);

        findViewById(R.id.fab).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(itHasBank){
                    if(!TextUtils.isEmpty(accountNumberEdit.getText()) && TextUtils.getTrimmedLength(accountNumberEdit.getText()) >9){
                        fullFarmerInfoPojo.setAcct_number(accountNumberEdit.getText());
                        fullFarmerInfoPojo.setBank_id(bank_selected);
                        Intent intent = new Intent(TwoAddFarmer.this, ThreeAddFarmer.class);
                        intent.putExtra("data", fullFarmerInfoPojo);
                        startActivity(intent);
                    }
                    else {
                        Toast.makeText(TwoAddFarmer.this, "Please enter the correct account number", Toast.LENGTH_SHORT).show();
                    }
                }
                else {
                    fullFarmerInfoPojo.setAcct_number("0");
                    fullFarmerInfoPojo.setBank_id(bank_selected);
                    Intent intent = new Intent(TwoAddFarmer.this, ThreeAddFarmer.class);
                    intent.putExtra("data", fullFarmerInfoPojo);
                    startActivity(intent);
                }


            }
        });

    }
}
