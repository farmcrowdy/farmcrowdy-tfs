package com.farmcrowdy.farmcrowdytfs.addFarmer;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.farmcrowdy.farmcrowdytfs.ApplicationInstance;
import com.farmcrowdy.farmcrowdytfs.DirectPojo.CropPojo;
import com.farmcrowdy.farmcrowdytfs.DirectPojo.FarmGroupPojo;
import com.farmcrowdy.farmcrowdytfs.DirectPojo.FarmLocationPojo;
import com.farmcrowdy.farmcrowdytfs.DirectPojo.FarmerPojo;
import com.farmcrowdy.farmcrowdytfs.DirectPojo.LocalGovtPojo;
import com.farmcrowdy.farmcrowdytfs.DirectPojo.LocalGovtPojo_Table;
import com.farmcrowdy.farmcrowdytfs.DirectPojo.StatePojo;
import com.farmcrowdy.farmcrowdytfs.ListPojo.FarmGroupListPojo;
import com.farmcrowdy.farmcrowdytfs.MyEndpoint;
import com.farmcrowdy.farmcrowdytfs.R;
import com.farmcrowdy.farmcrowdytfs.engine.LoadEffecient;
import com.farmcrowdy.farmcrowdytfs.profile.FullFarmerInfoPojo;
import com.novachevskyi.datepicker.CalendarDatePickerDialog;
import com.rafakob.floatingedittext.FloatingEditText;
import com.raizlabs.android.dbflow.sql.language.Delete;
import com.raizlabs.android.dbflow.sql.language.SQLite;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OneAddFarmer extends AppCompatActivity implements View.OnClickListener{
    FloatingEditText firstnameEdit, lastnameEdit, phoneNumberEdit, noOfDependantsEdit;
    RelativeLayout dob;
    TextView dobText;
    Spinner genderspinner, maritalStatus, cropSpinner, locationSpinner, farmgroup_spinner, stateSpinner, localGovtSpinner;

    String genderString, maritalString, cropString;
    int cropInt, locationInt,farmGroupInt,stateInt, govtInt;

    private static final String DATE_PICKER_TAG = "tag";

    ArrayList<String> cropListString = new ArrayList<>();
    ArrayList<Integer> cropListInt = new ArrayList<>();

    ArrayList<String> locationListString = new ArrayList<>();
    ArrayList<Integer> locationListInt = new ArrayList<>();

    ArrayList<String> farmGroupListString = new ArrayList<>();
    ArrayList<Integer> farmGroupListInt = new ArrayList<>();

    ArrayList<String> stateListString = new ArrayList<>();
    ArrayList<Integer> stateListInt = new ArrayList<>();

    ArrayList<String> localGovtListString = new ArrayList<>();
    ArrayList<Integer> localGovtListInt = new ArrayList<>();
    private int mYear, mMonth, mDay;
    String imageList="", dateForPaga;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_new_farmer);

        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitleTextColor(Color.WHITE);
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });


        firstnameEdit = findViewById(R.id.firstname_editText);
        lastnameEdit = findViewById(R.id.lastname_editText);
        phoneNumberEdit = findViewById(R.id.phoneNumber_editText);
        noOfDependantsEdit = findViewById(R.id.dependants_editText);
        dob = findViewById(R.id.choose_date_layout);
        dobText = findViewById(R.id.edit_dob);
        genderspinner = findViewById(R.id.genderSpinner);
        cropSpinner = findViewById(R.id.proficiency_spinner);
        locationSpinner = findViewById(R.id.farm_location_spinner);
        farmgroup_spinner = findViewById(R.id.farm_group_spinner);
        stateSpinner = findViewById(R.id.state_spinner);
        maritalStatus = findViewById(R.id.spinnerMaritalStatus);
        localGovtSpinner = findViewById(R.id.LGA_spinner);
        dob.setOnClickListener(this);

        getAddings();
        setupAllSpinners();
        findViewById(R.id.fab).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!TextUtils.isEmpty(firstnameEdit.getText()) && !TextUtils.isEmpty(lastnameEdit.getText()) && !TextUtils.isEmpty(phoneNumberEdit.getText())
                        && !TextUtils.isEmpty(noOfDependantsEdit.getText()) && !TextUtils.isEmpty(dobText.getText()) && !imageList.isEmpty()){

                    if(TextUtils.getTrimmedLength(phoneNumberEdit.getText()) ==11) {
                        FullFarmerInfoPojo fullFarmerInfoPojo = new FullFarmerInfoPojo();
                        fullFarmerInfoPojo.setFname(firstnameEdit.getText());
                        fullFarmerInfoPojo.setSname(lastnameEdit.getText());
                        fullFarmerInfoPojo.setPhone(phoneNumberEdit.getText());
                        fullFarmerInfoPojo.setNumber_of_dependants(Integer.valueOf(noOfDependantsEdit.getText()));
                        fullFarmerInfoPojo.setDob(dobText.getText().toString());
                        fullFarmerInfoPojo.setGender(genderString);
                        fullFarmerInfoPojo.setCrop_proficiency(cropString);
                        fullFarmerInfoPojo.setFarm_location_id(locationInt);
                        fullFarmerInfoPojo.setFarm_group_id(farmGroupInt);
                        fullFarmerInfoPojo.setState_id(stateInt);
                        fullFarmerInfoPojo.setMarital_status(maritalString);
                        fullFarmerInfoPojo.setLocal_id(govtInt);
                        fullFarmerInfoPojo.setPro_image(imageList);
                        fullFarmerInfoPojo.setDobForPaga(dateForPaga);

                        Intent intent = new Intent(OneAddFarmer.this, TwoAddFarmer.class);
                        intent.putExtra("data", fullFarmerInfoPojo);
                        startActivity(intent);
                    }
                    else {
                        Toast.makeText(OneAddFarmer.this, "Phone number is incorrect", Toast.LENGTH_SHORT).show();
                    }

                }
                else {
                    Toast.makeText(OneAddFarmer.this, "Please fill in all fields here including taking a photo", Toast.LENGTH_SHORT).show();
                }


            }
        });

        findViewById(R.id.takeUploadPhoto).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CropImage.activity()
                        .setGuidelines(CropImageView.Guidelines.ON)
                        .start(OneAddFarmer.this);
            }
        });


    }
    private void setupAllSpinners() {
        genderspinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                genderString = adapterView.getItemAtPosition(i).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        maritalStatus.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                maritalString = adapterView.getItemAtPosition(i).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });


        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, cropListString);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        cropSpinner.setAdapter(dataAdapter);

        ArrayAdapter<String> dataAdapterLocation = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, locationListString);
        dataAdapterLocation.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        locationSpinner.setAdapter(dataAdapterLocation);

        ArrayAdapter<String> dataAdapteFarmGroup = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, farmGroupListString);
        dataAdapteFarmGroup.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        farmgroup_spinner.setAdapter(dataAdapteFarmGroup);

        ArrayAdapter<String> dataAdapterState = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, stateListString);
        dataAdapterState.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        stateSpinner.setAdapter(dataAdapterState);



        stateSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                stateInt = stateListInt.get(i);
                if(i!=0) {


                List<LocalGovtPojo> govtList = SQLite.select().from(LocalGovtPojo.class).where(LocalGovtPojo_Table.state_id.is(stateInt)).
                        queryList();
                    localGovtListString = new ArrayList<>();
                for(int p=0; p<govtList.size(); p++) {
                    localGovtListString.add(govtList.get(p).getLocal_name());
                    localGovtListInt.add(govtList.get(p).getLocal_id());
                }

                    ArrayAdapter<String> dataAdapterGovt = new ArrayAdapter<String>(OneAddFarmer.this,
                            android.R.layout.simple_spinner_item, localGovtListString);
                    dataAdapterGovt.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    localGovtSpinner.setAdapter(dataAdapterGovt);

                    localGovtSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                            govtInt = localGovtListInt.get(i);
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {

                        }
                    });

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });



        cropSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if(i!=0) {
                    cropInt = cropListInt.get(i);
                    cropString = adapterView.getItemAtPosition(i).toString();
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        locationSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if(i!=0) {
                    locationInt = locationListInt.get(i);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        farmgroup_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if(i!=0) {
                    farmGroupInt = farmGroupListInt.get(i);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });



    }

    private void getAddings() {
        List<CropPojo> groupList = SQLite.select().
                from(CropPojo.class).queryList();

        cropListString = new ArrayList<>();
        cropListInt = new ArrayList<>();

        for(int i=0; i<groupList.size(); i++){
            cropListString.add(groupList.get(i).getCrop_name());
            cropListInt.add(groupList.get(i).getCrop_id());
            Log.d("DO_TESTING", "this is an updated for number of crops"+cropListString.get(i));
        }


        List<FarmLocationPojo> locList = SQLite.select().from(FarmLocationPojo.class).queryList();
        for(int i=0; i<locList.size(); i++) {
            locationListString.add(locList.get(i).getLocation());
            locationListInt.add(locList.get(i).getLocation_id());
            Log.d("DO_TESTING", "this is an updated for number of location"+locationListString.get(i));
        }

        List<FarmGroupPojo> fgList = SQLite.select().from(FarmGroupPojo.class).queryList();
        for(int i=0; i<fgList.size(); i++){
            farmGroupListString.add(fgList.get(i).getGroup_name());
            farmGroupListInt.add(fgList.get(i).getGroup_id());
            Log.d("DO_TESTING", "this is an updated for number of farmgroup"+farmGroupListString.get(i));
        }

        List<StatePojo> stateList = SQLite.select().from(StatePojo.class).queryList();
        for(int i=0; i<stateList.size(); i++){
            stateListString.add(stateList.get(i).getName());
            stateListInt.add(stateList.get(i).getState_id());
            Log.d("DO_TESTING", "this is an updated for number of states"+stateListString.get(i));
        }
        refreshGroupList();



    }

    private void showDatePicker() {
        final Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);


        DatePickerDialog datePickerDialog = new DatePickerDialog(this,
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {

                        dateForPaga = year+"-"+monthOfYear+"-"+dayOfMonth+"T"+"00:00:00";
                        dobText.setText(String.valueOf(dayOfMonth+" / ")+monthOfYear+" / "+ year);

                    }
                }, mYear, mMonth, mDay);
        datePickerDialog.show();


    }

    CalendarDatePickerDialog.OnDateSetListener dateSetListener =
            new CalendarDatePickerDialog.OnDateSetListener() {
                @Override
                public void onDateSet(CalendarDatePickerDialog dialog, int year, int monthOfYear,
                                      int dayOfMonth) {
                    dateForPaga = year+"-"+monthOfYear+"-"+dayOfMonth+"T"+"00:00:00";
                    dobText.setText(String.valueOf(dayOfMonth+" / ")+monthOfYear+" / "+ year);
                }
            };

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.choose_date_layout:
                showDatePicker();
            break;

        }
    }
    @Override
    protected void onActivityResult(int requestCode, final int resultCode, Intent data) {
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                Uri resultUri = result.getUri();
                imageList = resultUri.getPath();
                if (!imageList.equals("empty")) {
                    Bitmap high;
                    try {
                        high = new LoadEffecient().decodeSampledBitmapFromFile(imageList, 200, 200);
                        File file = new LoadEffecient().saveLocallyFromBitmapToTempFile(high, "bitmapHigh");
                        CircleImageView circleImageView = findViewById(R.id.selected_photo);
                        //Picasso.with(this).load(imageList).resize(200,200).into(circleImageView);
                       // circleImageView.setImageURI(Uri.parse(imageList));
                        circleImageView.setImageURI(Uri.parse(file.getPath()));

                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    }
                }
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
            }
        }
        /*List<Image> images = ImagePicker.getImages(data);
        if(images!=null) {
            imageList = images.get(0).getPath();
            CircleImageView circleImageView = findViewById(R.id.selected_photo);
            //Picasso.with(this).load(imageList).resize(200,200).into(circleImageView);
            circleImageView.setImageURI(Uri.parse(imageList));
            super.onActivityResult(requestCode, resultCode, data);
        }
        */
    }
    void refreshGroupList(){
        MyEndpoint myEndpoint = ApplicationInstance.getRetrofit().create(MyEndpoint.class);
            Call<FarmGroupListPojo> farmGroupListPojoCall = myEndpoint.getAllFarmGroups();
            farmGroupListPojoCall.enqueue(new Callback<FarmGroupListPojo>() {
                @Override
                public void onResponse(Call<FarmGroupListPojo> call, Response<FarmGroupListPojo> response) {
                    //save locally

                    if(response.isSuccessful()){
                        FarmGroupPojo farmGroupPojo2  = new FarmGroupPojo();
                        Delete.table(FarmGroupPojo.class);
                        farmGroupPojo2.setGroup_id(0);
                        farmGroupPojo2.setGroup_name("Select farm group");
                        farmGroupPojo2.save();

                        for(FarmGroupPojo farmGroupPojo: response.body().getMessage()){
                            FarmGroupPojo farmGroupPojo1  = new FarmGroupPojo();
                            farmGroupPojo1.setGroup_id(farmGroupPojo.getGroup_id());
                            farmGroupPojo1.setGroup_name(farmGroupPojo.getGroup_name());
                            farmGroupPojo1.save();
                        }
                        List<FarmGroupPojo> fgList = SQLite.select().from(FarmGroupPojo.class).queryList();
                        farmGroupListString = new ArrayList<>();
                        farmGroupListInt = new ArrayList<>();
                        for(int i=0; i<fgList.size(); i++){
                            farmGroupListString.add(fgList.get(i).getGroup_name());
                            farmGroupListInt.add(fgList.get(i).getGroup_id());
                        }

                        ArrayAdapter<String> dataAdapteFarmGroup = new ArrayAdapter<String>(OneAddFarmer.this,
                                android.R.layout.simple_spinner_item, farmGroupListString);
                        dataAdapteFarmGroup.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        farmgroup_spinner.setAdapter(dataAdapteFarmGroup);
                    }
                    else {
                        //Delete all stuff in localdatabase
                    }
                }

                @Override
                public void onFailure(Call<FarmGroupListPojo> call, Throwable t) {

                }
            });

    }

}
