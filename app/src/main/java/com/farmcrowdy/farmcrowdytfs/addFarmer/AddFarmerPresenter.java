package com.farmcrowdy.farmcrowdytfs.addFarmer;

import com.farmcrowdy.farmcrowdytfs.profile.FullFarmerInfoPojo;

/**
 * Created by Oluwatobi on 5/25/2018.
 */

public interface AddFarmerPresenter {
    void uploadPhoto(String path);
    void uploadFarmerDetails(FullFarmerInfoPojo fullFarmerInfoPojo, String path);
}
