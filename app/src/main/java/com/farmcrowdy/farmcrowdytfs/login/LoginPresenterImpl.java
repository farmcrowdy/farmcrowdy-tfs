package com.farmcrowdy.farmcrowdytfs.login;

import android.text.TextUtils;

import com.farmcrowdy.farmcrowdytfs.ApplicationInstance;
import com.farmcrowdy.farmcrowdytfs.DirectPojo.BanksJojo;
import com.farmcrowdy.farmcrowdytfs.DirectPojo.CropPojo;
import com.farmcrowdy.farmcrowdytfs.DirectPojo.FarmGroupPojo;
import com.farmcrowdy.farmcrowdytfs.DirectPojo.FarmLocationPojo;
import com.farmcrowdy.farmcrowdytfs.DirectPojo.LocalGovtPojo;
import com.farmcrowdy.farmcrowdytfs.DirectPojo.StatePojo;
import com.farmcrowdy.farmcrowdytfs.ListPojo.CropListPojo;
import com.farmcrowdy.farmcrowdytfs.ListPojo.FarmGroupListPojo;
import com.farmcrowdy.farmcrowdytfs.ListPojo.FarmLocationListPojo;
import com.farmcrowdy.farmcrowdytfs.ListPojo.GetBanksListPojo;
import com.farmcrowdy.farmcrowdytfs.ListPojo.LocalGovtListPojo;
import com.farmcrowdy.farmcrowdytfs.ListPojo.StateListPojo;
import com.farmcrowdy.farmcrowdytfs.LoginPojo;
import com.farmcrowdy.farmcrowdytfs.MyEndpoint;
import com.farmcrowdy.farmcrowdytfs.NoPhotoInterface;
import com.farmcrowdy.farmcrowdytfs.engine.TinyDB;
import com.raizlabs.android.dbflow.sql.language.Delete;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Oluwatobi on 5/25/2018.
 */

public class LoginPresenterImpl implements LoginPresenter {
    NoPhotoInterface noPhotoInterface;
    MyEndpoint myEndpoint = ApplicationInstance.getRetrofit().create(MyEndpoint.class);
    public LoginPresenterImpl(NoPhotoInterface noPhotoInterface) {
        this.noPhotoInterface = noPhotoInterface;
    }

    @Override
    public void loginNow(final String email, String password) {
        MyEndpoint myEndpoint  = ApplicationInstance.getRetrofit().create(MyEndpoint.class);

        RequestBody emailRequest  =  RequestBody.create(MediaType.parse("text/plain"), email.trim());
        RequestBody passwordRequest =  RequestBody.create(MediaType.parse("text/plain"), password);
        Call<LoginPojo> bodyCall = myEndpoint.login(emailRequest, passwordRequest);
        bodyCall.enqueue(new Callback<LoginPojo>() {
            @Override
            public void onResponse(Call<LoginPojo> call, Response<LoginPojo> response) {
                if(response.isSuccessful()){

                   if(!TextUtils.isEmpty(new TinyDB(ApplicationInstance.getContext()).getString("user_email").trim())) {
                    noPhotoInterface.successful();
                    }

                    else {
                       int user_id = response.body().getMessage();

                       new TinyDB(ApplicationInstance.getContext()).putInt("user_id", user_id);
                       new TinyDB(ApplicationInstance.getContext()).putString("user_email", email.trim());
                       pullInState();
                   }
                }
                else {
                    noPhotoInterface.failure("Invalid credentials, please check email or password");
                }
            }

            @Override
            public void onFailure(Call<LoginPojo> call, Throwable t) {
            noPhotoInterface.failure("No internet connection found");
            }
        });
    }
    private void pullInState(){

        Call<StateListPojo> stateListPojoCall = myEndpoint.getAllStates();
        stateListPojoCall.enqueue(new Callback<StateListPojo>() {
            @Override
            public void onResponse(Call<StateListPojo> call, Response<StateListPojo> response) {
                //save locally


                if(response.isSuccessful()){
                    StatePojo statePojo2 = new StatePojo();
                    statePojo2.setName("Select state");
                    statePojo2.setState_id(0);
                    statePojo2.save();
                    for(StatePojo statePojo: response.body().getMessage()){
                        StatePojo statePojo1 = new StatePojo();
                        statePojo1.setState_id(statePojo.getState_id());
                        statePojo1.setName(statePojo.getName());
                        statePojo1.save();
                    }
                    pullInLocalGovt();

                }
                else {
                    noPhotoInterface.failure(response.message());
                    //Delete all stuff in localdatabase
                }
            }

            @Override
            public void onFailure(Call<StateListPojo> call, Throwable t) {
                noPhotoInterface.failure(t.getMessage());
            }
        });

    }
    private void pullInLocalGovt() {
        Call<LocalGovtListPojo> localGovtListPojoCall = myEndpoint.getAllLocalGov();
        localGovtListPojoCall.enqueue(new Callback<LocalGovtListPojo>() {
            @Override
            public void onResponse(Call<LocalGovtListPojo> call, Response<LocalGovtListPojo> response) {
                //save locally

                if(response.isSuccessful()){
                    LocalGovtPojo localGovtPojo2 = new LocalGovtPojo();
                    localGovtPojo2.setLocal_id(0);
                    localGovtPojo2.setState_id(0);
                    localGovtPojo2.setLocal_name("Select local govt");
                    localGovtPojo2.save();
                    for(LocalGovtPojo localGovtPojo: response.body().getMessage()){
                        LocalGovtPojo localGovtPojo1 = new LocalGovtPojo();
                        localGovtPojo1.setLocal_id(localGovtPojo.getLocal_id());
                        localGovtPojo1.setState_id(localGovtPojo.getState_id());
                        localGovtPojo1.setLocal_name(localGovtPojo.getLocal_name());
                        localGovtPojo1.save();
                    }
                    pullInBanks();
                }
                else {
                    //Delete all stuff in localdatabase
                    noPhotoInterface.failure(response.message());
                }
            }

            @Override
            public void onFailure(Call<LocalGovtListPojo> call, Throwable t) {
                noPhotoInterface.failure(t.getMessage());
            }
        });
    }
    private void pullInBanks() {
        Call<GetBanksListPojo> banksListPojoCall  = myEndpoint.getAllBanks();
        banksListPojoCall.enqueue(new Callback<GetBanksListPojo>() {
            @Override
            public void onResponse(Call<GetBanksListPojo> call, Response<GetBanksListPojo> response) {
                //save Locally

                if(response.isSuccessful()){
                    BanksJojo banksJojo2 = new BanksJojo();
                    banksJojo2.setBank_id(0);
                    banksJojo2.setBank_name("Select bank");
                    banksJojo2.save();
                    for(BanksJojo banksJojo: response.body().getMessage()){
                        BanksJojo banksJojo1 = new BanksJojo();
                        banksJojo1.setBank_id(banksJojo.getBank_id());
                        banksJojo1.setBank_name(banksJojo.getBank_name());
                        banksJojo1.save();

                    }
                    pullInCrops();
                }
                else {
                    //Delete all stuff in localdatabase
                    noPhotoInterface.failure(response.message());
                }
            }

            @Override
            public void onFailure(Call<GetBanksListPojo> call, Throwable t) {
                noPhotoInterface.failure(t.getMessage());
            }
        });

    }
    private void pullInCrops(){
        Call<CropListPojo> cropListPojoCall = myEndpoint.getAllCrops();
        cropListPojoCall.enqueue(new Callback<CropListPojo>() {
            @Override
            public void onResponse(Call<CropListPojo> call, Response<CropListPojo> response) {
                //save locally


                if(response.isSuccessful()){
                    CropPojo cropPojo2 = new CropPojo();
                    cropPojo2.setCrop_id(0);
                    cropPojo2.setCrop_name("Select farm type");
                    cropPojo2.save();
                    for(CropPojo cropPojo : response.body().getMessage()){
                        CropPojo cropPojo1 = new CropPojo();
                        cropPojo1.setCrop_id(cropPojo.getCrop_id());
                        cropPojo1.setCrop_name(cropPojo.getCrop_name());
                        cropPojo1.save();
                    }
                    pullInFarmLocations();
                }
                else {
                    //Delete all stuff in localdatabase
                    noPhotoInterface.failure(response.message());
                }
            }

            @Override
            public void onFailure(Call<CropListPojo> call, Throwable t) {
                noPhotoInterface.failure(t.getMessage());
            }
        });

    }
    private void pullInFarmLocations(){
        Call<FarmLocationListPojo> locationListPojoCall = myEndpoint.getAllFarmLocations();
        locationListPojoCall.enqueue(new Callback<FarmLocationListPojo>() {
            @Override
            public void onResponse(Call<FarmLocationListPojo> call, Response<FarmLocationListPojo> response) {
                //save locally

                if(response.isSuccessful()){
                    FarmLocationPojo farmLocationPojo2 = new FarmLocationPojo();
                    farmLocationPojo2.setLocation("Select location");
                    farmLocationPojo2.setLocation_id(0);
                    farmLocationPojo2.save();
                    for(FarmLocationPojo farmLocationPojo: response.body().getMessage()){
                        FarmLocationPojo farmLocationPojo1 = new FarmLocationPojo();
                        farmLocationPojo1.setLocation_id(farmLocationPojo.getLocation_id());
                        farmLocationPojo1.setLocation(farmLocationPojo.getLocation());
                        farmLocationPojo.save();
                    }
                    pullInFarmGroups();
                }
                else {
                    //Delete all stuff in localdatabase
                    noPhotoInterface.failure(response.message());
                }
            }

            @Override
            public void onFailure(Call<FarmLocationListPojo> call, Throwable t) {
                noPhotoInterface.failure(t.getMessage());
            }
        });
    }
    private void pullInFarmGroups() {
        Call<FarmGroupListPojo> farmGroupListPojoCall = myEndpoint.getAllFarmGroups();
        farmGroupListPojoCall.enqueue(new Callback<FarmGroupListPojo>() {
            @Override
            public void onResponse(Call<FarmGroupListPojo> call, Response<FarmGroupListPojo> response) {
                //save locally

                if(response.isSuccessful()){
                    FarmGroupPojo farmGroupPojo2  = new FarmGroupPojo();
                    try{
                        Delete.table(FarmGroupPojo.class);}
                        catch (Exception e) {

                        }
                    farmGroupPojo2.setGroup_id(0);
                    farmGroupPojo2.setGroup_name("Select farm group");
                    farmGroupPojo2.save();
                    for(FarmGroupPojo farmGroupPojo: response.body().getMessage()){
                        FarmGroupPojo farmGroupPojo1  = new FarmGroupPojo();
                        farmGroupPojo1.setGroup_id(farmGroupPojo.getGroup_id());
                        farmGroupPojo1.setGroup_name(farmGroupPojo.getGroup_name());
                        farmGroupPojo1.save();
                    }
                    noPhotoInterface.successful();
                }
                else {
                    //Delete all stuff in localdatabase
                    noPhotoInterface.failure(response.message());
                }
            }

            @Override
            public void onFailure(Call<FarmGroupListPojo> call, Throwable t) {
            noPhotoInterface.failure(t.getMessage());
            }
        });
    }
}
