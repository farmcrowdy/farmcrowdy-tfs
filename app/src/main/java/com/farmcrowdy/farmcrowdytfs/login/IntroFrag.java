package com.farmcrowdy.farmcrowdytfs.login;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.farmcrowdy.farmcrowdytfs.R;

/**
 * Created by Oluwatobi on 5/30/2018.
 */

public class IntroFrag extends Fragment {
    View view;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.social_media_login, container,false);
        return view;
    }
}
