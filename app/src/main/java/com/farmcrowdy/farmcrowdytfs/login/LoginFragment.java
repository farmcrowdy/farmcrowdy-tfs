package com.farmcrowdy.farmcrowdytfs.login;

import android.app.ProgressDialog;
import android.content.Intent;
import android.hardware.fingerprint.FingerprintManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.farmcrowdy.MassCheck;
import com.farmcrowdy.farmcrowdytfs.ApplicationInstance;
import com.farmcrowdy.farmcrowdytfs.MainActivity;
import com.farmcrowdy.farmcrowdytfs.NoPhotoInterface;
import com.farmcrowdy.farmcrowdytfs.R;
import com.farmcrowdy.farmcrowdytfs.engine.TinyDB;
import com.rafakob.floatingedittext.FloatingEditText;

/**
 * Created by Oluwatobi on 5/30/2018.
 */

public class LoginFragment extends Fragment implements NoPhotoInterface {
    View view;
    LoginPresenter loginPresenter;
    ProgressDialog progressDialog;
    FloatingEditText emailEditText;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.login_fragment, container,false);
        //startActivity(new Intent(getContext(), MassCheck.class));
        loginPresenter = new LoginPresenterImpl(this);
         emailEditText =  view.findViewById(R.id.email_editText);
        final FloatingEditText passwordEditText = view.findViewById(R.id.password_editText);
        progressDialog = new ProgressDialog(getContext());
        progressDialog.setMessage("Logging in");
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);
        Button loginButton = view.findViewById(R.id.login_id);
        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                progressDialog.show();
                loginPresenter.loginNow(emailEditText.getText().trim().toLowerCase(), passwordEditText.getText().trim());
            }
        });


        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
            TextView textView = view.findViewById(R.id.useFingerPrint);
            textView.setVisibility(View.VISIBLE);
            textView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    if(new TinyDB(ApplicationInstance.getContext()).getInt("user_id", 0) == 0) {
                        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                        builder.setMessage("You need to login using email and password credentials for the first time.");
                        builder.setTitle("First time login");
                        builder.show();
                    }
                    else {
                        startActivity(new Intent(getContext(), FingerprintActivity.class));
                    }

                }
            });
        }

        return view;
    }

    @Override
    public void successful() {
        progressDialog.dismiss();
        progressDialog.cancel();
        Toast.makeText(getContext(), "Login successful", Toast.LENGTH_SHORT).show();
        startActivity(new Intent(getContext(), MainActivity.class));
        getActivity().finish();

    }

    @Override
    public void failure(String error) {
        if(error.contains("No address")) error = "No internet connection found";
        progressDialog.dismiss();
        progressDialog.cancel();

        Log.d("greatIssue", "error due to "+error);


        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle("Login failed");
        builder.setMessage(error);
        builder.show();
    }

    @Override
    public void uploadPhotoSuccessful(String url) {

    }
}
