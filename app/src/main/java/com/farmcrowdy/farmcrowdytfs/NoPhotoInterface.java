package com.farmcrowdy.farmcrowdytfs;

/**
 * Created by Oluwatobi on 5/25/2018.
 */

public interface NoPhotoInterface {
    void successful();
    void failure(String error);
    void uploadPhotoSuccessful(String url);
}
