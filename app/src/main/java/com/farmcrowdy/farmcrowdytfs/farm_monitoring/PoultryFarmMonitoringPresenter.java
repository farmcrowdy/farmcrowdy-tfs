package com.farmcrowdy.farmcrowdytfs.farm_monitoring;

public interface PoultryFarmMonitoringPresenter {
    void sendPoultryAssesment(int farmerId,int cropType,String farmStage, String filePath,int signs_of_scratching,int regular_feed_habit,
                              int regular_growth_habit,   int good_appereance, String age_of_bird,String weight_of_birds,
                              String poultry_state, String mon_comment);

    void sendCropAssessment(int farmerId,int cropType,String farmStage, String filePath,int landFertile, int floodAndDisastter, int externalThreats, String comments,int q4, int q5);
}
