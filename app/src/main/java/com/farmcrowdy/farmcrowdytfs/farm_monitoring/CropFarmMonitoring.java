package com.farmcrowdy.farmcrowdytfs.farm_monitoring;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.esafirm.imagepicker.features.ImagePicker;
import com.esafirm.imagepicker.model.Image;
import com.farmcrowdy.farmcrowdytfs.NoPhotoInterface;
import com.farmcrowdy.farmcrowdytfs.R;
import com.farmcrowdy.farmcrowdytfs.profile.FullFarmerInfoPojo;
import com.rafakob.floatingedittext.FloatingEditText;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class CropFarmMonitoring extends AppCompatActivity implements NoPhotoInterface {
    String photoString = "";

    FullFarmerInfoPojo fullFarmerInfoPojo;
    int farmerId;
    CircleImageView uploadImage;
    ProgressDialog progressDialog;
    FloatingEditText commentEdit;

    String farmStageString;

    Switch landFertileSwitch, disasterSwitch,eThreatSwitch, q4Spinner,q5Spinner;
   PoultryFarmMonitoringPresenter poultryFarmMonitoringPresenter;
    Spinner farmStageSpinner;
    TextView q1, q2, q3,q4,q5;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.crop_farm_only_monitoring);

        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitleTextColor(Color.WHITE);
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        q1 = findViewById(R.id.four);
        q2 = findViewById(R.id.five);
        q3 = findViewById(R.id.six);
        q4 = findViewById(R.id.seven);
        q5 = findViewById(R.id.eight);

        landFertileSwitch = findViewById(R.id.oneSpinner);
        disasterSwitch = findViewById(R.id.twoSpinner);
        eThreatSwitch = findViewById(R.id.threeSpinner);
        q4Spinner = findViewById(R.id.fourSpinner);
        q5Spinner = findViewById(R.id.fiveSpinner);

        fullFarmerInfoPojo = getIntent().getExtras().getParcelable("group");
        farmerId = getIntent().getExtras().getInt("id", 1);

        farmStageSpinner = findViewById(R.id.three);
        commentEdit = findViewById(R.id.comment_id);

        uploadImage = findViewById(R.id.uploadImage);
        uploadImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ImagePicker.create(CropFarmMonitoring.this)
                        // set whether pick and / or camera action should return immediate result or not.
                        .folderMode(true)
                        // folder mode (false by default
                        .single() // single mode
                        .limit(1) // max images can be selected (99 by default)
                        .showCamera(true) // show camera or not (true by default)
                        .imageDirectory("Camera") // directory name for captured image  ("Camera" folder by default)
                        .enableLog(false) // disabling log
                        .start(201); // start image picker activity with request code
            }
        });


        progressDialog = new ProgressDialog(this);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Sending assessment information to headquaters");

        farmStageSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                farmStageString = adapterView.getItemAtPosition(i).toString();
                switch (i){
                    case 0: updateForOne();;
                    break;
                    case 1: updateForTwo();
                    break;
                    case 2: updateForThree();
                    break;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });



        poultryFarmMonitoringPresenter = new FarmPresenterImpl(this);


        findViewById(R.id.approveAssessment).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(!TextUtils.isEmpty(photoString)) {
                showDdd();
                }
                else {
                    Toast.makeText(CropFarmMonitoring.this, "Please upload photo", Toast.LENGTH_SHORT).show();
                }

            }
        });

    }
    void updateForOne() {
    q1.setText("Is land fertile for crop?");
    q2.setText("Prone to flood and disaster?");
    q3.setText("Severe external threats?");
    q4.setText("Optimal land preparation");
    q5.setText("Get crop history of land");
    clearSwitch();
    }
    void updateForTwo() {
        q1.setText("Verified planting material");
        q2.setText("Adequate planting spacing");
        q3.setText("Symptoms of plant diseases");
        q4.setText("External threats to crop");
        q5.setText("Efficient use of farm inputs");
        clearSwitch();
    }
    void updateForThree() {
        q1.setText("Good crop maturity");
        q2.setText("Availability of labour for harvest");
        q3.setText("Adequate transportation facility");
        q4.setText("Adequate storage facility");
        q5.setText("Sort out damaged produce");
        clearSwitch();

    }
    void clearSwitch() {
        q4Spinner.setChecked(false);
        q5Spinner.setChecked(false);
        eThreatSwitch.setChecked(false);
        disasterSwitch.setChecked(false);
        landFertileSwitch.setChecked(false);
    }

    void showDdd() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Are you sure");
        builder.setMessage("Do you want to continue sending this information?");
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
                dialogInterface.cancel();
                progressDialog.show();
                poultryFarmMonitoringPresenter.sendCropAssessment(farmerId,0,farmStageString,photoString,landFertileSwitch.isChecked()?1:0,
                        disasterSwitch.isChecked() ?1:0, eThreatSwitch.isChecked() ? 1:0, commentEdit.getText(), q4Spinner.isChecked() ? 1:0, q5Spinner.isChecked() ? 1:0);

            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
                dialogInterface.cancel();
            }
        });
        builder.show();
    }

    @Override
    public void successful() {
        progressDialog.dismiss();
        progressDialog.cancel();
        Toast.makeText(this, "Farm assessment upload successful", Toast.LENGTH_SHORT).show();
        finish();

    }

    @Override
    public void failure(String error) {
        if(error.contains("No address")) error = "No internet connection found";
        progressDialog.dismiss();
        progressDialog.cancel();
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Sending assessment failed due to: "+error);
        builder.setTitle("Failed");
        builder.show();
    }
    @Override
    public void uploadPhotoSuccessful(String url) {

    }
    @Override
    protected void onActivityResult(int requestCode, final int resultCode, Intent data) {
        List<Image> images = ImagePicker.getImages(data);
        if(images !=null) {
            photoString = images.get(0).getPath();
            uploadImage.setImageURI(Uri.parse(photoString));
            super.onActivityResult(requestCode, resultCode, data);
        }
    }
}
