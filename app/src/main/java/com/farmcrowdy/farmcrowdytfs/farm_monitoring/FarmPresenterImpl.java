package com.farmcrowdy.farmcrowdytfs.farm_monitoring;

import android.graphics.Bitmap;
import android.text.TextUtils;

import com.farmcrowdy.farmcrowdytfs.ApplicationInstance;
import com.farmcrowdy.farmcrowdytfs.MyEndpoint;
import com.farmcrowdy.farmcrowdytfs.NoPhotoInterface;
import com.farmcrowdy.farmcrowdytfs.engine.LoadEffecient;
import com.farmcrowdy.farmcrowdytfs.engine.TinyDB;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Date;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.http.Part;

public class FarmPresenterImpl implements PoultryFarmMonitoringPresenter {
    NoPhotoInterface noPhotoInterface;
    RequestBody fileMinNameBody;


    public FarmPresenterImpl(NoPhotoInterface noPhotoInterface) {
        this.noPhotoInterface = noPhotoInterface;
    }

    private MultipartBody.Part getPhotoPart(String filePath) {
        Bitmap high = null;
        try {
            high = new LoadEffecient().decodeSampledBitmapFromFile(filePath, LoadEffecient.maxHeight, LoadEffecient.maxWidth);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        // Bitmap low = new LoadEffecient().decodeSampledBitmapFromFile(filePath, 80.0f, 80.0f);
        File file = new LoadEffecient().saveLocallyFromBitmapToTempFile(high, "bitmapHigh");
        RequestBody requestFile =
                RequestBody.create(
                        MediaType.parse("*/*"), file
                );
        String fileMinName = "monPhoto"+new Date().getTime()+".jpg";
       this.fileMinNameBody = RequestBody.create(MediaType.parse("text/plain"), fileMinName);

        MultipartBody.Part body =
                MultipartBody.Part.createFormData("pimage", fileMinName, requestFile);
        return body;
    }

    @Override
    public void sendPoultryAssesment(int farmerId,int cropType,String farmStage, String filePath,int signs_of_scratching,int regular_feed_habit,
                                     int regular_growth_habit,   int good_appereance, String age_of_bird,String weight_of_birds,
                                   String poultry_state, String mon_comment ) {

        if(TextUtils.isEmpty(age_of_bird)) age_of_bird = "empty";
        if(TextUtils.isEmpty(weight_of_birds)) weight_of_birds = "empty";
        if(TextUtils.isEmpty(poultry_state)) poultry_state= "empty";
        if(TextUtils.isEmpty(mon_comment)) mon_comment = "empty";
        if(TextUtils.isEmpty(farmStage)) farmStage = "empty";

        RequestBody age_of_birdBody = RequestBody.create(MediaType.parse("text/plain"), age_of_bird);
        RequestBody weight_of_birdsBody = RequestBody.create(MediaType.parse("text/plain"), weight_of_birds);
        RequestBody poultry_stateBody = RequestBody.create(MediaType.parse("text/plain"), poultry_state);
        RequestBody mon_commentBody = RequestBody.create(MediaType.parse("text/plain"), mon_comment);
        RequestBody farmStageBody = RequestBody.create(MediaType.parse("text/plain"), farmStage);


        MyEndpoint myEndpoint = ApplicationInstance.getRetrofit().create(MyEndpoint.class);


        MultipartBody.Part body = getPhotoPart(filePath);

        int tfs_id = new TinyDB(ApplicationInstance.getContext()).getInt("user_id", 1);


        Call<ResponseBody> call = myEndpoint.doCropMonitor(tfs_id,farmerId,cropType,farmStageBody,0,0,0,
                body,  fileMinNameBody,signs_of_scratching

                ,regular_feed_habit,regular_growth_habit,good_appereance,age_of_birdBody,weight_of_birdsBody,
                poultry_stateBody,mon_commentBody,0,0);

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if(response.isSuccessful()) noPhotoInterface.successful();
                else noPhotoInterface.failure(response.message());
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                noPhotoInterface.failure(t.getMessage());
            }
        });
    }

    @Override
    public void sendCropAssessment(int farmerId,int cropType,String farmStage, String filePath,int landFertile, int floodAndDisastter, int externalThreats, String comments, int q4, int q5) {
        MyEndpoint myEndpoint = ApplicationInstance.getRetrofit().create(MyEndpoint.class);


        MultipartBody.Part body = getPhotoPart(filePath);
        int tfs_id = new TinyDB(ApplicationInstance.getContext()).getInt("user_id", 1);
        RequestBody farmStageBody = RequestBody.create(MediaType.parse("text/plain"), farmStage);
        RequestBody mon_commentBody = RequestBody.create(MediaType.parse("text/plain"), comments);

        Call<ResponseBody> call = myEndpoint.doCropMonitor(tfs_id,farmerId,cropType,farmStageBody,landFertile,floodAndDisastter,externalThreats,
                body,  fileMinNameBody,0,0,0,0,null,null,
                null,mon_commentBody, q4, q5);

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if(response.isSuccessful()) noPhotoInterface.successful();
                else noPhotoInterface.failure(response.message());
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                noPhotoInterface.failure(t.getMessage());
            }
        });
    }
}
