package com.farmcrowdy.farmcrowdytfs.farm_monitoring;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.widget.AdapterView;
import android.widget.CompoundButton;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.esafirm.imagepicker.features.ImagePicker;
import com.esafirm.imagepicker.model.Image;
import com.farmcrowdy.farmcrowdytfs.NoPhotoInterface;
import com.farmcrowdy.farmcrowdytfs.R;
import com.farmcrowdy.farmcrowdytfs.addFarmer.OneAddFarmer;
import com.farmcrowdy.farmcrowdytfs.profile.FullFarmerInfoPojo;
import com.rafakob.floatingedittext.FloatingEditText;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class PoultryFarmMonitoring extends AppCompatActivity implements NoPhotoInterface {
    Spinner farmStageSpinner, ageOfBirdSpinner, weightOfBirdSpinner;
    Switch signsOfSractchingSwitch, feedHabitSwitch,growthHabitSwitch,appearanceSwitch;
    FloatingEditText commentEdit, poultryState;
    CircleImageView uploadImage;

    String farmStageString="", ageOfBirdString, weightOfBirdString;
    String photoString = "";

    FullFarmerInfoPojo fullFarmerInfoPojo;
    int farmerId;
     PoultryFarmMonitoringPresenter poultryFarmMonitoringPresenter;

    ProgressDialog progressDialog;
    TextView q1,q2,q3,q4;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.poultry_farm_monitoring);

        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitleTextColor(Color.WHITE);
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        progressDialog = new ProgressDialog(this);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Sending assessment");

        q1 = findViewById(R.id.four);
        q2 = findViewById(R.id.five);
        q3 = findViewById(R.id.six);
        q4 = findViewById(R.id.seven);

        fullFarmerInfoPojo = getIntent().getExtras().getParcelable("group");
        farmerId = getIntent().getExtras().getInt("id", 1);

        farmStageSpinner = findViewById(R.id.three);
        ageOfBirdSpinner = findViewById(R.id.middleLeft);
        weightOfBirdSpinner = findViewById(R.id.middleRight);

        signsOfSractchingSwitch = findViewById(R.id.oneSpinner);
        feedHabitSwitch = findViewById(R.id.twoSpinner);
        growthHabitSwitch = findViewById(R.id.threeSpinner);
        appearanceSwitch = findViewById(R.id.fourSpinner);

        commentEdit = findViewById(R.id.comment_id);
        poultryState = findViewById(R.id.poultryState_id);

        uploadImage = findViewById(R.id.uploadImage);
        uploadImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ImagePicker.create(PoultryFarmMonitoring.this)
                        // set whether pick and / or camera action should return immediate result or not.
                        .folderMode(true)
                        // folder mode (false by default
                        .single() // single mode
                        .limit(1) // max images can be selected (99 by default)
                        .showCamera(true) // show camera or not (true by default)
                        .imageDirectory("Camera") // directory name for captured image  ("Camera" folder by default)
                        .enableLog(false) // disabling log
                        .start(201); // start image picker activity with request code
            }
        });

        farmStageSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                farmStageString = adapterView.getItemAtPosition(i).toString();
                switch (i){
                    case 0: selectedBrooding();
                    break;
                    case 1: selectedGrower();
                    break;
                    case 2: selectedFinisher();
                    break;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        ageOfBirdSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                ageOfBirdString = adapterView.getItemAtPosition(i).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        weightOfBirdSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                weightOfBirdString = adapterView.getItemAtPosition(i).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
         poultryFarmMonitoringPresenter = new FarmPresenterImpl(this);

        findViewById(R.id.approveAssessment).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!TextUtils.isEmpty(photoString)){
                    showDdd();
                }
                else {
                    Toast.makeText(PoultryFarmMonitoring.this, "Please upload photo", Toast.LENGTH_SHORT).show();
                }


            }
        });


    }
    void selectedBrooding() {
        q1.setText("Signs of scracthing?");
        q2.setText("Regular feed habit?");
        q3.setText("Regular growth habit?");
        q4.setText("Good chick appearance?");
        clearSwitch();
    }
    void selectedGrower() {
        q1.setText("Feathering");
        q2.setText("Good attitude to feed change");
        q3.setText("Good response to medications");
        q4.setText("Good weight and growth pattern");
        clearSwitch();
    }
    void selectedFinisher() {
        q1.setText("Mortality rate is < 3-5%");
        q2.setText("Good feeding habit");
        q3.setText("Uniformity in size");
        q4.setText("Good appearance");
        clearSwitch();
    }
    void clearSwitch() {
        signsOfSractchingSwitch.setChecked(false);
        feedHabitSwitch.setChecked(false);
        growthHabitSwitch.setChecked(false);
        appearanceSwitch.setChecked(false);
    }

    void showDdd() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Are you sure");
        builder.setMessage("Do you want to continue sending this information?");
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
                dialogInterface.cancel();
                if (!TextUtils.isEmpty(farmStageString)) {
                    progressDialog.show();
                    poultryFarmMonitoringPresenter.sendPoultryAssesment(farmerId, 0, farmStageString, photoString,
                            signsOfSractchingSwitch.isChecked() ? 1 : 0, feedHabitSwitch.isChecked() ? 1 : 0, growthHabitSwitch.isChecked() ? 1 : 0, appearanceSwitch.isChecked() ? 1 : 0,
                            ageOfBirdString, weightOfBirdString, poultryState.getText(), commentEdit.getText());
                }
                else {

Toast.makeText(PoultryFarmMonitoring.this, "Select a farm stage", Toast.LENGTH_SHORT).show();
                }
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
                dialogInterface.cancel();
            }
        });
        builder.show();
    }

    @Override
    public void successful() {
        progressDialog.dismiss();
        progressDialog.cancel();
        Toast.makeText(this, "Farm assessment upload successful", Toast.LENGTH_SHORT).show();
        finish();
    }

    @Override
    public void failure(String error) {
        if(error.contains("No address")) error = "No internet connection found";
        progressDialog.dismiss();
        progressDialog.cancel();
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Sending assessment failed due to: "+error);
        builder.setTitle("Failed");
        builder.show();
    }

    @Override
    public void uploadPhotoSuccessful(String url) {

    }
    @Override
    protected void onActivityResult(int requestCode, final int resultCode, Intent data) {
        List<Image> images = ImagePicker.getImages(data);
        if(images !=null) {
            photoString = images.get(0).getPath();
            uploadImage.setImageURI(Uri.parse(photoString));
            super.onActivityResult(requestCode, resultCode, data);
        }
    }
}
