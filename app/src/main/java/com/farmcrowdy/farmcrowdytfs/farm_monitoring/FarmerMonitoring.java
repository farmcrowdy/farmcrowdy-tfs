package com.farmcrowdy.farmcrowdytfs.farm_monitoring;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.farmcrowdy.farmcrowdytfs.DirectPojo.CropPojo;
import com.farmcrowdy.farmcrowdytfs.DirectPojo.CropPojo_Table;
import com.farmcrowdy.farmcrowdytfs.DirectPojo.LocalGovtPojo;
import com.farmcrowdy.farmcrowdytfs.DirectPojo.LocalGovtPojo_Table;
import com.farmcrowdy.farmcrowdytfs.NoPhotoInterface;
import com.farmcrowdy.farmcrowdytfs.R;
import com.farmcrowdy.farmcrowdytfs.profile.FullFarmerInfoPojo;
import com.novachevskyi.datepicker.CalendarDatePickerDialog;
import com.rafakob.floatingedittext.FloatingEditText;
import com.raizlabs.android.dbflow.sql.language.SQLite;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class FarmerMonitoring extends AppCompatActivity implements NoPhotoInterface {
    FullFarmerInfoPojo fullFarmerInfoPojo;
    int farmerId;
    TextView cropType, dataText, farmGroup;
    LinearLayout  dataLayout;
    RatingBar ratingBar;
    FloatingEditText commentEdit;
    Button approve;
    final String DATE_PICKER_TAG = "TAG";
    float whatRate;
    FarmerMonitoringPresenter presenter;
    ProgressDialog progressDialog;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.crop_farmer_monitoring);

        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitleTextColor(Color.WHITE);
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        fullFarmerInfoPojo = getIntent().getExtras().getParcelable("data");
        farmerId = getIntent().getExtras().getInt("id", 1);
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Sending assessment to admin");
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);


        cropType = findViewById(R.id.assg_one);
        farmGroup = findViewById(R.id.ass_two);
        dataText = findViewById(R.id.dateText);
        dataLayout = findViewById(R.id.selectDateLayout);
        ratingBar = findViewById(R.id.ratingBar);
        commentEdit = findViewById(R.id.comment_id);
        approve = findViewById(R.id.approveAssessment);

        presenter = new FarmerMonitoringImpl(this);

        cropType.setText(fullFarmerInfoPojo.getCrop_proficiency());
        farmGroup.setText(getIntent().getExtras().getString("group"));

        dataLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDatePicker();
            }
        });

        ratingBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float v, boolean b) {
                whatRate = v;
            }
        });

        approve.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDdd();
            }
        });
    }

    void showDdd() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Are you sure");
        builder.setMessage("Do you want to continue sending this information?");
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
                dialogInterface.cancel();
                progressDialog.show();
                presenter.doAction(farmerId,
                        fullFarmerInfoPojo.getCrop_proficiency(),
                        fullFarmerInfoPojo.getFarm_group_id(),
                        dataText.getText().toString(),
                        whatRate,
                        commentEdit.getText());

            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
                dialogInterface.cancel();
            }
        });
        builder.show();
    }



    private void showDatePicker() {
        Calendar calendar = Calendar.getInstance();
        int year = calendar.get(Calendar.YEAR);
        int monthOfYear = calendar.get(Calendar.MONTH);
        int dayOfMonth = calendar.get(Calendar.DAY_OF_MONTH);
        final CalendarDatePickerDialog dialog =
                CalendarDatePickerDialog.newInstance(dateSetListener, year, monthOfYear, dayOfMonth);

        dialog.show(getSupportFragmentManager(), DATE_PICKER_TAG);

    }

    CalendarDatePickerDialog.OnDateSetListener dateSetListener =
            new CalendarDatePickerDialog.OnDateSetListener() {
                @Override
                public void onDateSet(CalendarDatePickerDialog dialog, int year, int monthOfYear,
                                      int dayOfMonth) {
                    dataText.setText(String.valueOf(dayOfMonth+"/")+monthOfYear+"/"+ year);
                }
            };

    @Override
    public void successful() {
        progressDialog.dismiss();
        progressDialog.cancel();
        Toast.makeText(this, "Assessment sent successful", Toast.LENGTH_SHORT).show();
        finish();
    }

    @Override
    public void failure(String error) {
        if(error.contains("No address")) error = "No internet connection found";
        progressDialog.dismiss();
        progressDialog.cancel();
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Failure");
        builder.setMessage("Failed while sending assessment due to "+error);
        builder.show();
    }

    @Override
    public void uploadPhotoSuccessful(String url) {

    }
}
