package com.farmcrowdy.farmcrowdytfs.farm_monitoring;

public interface FarmerMonitoringPresenter {
    void doAction(int farmerId, String cropType, int groupId, String date, float rating, String comment);
}
