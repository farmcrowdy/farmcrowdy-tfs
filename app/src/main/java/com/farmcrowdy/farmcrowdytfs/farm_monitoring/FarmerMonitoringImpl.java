package com.farmcrowdy.farmcrowdytfs.farm_monitoring;

import android.text.TextUtils;
import android.util.Log;

import com.farmcrowdy.farmcrowdytfs.ApplicationInstance;
import com.farmcrowdy.farmcrowdytfs.DirectPojo.CropPojo;
import com.farmcrowdy.farmcrowdytfs.DirectPojo.CropPojo_Table;
import com.farmcrowdy.farmcrowdytfs.MyEndpoint;
import com.farmcrowdy.farmcrowdytfs.NoPhotoInterface;
import com.farmcrowdy.farmcrowdytfs.engine.TinyDB;
import com.raizlabs.android.dbflow.sql.language.SQLite;

import java.util.List;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FarmerMonitoringImpl implements FarmerMonitoringPresenter {
    NoPhotoInterface noPhotoInterface;

    public FarmerMonitoringImpl(NoPhotoInterface noPhotoInterface) {
        this.noPhotoInterface = noPhotoInterface;
    }


    @Override
    public void doAction(int farmerId, String cropType, int groupId, String date, float rating, String comment) {
        MyEndpoint myEndpoint = ApplicationInstance.getRetrofit().create(MyEndpoint.class);
        int tfs_id = new TinyDB(ApplicationInstance.getContext()).getInt("user_id", 1);
        if(TextUtils.isEmpty(date)) date = "now";
        if(TextUtils.isEmpty(comment)) comment = "empty";
        RequestBody dateBody = RequestBody.create(MediaType.parse("text/plain"), date);
        RequestBody commentBody = RequestBody.create(MediaType.parse("text/plain"), comment);


        Call<ResponseBody> call = myEndpoint.doMonitor(farmerId, tfs_id,farmerId,getCropIdFromString(cropType),groupId,dateBody,commentBody);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if(response.isSuccessful()) noPhotoInterface.successful();
                else noPhotoInterface.failure(response.message());
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                noPhotoInterface.failure(t.getMessage());
            }
        });
    }
    private int getCropIdFromString(String crop){
        List<CropPojo> groupList = SQLite.select().from(CropPojo.class)
                .where(CropPojo_Table.crop_name.is(crop)).queryList();
            if(groupList.size() == 0){
                groupList = SQLite.select().from(CropPojo.class)
                        .where(CropPojo_Table.crop_name.is(crop.toLowerCase())).queryList();
            }
            Log.d("EREST", " Crop id is" + groupList.get(0).getCrop_id());



        return groupList.get(0).getCrop_id();
    }
}
