package com.farmcrowdy.farmcrowdytfs.farm_monitoring;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.farmcrowdy.farmcrowdytfs.R;
import com.farmcrowdy.farmcrowdytfs.profile.FullFarmerInfoPojo;

public class MonitoringMainActivity extends AppCompatActivity {
    int farmerId;
    String cropType;
    FullFarmerInfoPojo fullFarmerInfoPojo;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.cropmon_layout);

        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitleTextColor(Color.WHITE);
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        cropType = getIntent().getExtras().getString("data");
        farmerId = getIntent().getExtras().getInt("id", 1);
        fullFarmerInfoPojo = getIntent().getExtras().getParcelable("pojo");

        findViewById(R.id.farmer_assessment_id).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                        Intent intent = new Intent(MonitoringMainActivity.this, FarmerMonitoring.class);
                        intent.putExtra("id", farmerId);
                        intent.putExtra("data", fullFarmerInfoPojo);
                        intent.putExtra("group", getIntent().getExtras().getString("group"));
                        startActivity(intent);
            }
        });
        findViewById(R.id.farm_assessment_id).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(cropType.equals("poultry")){
                   Intent intent =  new Intent(MonitoringMainActivity.this, PoultryFarmMonitoring.class);
                   intent.putExtra("id", farmerId);
                    intent.putExtra("group", getIntent().getExtras().getString("group"));
                    startActivity(intent);
                }
                else {
                    Intent intent =new Intent(MonitoringMainActivity.this, CropFarmMonitoring.class);
                    intent.putExtra("id", farmerId);
                    intent.putExtra("group", getIntent().getExtras().getString("group"));
                    startActivity(intent);
                }
            }
        });
    }
}
