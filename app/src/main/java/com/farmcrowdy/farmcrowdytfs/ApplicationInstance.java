package com.farmcrowdy.farmcrowdytfs;

import android.app.Application;
import android.content.Context;
import android.support.multidex.MultiDexApplication;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.raizlabs.android.dbflow.config.FlowConfig;
import com.raizlabs.android.dbflow.config.FlowManager;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

/**
 * Created by Oluwatobi on 5/25/2018.
 */

public class ApplicationInstance extends MultiDexApplication {
    private static Retrofit retrofit;
    private static Context context;
    private static final RequestBody apiRequest  =  RequestBody.create(MediaType.parse("text/plain"), "aa47f8215c6f30a0dcdb2a36a9f4168e");
    private static Retrofit dataRetrofit;

    private static String baseUrl = "https://farmcrowdy.com/";

    public static String pagaPrincipal = "8889AF42-CE56-4F58-ACC3-1361BDBB95EA";
    public static String pagaCredentials = "hQ5*ZQvf26S%gH2";
    public static String hashKey = "792ebeff24b04e65b3f37a3c40eb8e599ce4f53165a34f9abc4ab6b9f04a57a6f624414bb1274320ae71b8c7df35f6b85e0ba6df31a345ce8e3f6f5606f36a32";


    @Override
    public void onCreate() {
        super.onCreate();
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();
        context  =this;
        FlowManager.init(new FlowConfig.Builder(this).build());
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(new Interceptor() {
            @Override
            public Response intercept(Interceptor.Chain chain) throws IOException {
                Request original = chain.request();
                // Request customization: add request headers
                Request.Builder requestBuilder = original.newBuilder()
                        .header("api-key", "aa47f8215c6f30a0dcdb2a36a9f4168e"); // <-- this is the important line
                Request request = requestBuilder.build();

                return chain.proceed(request);
            }
        });
        OkHttpClient client = httpClient.build();

        retrofit = new Retrofit.Builder()
                    .client(client)
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .baseUrl("http://192.168.20.70/farmapp/api/")
                .build();

        dataRetrofit = new Retrofit.Builder()
                .client(client)
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .baseUrl(baseUrl+"farmdata/api/")
                .build();

    }

    public static RequestBody getApiRequest() {
        return apiRequest;
    }

    public static Retrofit getRetrofit() {
        return retrofit;
    }


    public static Context getContext() {
        return context;
    }

    public static Retrofit getDataRetrofit() {
        return dataRetrofit;
    }

    public static String getBaseUrl() {
        return baseUrl;
    }

    public static void setBaseUrl(String baseUrl) {

        ApplicationInstance.baseUrl = "http://"+baseUrl;
    }
}
