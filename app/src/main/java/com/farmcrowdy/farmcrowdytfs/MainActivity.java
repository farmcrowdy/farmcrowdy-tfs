package com.farmcrowdy.farmcrowdytfs;

import android.content.Intent;
import android.os.StrictMode;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.coremedia.iso.Hex;
import com.farmcrowdy.farmcrowdytfs.DirectPojo.CropPojo;
import com.farmcrowdy.farmcrowdytfs.DirectPojo.FarmGroupPojo;
import com.farmcrowdy.farmcrowdytfs.DirectPojo.FingerprintDecidePojo;
import com.farmcrowdy.farmcrowdytfs.DirectPojo.LocalGovtPojo;
import com.farmcrowdy.farmcrowdytfs.DirectPojo.PagaResponsePojo;
import com.farmcrowdy.farmcrowdytfs.DirectPojo.StatePojo;
import com.farmcrowdy.farmcrowdytfs.addFarmGroup.AddFarmGroupActivity;
import com.farmcrowdy.farmcrowdytfs.addFarmer.OneAddFarmer;
import com.farmcrowdy.farmcrowdytfs.addFarmer.PagaAddBodyPojo;
import com.farmcrowdy.farmcrowdytfs.addFarmer.PagaBuyAirtime;
import com.farmcrowdy.farmcrowdytfs.addFarmer.ThreeAddFarmer;
import com.fgtit.reader.BluetoothReaderTest;
import com.farmcrowdy.farmcrowdytfs.calender.CalenderActivity;
import com.farmcrowdy.farmcrowdytfs.engine.TinyDB;
import com.farmcrowdy.farmcrowdytfs.farmGroupLinker.FarmGroupLinkerActivity;
import com.farmcrowdy.farmcrowdytfs.farmStats.FarmStatList;
import com.farmcrowdy.farmcrowdytfs.farmStats.FarmStatPojo;
import com.farmcrowdy.farmcrowdytfs.farmStats.FarmStatPojo_Table;
import com.farmcrowdy.farmcrowdytfs.farmUpdates.FarmUpdateActvity;
import com.farmcrowdy.farmcrowdytfs.history.HistoryActivity;
import com.farmcrowdy.farmcrowdytfs.inventory.InventoryActivity;
import com.farmcrowdy.farmcrowdytfs.login.LoginActivity;
import com.farmcrowdy.farmcrowdytfs.messages.MessageActivity;
import com.farmcrowdy.farmcrowdytfs.search.MySearchActivity;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.raizlabs.android.dbflow.sql.language.Delete;
import com.raizlabs.android.dbflow.sql.language.SQLite;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.math.BigInteger;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.MessageDigest;
import java.text.DecimalFormat;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

public class MainActivity extends AppCompatActivity {
    private NavigationView mNavigationView;
    DrawerLayout mDrawerLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(new TinyDB(ApplicationInstance.getContext()).getInt("user_id", 0) == 0) {
            startActivity(new Intent(this, LoginActivity.class));
            return;
        }
        setContentView(R.layout.activity_main);
        mNavigationView = findViewById(R.id.nav_view);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
         mDrawerLayout = findViewById(R.id.drawer_layout);
         farmStatsOnline();

        final ActionBar ab = getSupportActionBar();
        ab.setHomeAsUpIndicator(R.drawable.ic_menu_white_24dp);
        ab.setDisplayHomeAsUpEnabled(true);

        mNavigationView.setNavigationItemSelectedListener(
                new NavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(MenuItem menuItem) {
                        menuItem.setChecked(true);
                        mDrawerLayout.closeDrawers();
                        switch (menuItem.getItemId()){
                            case R.id.nav_farmupdates:
                                startActivity(new Intent(MainActivity.this, FarmUpdateActvity.class));
                                break;
                            case R.id.nav_signout:
                                new TinyDB(MainActivity.this).clear();
                                Delete.tables(CropPojo.class, FarmGroupPojo.class, LocalGovtPojo.class, StatePojo.class);
                                startActivity(new Intent(MainActivity.this, LoginActivity.class));
                                break;
                            case R.id.nav_inventory:
                                startActivity(new Intent(MainActivity.this, InventoryActivity.class));
                                break;
                            case R.id.nav_addGroup:
                                startActivity(new Intent(MainActivity.this, AddFarmGroupActivity.class));
                                break;
                            case R.id.nav_farmGroups:
                                startActivity(new Intent(MainActivity.this, FarmGroupLinkerActivity.class));
                                break;
                            case R.id.nav_calender:
                                startActivity(new Intent(MainActivity.this, CalenderActivity.class));
                                break;
                            case R.id.nav_history:
                                startActivity(new Intent(MainActivity.this, HistoryActivity.class));
                                break;
                            case R.id.nav_messages:
                                startActivity(new Intent(MainActivity.this, MessageActivity.class));
                                break;
                            case R.id.nav_biometrics:
                                Intent intent = new Intent(MainActivity.this, BluetoothReaderTest.class);
                                Toast.makeText(MainActivity.this, "farmer id is"+4, Toast.LENGTH_SHORT).show();
                                intent.putExtra("data", new FingerprintDecidePojo(1, Integer.valueOf(4), "", 1));
                                startActivity(intent);
                                break;
                        }
                        return false;
                    }
                });
        doSomeClickings();
    }

    void doSomeClickings() {
        findViewById(R.id.addFarmer_tabber).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this, OneAddFarmer.class));

            }
        });
        findViewById(R.id.farmGroups_id_tab).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this, FarmGroupLinkerActivity.class));
            }
        });
        findViewById(R.id.farmUpdates_tabs).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this, FarmUpdateActvity.class));
            }
        });
        findViewById(R.id.Operations_id_tab).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this, CalenderActivity.class));
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        switch (id) {
            case R.id.action_search:
                startActivity(new Intent(this, MySearchActivity.class));
               break;
            case android.R.id.home:
                mDrawerLayout.openDrawer(GravityCompat.START);
              //  Toast.makeText(this, "Updates", Toast.LENGTH_SHORT).show();
                break;


        }

        return super.onOptionsItemSelected(item);
    }

    void loadFromOfflineFirst() {
        //LocalGovtPojo_Table.state_id.is(stateInt)
       FarmStatPojo farmStatPojo = SQLite.select().from(FarmStatPojo.class).where(FarmStatPojo_Table.stat_id.eq(0)).querySingle();
       if(farmStatPojo !=null) {
           TextView farmSponsored = findViewById(R.id.numberOfFarmSponsored);
           TextView farmEmpowered = findViewById(R.id.numberOfFarmerEmpowered);
           TextView sponsors = findViewById(R.id.numberOfSponsors);
           TextView followers = findViewById(R.id.numberOfFollowers);
           TextView chickens = findViewById(R.id.numberOfChickens);
           TextView farmSponsoredWeek = findViewById(R.id.farmSponsoredWeekResult);

           DecimalFormat formatter = new DecimalFormat("#,###,###");

           chickens.setText(formatter.format(Integer.valueOf(farmStatPojo.getPoultry())));
           sponsors.setText(formatter.format(Integer.valueOf(farmStatPojo.getFarm_sponsors())));
           farmEmpowered.setText(formatter.format(Integer.valueOf(farmStatPojo.getFarmers_empowered())));
           farmSponsored.setText(formatter.format(Integer.valueOf(farmStatPojo.getFarms_sponsored())));
           followers.setText(formatter.format(Integer.valueOf(farmStatPojo.getFarm_followers())));
           farmSponsoredWeek.setText(formatter.format(Integer.valueOf(farmStatPojo.getSold_this_week())));
       }

        //for(int i=0; i<fgList.size(); i++){

        //}
    }
    void farmStatsOnline(){
        loadFromOfflineFirst();
        final MyEndpoint myEndpoint = ApplicationInstance.getDataRetrofit().create(MyEndpoint.class);
        Call<FarmStatList> statListCall = myEndpoint.getStat();
        statListCall.enqueue(new Callback<FarmStatList>() {
            @Override
            public void onResponse(Call<FarmStatList> call, Response<FarmStatList> response) {
                if(response.isSuccessful()) {
                    FarmStatPojo farmStatPojo = response.body().getMessage().get(0);
                    FarmStatPojo mystat = new FarmStatPojo();
                    TextView farmSponsored = findViewById(R.id.numberOfFarmSponsored);
                    TextView farmEmpowered = findViewById(R.id.numberOfFarmerEmpowered);
                    TextView sponsors = findViewById(R.id.numberOfSponsors);
                    TextView followers = findViewById(R.id.numberOfFollowers);
                    TextView chickens = findViewById(R.id.numberOfChickens);
                    TextView farmSponsoredWeek = findViewById(R.id.farmSponsoredWeekResult);

                    DecimalFormat formatter = new DecimalFormat("#,###,###");

                    chickens.setText(formatter.format(Integer.valueOf(farmStatPojo.getPoultry())));
                    sponsors.setText(formatter.format(Integer.valueOf(farmStatPojo.getFarm_sponsors())));
                    farmEmpowered.setText(formatter.format(Integer.valueOf(farmStatPojo.getFarmers_empowered())));
                    farmSponsored.setText(formatter.format(Integer.valueOf(farmStatPojo.getFarms_sponsored())));
                    followers.setText(formatter.format(Integer.valueOf(farmStatPojo.getFarm_followers())));
                    farmSponsoredWeek.setText(formatter.format(Integer.valueOf(farmStatPojo.getSold_this_week())));

                    mystat.setFarm_followers(farmStatPojo.getFarm_followers());
                    mystat.setFarm_sponsors(farmStatPojo.getFarm_sponsors());
                    mystat.setFarmers_empowered(farmStatPojo.getFarmers_empowered());
                    mystat.setFarms_sponsored(farmStatPojo.getFarms_sponsored());
                    mystat.setLast_twentyfour_hours(farmStatPojo.getLast_twentyfour_hours());
                    mystat.setNew_this_week(farmStatPojo.getNew_this_week());
                    mystat.setSold_this_week(farmStatPojo.getSold_this_week());
                    mystat.setPoultry(farmStatPojo.getPoultry());

                    FarmStatPojo farmStatPojo2 = SQLite.select().from(FarmStatPojo.class).where(FarmStatPojo_Table.stat_id.eq(0)).querySingle();
                    if(farmStatPojo2 ==null) mystat.save();
                    else mystat.update();


                }
                else {

                }
            }

            @Override
            public void onFailure(Call<FarmStatList> call, Throwable t) {

            }
        });
    }
    public static Object hashSHA512(String message) {

        try {

            String toReturn = null;
            try {
                MessageDigest digest = MessageDigest.getInstance("SHA-512");
                digest.reset();
                digest.update(message.getBytes("utf8"));
                toReturn = String.format("%040x", new BigInteger(1, digest.digest()));
            } catch (Exception e) {
                e.printStackTrace();
            }

            return toReturn;

        } catch(Exception e) {
            return null;
        }
    }
    @Override
    public void onBackPressed() {
        if (isNavDrawerOpen()) {
            closeNavDrawer();
        } else {
            super.onBackPressed();
        }
    }

    protected boolean isNavDrawerOpen() {
        return mDrawerLayout != null && mDrawerLayout.isDrawerOpen(GravityCompat.START);
    }

    protected void closeNavDrawer() {
        if (mDrawerLayout != null) {
            mDrawerLayout.closeDrawer(GravityCompat.START);
        }
    }

}
