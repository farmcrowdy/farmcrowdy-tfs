package com.farmcrowdy.farmcrowdytfs.inventory;

import com.farmcrowdy.farmcrowdytfs.ApplicationInstance;
import com.farmcrowdy.farmcrowdytfs.MyEndpoint;
import com.farmcrowdy.farmcrowdytfs.NoPhotoInterface;
import com.farmcrowdy.farmcrowdytfs.engine.TinyDB;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Oluwatobi on 5/25/2018.
 */

public class InventoryPresenterImpl implements InventoryPresenter {
    NoPhotoInterface noPhotoInterface;

    public InventoryPresenterImpl(NoPhotoInterface noPhotoInterface) {
        this.noPhotoInterface = noPhotoInterface;
    }

    @Override
    public void inputInventory(int userID, int cropId, String inputName, int totalQuanity, String inputUnit, String sellerName) {
        MyEndpoint myEndpoint = ApplicationInstance.getRetrofit().create(MyEndpoint.class);

        RequestBody inputNameRequest  =  RequestBody.create(MediaType.parse("text/plain"), inputName);
        RequestBody sellerNameRequest  =  RequestBody.create(MediaType.parse("text/plain"), sellerName);
        RequestBody unitRequest  =  RequestBody.create(MediaType.parse("text/plain"), inputUnit);


        int tfs_id = new TinyDB(ApplicationInstance.getContext()).getInt("user_id", 1);
        Call<ResponseBody> call = myEndpoint.addInputInventory(tfs_id,cropId,inputNameRequest,totalQuanity,unitRequest,sellerNameRequest);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if(response.isSuccessful()){
                    noPhotoInterface.successful();
                }
                else {
                    noPhotoInterface.failure(response.message());
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
noPhotoInterface.failure(t.getMessage());
            }
        });
    }
}
