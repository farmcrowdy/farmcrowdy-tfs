package com.farmcrowdy.farmcrowdytfs.inventory;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;

import com.farmcrowdy.farmcrowdytfs.ApplicationInstance;
import com.farmcrowdy.farmcrowdytfs.DirectPojo.CropPojo;
import com.farmcrowdy.farmcrowdytfs.DirectPojo.FarmGroupPojo;
import com.farmcrowdy.farmcrowdytfs.NoPhotoInterface;
import com.farmcrowdy.farmcrowdytfs.R;
import com.farmcrowdy.farmcrowdytfs.engine.TinyDB;
import com.farmcrowdy.farmcrowdytfs.farmUpdates.FarmUpdateActvity;
import com.rafakob.floatingedittext.FloatingEditText;
import com.raizlabs.android.dbflow.sql.language.SQLite;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Oluwatobi on 5/31/2018.
 */

public class InventoryActivity extends AppCompatActivity implements NoPhotoInterface {
    FloatingEditText inventory_inputeName, input_quantity, sellerName;
    Spinner qus,inventory_cropType;
    List<String> cropListString;
    List<Integer> cropListInt;
    ProgressDialog progressDialog;
    int chosenId;
    String chosenString;
    InventoryPresenter inventoryPresenter;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.input_inventory_layout);

        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitleTextColor(Color.WHITE);
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        inventoryPresenter = new InventoryPresenterImpl(this);
        inventory_cropType = findViewById(R.id.inventory_cropType);
        sellerName = findViewById(R.id.inventory_sellername);
        qus = findViewById(R.id.qus);
        input_quantity = findViewById(R.id.input_quantity);
        inventory_inputeName = findViewById(R.id.inventory_inputeName);
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Sending inventory data to admin at the headquaters");
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);
        getAddings();

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, cropListString);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        inventory_cropType.setAdapter(dataAdapter);

        inventory_cropType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                chosenId = cropListInt.get(i);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        qus.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                chosenString = adapterView.getItemAtPosition(i).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

       Button send = (Button) findViewById(R.id.add_inventory_button);
        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(!TextUtils.isEmpty(inventory_inputeName.getText()) || !TextUtils.isEmpty(input_quantity.getText())) {
                    progressDialog.show();
                    int tfs_id = new TinyDB(ApplicationInstance.getContext()).getInt("user_id", 1);

                    inventoryPresenter.inputInventory(tfs_id, chosenId, inventory_inputeName.getText(), Integer.valueOf(input_quantity.getText()),
                            chosenString, sellerName.getText());

                }
                else {
                    Toast.makeText(InventoryActivity.this, "Please input into all fields", Toast.LENGTH_SHORT).show();
                }
            }
        });

    }
    private void getAddings() {
       List<CropPojo> groupList = SQLite.select().
                from(CropPojo.class).queryList();

        cropListString = new ArrayList<>();
        cropListInt = new ArrayList<>();

        for(int i=0; i<groupList.size(); i++){
            cropListString.add(groupList.get(i).getCrop_name());
            cropListInt.add(groupList.get(i).getCrop_id());
        }
    }

    @Override
    public void successful() {
        progressDialog.dismiss();
        progressDialog.cancel();
        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        builder.setMessage("Inventory updated");
        builder.setTitle("Successful");
        builder.setCancelable(false);
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                finish();
            }
        });
        builder.show();

    }

    @Override
    public void failure(String error) {
        if(error.contains("No address")) error = "No internet connection found";
        progressDialog.dismiss();
        progressDialog.cancel();
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Due to "+error);
        builder.setTitle("Failed");
        builder.show();

    }

    @Override
    public void uploadPhotoSuccessful(String url) {

    }
}
