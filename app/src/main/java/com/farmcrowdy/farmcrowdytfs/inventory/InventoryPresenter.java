package com.farmcrowdy.farmcrowdytfs.inventory;

import okhttp3.RequestBody;
import retrofit2.http.Part;

/**
 * Created by Oluwatobi on 5/25/2018.
 */

public interface InventoryPresenter {
    void inputInventory(int userID,  int cropId,
                       String inputName,
                         int totalQuanity,
                        String inputUnit,
                        String sellerName);
}
