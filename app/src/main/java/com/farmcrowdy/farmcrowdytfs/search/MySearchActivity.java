package com.farmcrowdy.farmcrowdytfs.search;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.farmcrowdy.farmcrowdytfs.ListPojo.FarmerListPojo;
import com.farmcrowdy.farmcrowdytfs.R;
import com.farmcrowdy.farmcrowdytfs.onlyFarmer.JustFarmerActivityInterface;
import com.farmcrowdy.farmcrowdytfs.onlyFarmer.JustFarmerAdapter;
import com.farmcrowdy.farmcrowdytfs.onlyFarmer.JustFarmerPresenter;
import com.farmcrowdy.farmcrowdytfs.onlyFarmer.JustFarmerPresenterImpl;

/**
 * Created by Oluwatobi on 6/1/2018.
 */

public class MySearchActivity extends AppCompatActivity implements JustFarmerActivityInterface {
    RecyclerView searchRecycler;
    Button searchButton;
    EditText searchEdit;
    ProgressBar progressBar;
    TextView callback;
    JustFarmerPresenter justFarmerPresenter;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.search_activity_layout);
        searchRecycler = (RecyclerView) findViewById(R.id.searchRecyclerView);
        searchButton = (Button) findViewById(R.id.search_farmer_button);
        searchEdit = (EditText) findViewById(R.id.searchEdit);
        progressBar = findViewById(R.id.grgProgress);
        callback = findViewById(R.id.callback);
        findViewById(R.id.backFromSearch).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               finish();
            }
        });
        justFarmerPresenter = new JustFarmerPresenterImpl(this);

        searchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startSearchMock();
                justFarmerPresenter.searchForFarmers(searchEdit.getText().toString());
            }
        });
        endSearchMock();
        makeButtonResponsive();
    }
    private void startSearchMock(){
        progressBar.setVisibility(View.VISIBLE);
        callback.setText("Getting information");
        callback.setVisibility(View.VISIBLE);

    }
    private void endSearchMock(){
        progressBar.setVisibility(View.GONE);
        callback.setVisibility(View.GONE);
    }
    private void negativeFeedback(String string){
        progressBar.setVisibility(View.GONE);
        callback.setText("Something went wrong while trying bacause: "+string);
    }
    private void makeButtonResponsive() {
        searchButton.setActivated(false);
        searchButton.setClickable(false);
        searchEdit.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            if(charSequence.length()>1){
                searchButton.setActivated(true);
                searchButton.setClickable(true);
            }
            else {
                searchButton.setActivated(false);
                searchButton.setClickable(false);
            }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }

    @Override
    public void receiveSuccessful(FarmerListPojo farmerListPojo) {
        RecyclerView recyclerView = findViewById(R.id.searchRecyclerView);
        LinearLayoutManager linearLayout = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(linearLayout);
        recyclerView.setHasFixedSize(true);
        JustFarmerAdapter justAdapter = new JustFarmerAdapter(farmerListPojo.getMessage(), this);
        recyclerView.setAdapter(justAdapter);
    }

    @Override
    public void failureRcv(String error) {
        negativeFeedback(error);
    }
}
