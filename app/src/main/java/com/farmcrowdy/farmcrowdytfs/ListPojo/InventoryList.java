package com.farmcrowdy.farmcrowdytfs.ListPojo;

import com.farmcrowdy.farmcrowdytfs.DirectPojo.InventoryPojo;

import java.util.ArrayList;

/**
 * Created by Oluwatobi on 5/24/2018.
 */

public class InventoryList {
    String status;
    ArrayList<InventoryPojo> message;

    public String getStatus() {
        return status;
    }

    public ArrayList<InventoryPojo> getMessage() {
        return message;
    }
}
