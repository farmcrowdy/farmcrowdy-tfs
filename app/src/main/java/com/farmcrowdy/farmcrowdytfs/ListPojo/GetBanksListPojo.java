package com.farmcrowdy.farmcrowdytfs.ListPojo;

import com.farmcrowdy.farmcrowdytfs.DirectPojo.BanksJojo;

import java.util.ArrayList;

/**
 * Created by Oluwatobi on 5/24/2018.
 */

public class GetBanksListPojo {
    String status;

    ArrayList<BanksJojo> message;

    public String getStatus() {
        return status;
    }

    public ArrayList<BanksJojo> getMessage() {
        return message;
    }
}
