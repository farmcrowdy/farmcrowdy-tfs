package com.farmcrowdy.farmcrowdytfs.ListPojo;

import com.farmcrowdy.farmcrowdytfs.DirectPojo.FarmGroupPojo;

public class FarmGroupSinglePojo {
    String status;
    FarmGroupPojo message;

    public String getStatus() {
        return status;
    }

    public FarmGroupPojo getMessage() {
        return message;
    }
}
