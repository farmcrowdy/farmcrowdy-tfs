package com.farmcrowdy.farmcrowdytfs.ListPojo;

import com.farmcrowdy.farmcrowdytfs.DirectPojo.BanksJojo;
import com.farmcrowdy.farmcrowdytfs.DirectPojo.FarmLocationPojo;

import java.util.ArrayList;

/**
 * Created by Oluwatobi on 5/24/2018.
 */

public class FarmLocationListPojo {
    String status;

    ArrayList<FarmLocationPojo> message;

    public ArrayList<FarmLocationPojo> getMessage() {
        return message;
    }

    public String getStatus() {
        return status;
    }
}
