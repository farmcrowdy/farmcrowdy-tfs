package com.farmcrowdy.farmcrowdytfs.ListPojo;

import com.farmcrowdy.farmcrowdytfs.DirectPojo.LocalGovtPojo;

import java.util.ArrayList;

/**
 * Created by Oluwatobi on 5/24/2018.
 */

public class LocalGovtListPojo {
String status;
    ArrayList<LocalGovtPojo> message;

    public String getStatus() {
        return status;
    }

    public ArrayList<LocalGovtPojo> getMessage() {
        return message;
    }
}
