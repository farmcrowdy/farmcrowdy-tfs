package com.farmcrowdy.farmcrowdytfs.ListPojo;

import com.farmcrowdy.farmcrowdytfs.DirectPojo.CropPojo;

import java.util.ArrayList;

/**
 * Created by Oluwatobi on 5/24/2018.
 */

public class CropListPojo {
    String status;
    ArrayList<CropPojo> message;

    public String getStatus() {
        return status;
    }

    public ArrayList<CropPojo> getMessage() {
        return message;
    }
}
