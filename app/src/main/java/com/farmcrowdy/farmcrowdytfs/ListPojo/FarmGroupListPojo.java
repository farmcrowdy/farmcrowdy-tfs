package com.farmcrowdy.farmcrowdytfs.ListPojo;

import com.farmcrowdy.farmcrowdytfs.DirectPojo.FarmGroupPojo;

import java.util.ArrayList;

/**
 * Created by Oluwatobi on 5/24/2018.
 */

public class FarmGroupListPojo {
    String status;
    ArrayList<FarmGroupPojo> message;

    public String getStatus() {
        return status;
    }

    public ArrayList<FarmGroupPojo> getMessage() {
        return message;
    }
}
