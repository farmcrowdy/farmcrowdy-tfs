package com.farmcrowdy.farmcrowdytfs.updateProfile;

import android.app.ProgressDialog;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.Toast;

import com.farmcrowdy.farmcrowdytfs.R;
import com.farmcrowdy.farmcrowdytfs.addFarmer.AddFarmerPresenter;
import com.farmcrowdy.farmcrowdytfs.addFarmer.AddFarmerPresenterImpl;
import com.farmcrowdy.farmcrowdytfs.profile.FullFarmerInfoPojo;
import com.rafakob.floatingedittext.FloatingEditText;

import java.util.ArrayList;

public class UpdateHistoryActivity extends AppCompatActivity implements UpdateInterface {

    FloatingEditText landAreaFarmedEdit, yearsOfExperience,commentEdit;
    Spinner amountRangeSpinner;
    Switch hasPreviousTraniningSwitch;
    String amountRangeString;
    int previousTrainingInt;
   UpdateOnePresenter presenter;

    ProgressDialog progressDialog;
    FullFarmerInfoPojo fullFarmerInfoPojo;
    int farmerId;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_farmer_step_three_layout);


        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitleTextColor(Color.WHITE);
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Uploading farmer's details");
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);
        presenter = new UpdateProfilePresenterImpl(this);

        fullFarmerInfoPojo = getIntent().getExtras().getParcelable("data");
        farmerId = getIntent().getExtras().getInt("id", 1);
        landAreaFarmedEdit = findViewById(R.id.farmed_previously);
        yearsOfExperience = findViewById(R.id.years_of_experience_edit);
        commentEdit = findViewById(R.id.comment_editText);
        amountRangeSpinner = findViewById(R.id.income_range_spinner);
        hasPreviousTraniningSwitch = findViewById(R.id.previous_trained_switch);

        hasPreviousTraniningSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                previousTrainingInt = b ? 1 : 0;

            }
        });

        if(fullFarmerInfoPojo.getPrevious_training() ==1) hasPreviousTraniningSwitch.setChecked(true);
        else hasPreviousTraniningSwitch.setChecked(false);
        commentEdit.setText(fullFarmerInfoPojo.getComment());
        yearsOfExperience.setText(String.valueOf(fullFarmerInfoPojo.getYears_of_experience()));
        landAreaFarmedEdit.setText(String.valueOf(fullFarmerInfoPojo.getLand_area_farmed()));

        ArrayList<String> strings = new ArrayList<>();
        strings.add(fullFarmerInfoPojo.getIncome_range());
        strings.add( "#0 - #99,000");
        strings.add("#100,000 - #500,000");
        strings.add("#500,000 - #1");
        strings.add("1M - 2M");
        strings.add("M - 5M");

        ArrayAdapter<String> dataAdapterLocation = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, strings);

        dataAdapterLocation.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        amountRangeSpinner.setAdapter(dataAdapterLocation);

        amountRangeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                amountRangeString = adapterView.getItemAtPosition(i).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        findViewById(R.id.fab).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                progressDialog.show();
                fullFarmerInfoPojo.setPrevious_training(previousTrainingInt);
                fullFarmerInfoPojo.setComment(commentEdit.getText());
                fullFarmerInfoPojo.setYears_of_experience(Integer.valueOf(yearsOfExperience.getText()));
                fullFarmerInfoPojo.setLand_area_farmed(landAreaFarmedEdit.getText());
                if(!amountRangeString.isEmpty())fullFarmerInfoPojo.setIncome_range(amountRangeString);

                presenter.doUpdate(fullFarmerInfoPojo,farmerId);



            }
        });

    }

    @Override
    public void updateSuccessful() {
        progressDialog.dismiss();
        progressDialog.cancel();

        Toast.makeText(this, "Successfully updated farmer's profile", Toast.LENGTH_SHORT).show();
        finish();
        finish();


    }

    @Override
    public void failureJors(String error) {
        if(error.contains("No address")) error = "No internet connection found";
        progressDialog.dismiss();
        progressDialog.cancel();
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Something went wrong due to "+error);
        builder.setTitle("Update failure");
        builder.show();
    }
}
