package com.farmcrowdy.farmcrowdytfs.updateProfile;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.farmcrowdy.farmcrowdytfs.DirectPojo.CropPojo;
import com.farmcrowdy.farmcrowdytfs.DirectPojo.FarmGroupPojo;
import com.farmcrowdy.farmcrowdytfs.DirectPojo.FarmLocationPojo;
import com.farmcrowdy.farmcrowdytfs.DirectPojo.LocalGovtPojo;
import com.farmcrowdy.farmcrowdytfs.DirectPojo.LocalGovtPojo_Table;
import com.farmcrowdy.farmcrowdytfs.DirectPojo.StatePojo;
import com.farmcrowdy.farmcrowdytfs.R;
import com.farmcrowdy.farmcrowdytfs.engine.LoadEffecient;
import com.farmcrowdy.farmcrowdytfs.profile.FullFarmerInfoPojo;
import com.novachevskyi.datepicker.CalendarDatePickerDialog;
import com.rafakob.floatingedittext.FloatingEditText;
import com.raizlabs.android.dbflow.sql.language.SQLite;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class UpdateProfileActivity extends AppCompatActivity implements View.OnClickListener, UpdateInterface {
    FullFarmerInfoPojo fullFarmerInfoPojo;
    FloatingEditText firstnameEdit, lastnameEdit, phoneNumberEdit, noOfDependantsEdit;
    TextView dobText;
    RelativeLayout dob;
    Spinner genderspinner, maritalStatus, cropSpinner, locationSpinner, farmgroup_spinner, stateSpinner, localGovtSpinner;
    String genderString = "", maritalString ="", cropString = "";
    int cropInt =0, locationInt=0,farmGroupInt=0,stateInt=0, govtInt=0;

    private static final String DATE_PICKER_TAG = "tag";

    ArrayList<String> cropListString = new ArrayList<>();
    ArrayList<Integer> cropListInt = new ArrayList<>();

    ArrayList<String> locationListString = new ArrayList<>();
    ArrayList<Integer> locationListInt = new ArrayList<>();

    ArrayList<String> farmGroupListString = new ArrayList<>();
    ArrayList<Integer> farmGroupListInt = new ArrayList<>();

    ArrayList<String> stateListString = new ArrayList<>();
    ArrayList<Integer> stateListInt = new ArrayList<>();

    ArrayList<String> localGovtListString = new ArrayList<>();
    ArrayList<Integer> localGovtListInt = new ArrayList<>();
    String imageList="";

    int farmerId;
    UpdateOnePresenter onePresenter;
    ProgressDialog progressDialog;



    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_new_farmer);
        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitleTextColor(Color.WHITE);
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        fullFarmerInfoPojo = getIntent().getExtras().getParcelable("data");
        farmerId = getIntent().getExtras().getInt("id",1);

        onePresenter = new UpdateProfilePresenterImpl(this);
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Updating farmer's personal details");
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);

        firstnameEdit = findViewById(R.id.firstname_editText);
        lastnameEdit = findViewById(R.id.lastname_editText);
        phoneNumberEdit = findViewById(R.id.phoneNumber_editText);
        noOfDependantsEdit = findViewById(R.id.dependants_editText);
        dob = findViewById(R.id.choose_date_layout);
        dobText = findViewById(R.id.edit_dob);
        genderspinner = findViewById(R.id.genderSpinner);
        cropSpinner = findViewById(R.id.proficiency_spinner);
        locationSpinner = findViewById(R.id.farm_location_spinner);
        farmgroup_spinner = findViewById(R.id.farm_group_spinner);
        stateSpinner = findViewById(R.id.state_spinner);
        maritalStatus = findViewById(R.id.spinnerMaritalStatus);
        localGovtSpinner = findViewById(R.id.LGA_spinner);

        firstnameEdit.setText(fullFarmerInfoPojo.getFname());
        lastnameEdit.setText(fullFarmerInfoPojo.getSname());
        phoneNumberEdit.setText(fullFarmerInfoPojo.getPhone());
        noOfDependantsEdit.setText(String.valueOf(fullFarmerInfoPojo.getNumber_of_dependants()));
        dobText.setText(fullFarmerInfoPojo.getDob());


        getAddings();
        setupAllSpinners();
        findViewById(R.id.fab).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                progressDialog.show();

                fullFarmerInfoPojo.setFname(firstnameEdit.getText());
                fullFarmerInfoPojo.setSname(lastnameEdit.getText());
                fullFarmerInfoPojo.setPhone(phoneNumberEdit.getText());
                fullFarmerInfoPojo.setNumber_of_dependants(Integer.valueOf(noOfDependantsEdit.getText()));
                fullFarmerInfoPojo.setDob(dobText.getText().toString());

                if(!genderString.isEmpty()) fullFarmerInfoPojo.setGender(genderString);
                if(!cropString.isEmpty())fullFarmerInfoPojo.setCrop_proficiency(cropString);
                if(locationInt != 0)fullFarmerInfoPojo.setFarm_location_id(locationInt);
                if(farmGroupInt !=0)fullFarmerInfoPojo.setFarm_group_id(farmGroupInt);
                if(stateInt!=0)fullFarmerInfoPojo.setState_id(stateInt);
                if(!maritalString.isEmpty())fullFarmerInfoPojo.setMarital_status(maritalString);
                if(govtInt!=0)fullFarmerInfoPojo.setLocal_id(govtInt);
                if(!imageList.isEmpty())fullFarmerInfoPojo.setPro_image(imageList);

                onePresenter.doUpdate(fullFarmerInfoPojo, farmerId);

            }
        });

        findViewById(R.id.takeUploadPhoto).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CropImage.activity()
                        .setGuidelines(CropImageView.Guidelines.ON)
                        .start(UpdateProfileActivity.this);
            }
        });
    }


    private void setupAllSpinners() {
        if(fullFarmerInfoPojo.getGender().equals("Male")) genderspinner.setSelection(0);
        else genderspinner.setSelection(1);
        genderspinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                genderString = adapterView.getItemAtPosition(i).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        if(fullFarmerInfoPojo.getMarital_status().equals("Single")) maritalStatus.setSelection(0);
        else if(fullFarmerInfoPojo.getMarital_status().equals("Married")) maritalStatus.setSelection(1);
        else maritalStatus.setSelection(2);
        maritalStatus.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                maritalString = adapterView.getItemAtPosition(i).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });


        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, cropListString);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        cropSpinner.setAdapter(dataAdapter);

        ArrayAdapter<String> dataAdapterLocation = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, locationListString);
        dataAdapterLocation.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        locationSpinner.setAdapter(dataAdapterLocation);

        ArrayAdapter<String> dataAdapteFarmGroup = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, farmGroupListString);
        dataAdapteFarmGroup.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        farmgroup_spinner.setAdapter(dataAdapteFarmGroup);

        ArrayAdapter<String> dataAdapterState = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, stateListString);
        dataAdapterState.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        stateSpinner.setAdapter(dataAdapterState);


        List<CropPojo> cropLister = SQLite.select().from(CropPojo.class).queryList();
        for(int i=0; i<cropLister.size(); i++) {
            if(fullFarmerInfoPojo.getCrop_proficiency().equals(cropLister.get(i).getCrop_name())) {
                cropSpinner.setSelection(i);
            }
        }
        List<FarmGroupPojo> groupLister = SQLite.select().from(FarmGroupPojo.class).queryList();
        for(int i=0; i<groupLister.size(); i++) {
            if(fullFarmerInfoPojo.getFarm_group_id() == groupLister.get(i).getGroup_id()) {
                farmgroup_spinner.setSelection(i);
            }
        }

        List<StatePojo> statePojorLister = SQLite.select().from(StatePojo.class).queryList();
        for(int i=0; i<statePojorLister.size(); i++) {
            if(fullFarmerInfoPojo.getState_id() == statePojorLister.get(i).getState_id()) {
                stateSpinner.setSelection(i);
            }
        }
        List<FarmLocationPojo> farmLocatio = SQLite.select().from(FarmLocationPojo.class).queryList();
        for(int i=0; i<farmLocatio.size(); i++) {
            if(fullFarmerInfoPojo.getFarm_location_id() == farmLocatio.get(i).getLocation_id()) {
                locationSpinner.setSelection(i);
            }
        }



        stateSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                stateInt = stateListInt.get(i);
                if(i!=0) {


                    List<LocalGovtPojo> govtList = SQLite.select().from(LocalGovtPojo.class).where(LocalGovtPojo_Table.state_id.is(stateInt)).
                            queryList();
                    localGovtListString = new ArrayList<>();
                    for(int p=0; p<govtList.size(); p++) {
                        localGovtListString.add(govtList.get(p).getLocal_name());
                        localGovtListInt.add(govtList.get(p).getLocal_id());

                    }

                    ArrayAdapter<String> dataAdapterGovt = new ArrayAdapter<String>(UpdateProfileActivity.this,
                            android.R.layout.simple_spinner_item, localGovtListString);
                    dataAdapterGovt.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    localGovtSpinner.setAdapter(dataAdapterGovt);

                    List<LocalGovtPojo> govtListr = SQLite.select().from(LocalGovtPojo.class).where(LocalGovtPojo_Table.state_id.is(stateInt)).
                            queryList();

                    for(int p=0; p<govtListr.size(); p++) {
                        if(fullFarmerInfoPojo.getLocal_id() == govtListr.get(p).getLocal_id()) {
                            localGovtSpinner.setSelection(p);
                        }
                    }


                    localGovtSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                            govtInt = localGovtListInt.get(i);
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {

                        }
                    });

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });



        cropSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                cropInt = cropListInt.get(i);
                cropString = adapterView.getItemAtPosition(i).toString();


            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        locationSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                locationInt = locationListInt.get(i);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        farmgroup_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                farmGroupInt = farmGroupListInt.get(i);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });



    }

    private void getAddings() {
        List<CropPojo> groupList = SQLite.select().
                from(CropPojo.class).queryList();

        cropListString = new ArrayList<>();
        cropListInt = new ArrayList<>();

        for(int i=0; i<groupList.size(); i++){
            cropListString.add(groupList.get(i).getCrop_name());
            cropListInt.add(groupList.get(i).getCrop_id());
        }

        List<FarmLocationPojo> locList = SQLite.select().from(FarmLocationPojo.class).queryList();
        for(int i=0; i<locList.size(); i++) {
            locationListString.add(locList.get(i).getLocation());
            locationListInt.add(locList.get(i).getLocation_id());
        }

        List<FarmGroupPojo> fgList = SQLite.select().from(FarmGroupPojo.class).queryList();
        for(int i=0; i<fgList.size(); i++){
            farmGroupListString.add(fgList.get(i).getGroup_name());
            farmGroupListInt.add(fgList.get(i).getGroup_id());
        }

        List<StatePojo> stateList = SQLite.select().from(StatePojo.class).queryList();
        for(int i=0; i<stateList.size(); i++){
            stateListString.add(stateList.get(i).getName());
            stateListInt.add(stateList.get(i).getState_id());
        }




    }

    private void showDatePicker() {
        Calendar calendar = Calendar.getInstance();
        int year = calendar.get(Calendar.YEAR);
        int monthOfYear = calendar.get(Calendar.MONTH);
        int dayOfMonth = calendar.get(Calendar.DAY_OF_MONTH);
        final CalendarDatePickerDialog dialog =
                CalendarDatePickerDialog.newInstance(dateSetListener, year, monthOfYear, dayOfMonth);

        dialog.show(getSupportFragmentManager(), DATE_PICKER_TAG);

    }

    CalendarDatePickerDialog.OnDateSetListener dateSetListener =
            new CalendarDatePickerDialog.OnDateSetListener() {
                @Override
                public void onDateSet(CalendarDatePickerDialog dialog, int year, int monthOfYear,
                                      int dayOfMonth) {
                    dobText.setText(String.valueOf(dayOfMonth+" / ")+monthOfYear+" / "+ year);
                }
            };

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.choose_date_layout:
                showDatePicker();
                break;

        }
    }
    @Override
    protected void onActivityResult(int requestCode, final int resultCode, Intent data) {
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                Uri resultUri = result.getUri();
                imageList = resultUri.getPath();
                if (!imageList.equals("empty")) {
                    Bitmap high;
                    try {
                        high = new LoadEffecient().decodeSampledBitmapFromFile(imageList, 200, 200);
                        File file = new LoadEffecient().saveLocallyFromBitmapToTempFile(high, "bitmapHigh");
                        CircleImageView circleImageView = findViewById(R.id.selected_photo);
                        //Picasso.with(this).load(imageList).resize(200,200).into(circleImageView);
                        // circleImageView.setImageURI(Uri.parse(imageList));
                        circleImageView.setImageURI(Uri.parse(file.getPath()));

                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    }
                }
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
            }
        }
    }

    @Override
    public void updateSuccessful() {
        progressDialog.dismiss();
        progressDialog.cancel();

        Toast.makeText(this, "Successfully updated farmer's profile", Toast.LENGTH_SHORT).show();
        finish();


    }

    @Override
    public void failureJors(String error) {
        if(error.contains("No address")) error = "No internet connection found";
        progressDialog.dismiss();
        progressDialog.cancel();
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Something went wrong due to "+error);
        builder.setTitle("Update failure");
        builder.show();
    }
}
