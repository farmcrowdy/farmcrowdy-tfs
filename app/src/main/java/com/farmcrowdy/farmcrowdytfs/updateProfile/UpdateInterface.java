package com.farmcrowdy.farmcrowdytfs.updateProfile;

public interface UpdateInterface {
    void updateSuccessful();
    void failureJors(String error);
}
