package com.farmcrowdy.farmcrowdytfs.updateProfile;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.farmcrowdy.farmcrowdytfs.R;
import com.farmcrowdy.farmcrowdytfs.profile.FullFarmerInfoPojo;

public class ListOfUpdatablesActivity extends AppCompatActivity {
    int farmerId;
    FullFarmerInfoPojo fullFarmerInfoPojo;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.update_listable);

        farmerId = getIntent().getExtras().getInt("id", 1);
        fullFarmerInfoPojo = getIntent().getExtras().getParcelable("pojo");

        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitleTextColor(Color.WHITE);
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        findViewById(R.id.personalProfile_id).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(ListOfUpdatablesActivity.this, UpdateProfileActivity.class);
                intent.putExtra("data", fullFarmerInfoPojo);
                intent.putExtra("id", farmerId);
                startActivity(intent);

            }
        });

        findViewById(R.id.bankDetails_id).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ListOfUpdatablesActivity.this, UpdateBankActivity.class);
                intent.putExtra("data", fullFarmerInfoPojo);
                intent.putExtra("id", farmerId);
                startActivity(intent);
            }
        });

        findViewById(R.id.history_id).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ListOfUpdatablesActivity.this, UpdateHistoryActivity.class);
                intent.putExtra("data", fullFarmerInfoPojo);
                intent.putExtra("id", farmerId);
                startActivity(intent);
            }
        });


    }
}
