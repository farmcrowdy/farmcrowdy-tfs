package com.farmcrowdy.farmcrowdytfs.updateProfile;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.Toast;

import com.farmcrowdy.farmcrowdytfs.DirectPojo.BanksJojo;
import com.farmcrowdy.farmcrowdytfs.DirectPojo.FarmLocationPojo;
import com.farmcrowdy.farmcrowdytfs.R;
import com.farmcrowdy.farmcrowdytfs.addFarmer.ThreeAddFarmer;
import com.farmcrowdy.farmcrowdytfs.addFarmer.TwoAddFarmer;
import com.farmcrowdy.farmcrowdytfs.profile.FullFarmerInfoPojo;
import com.rafakob.floatingedittext.FloatingEditText;
import com.raizlabs.android.dbflow.sql.language.SQLite;

import java.util.ArrayList;
import java.util.List;

public class UpdateBankActivity extends AppCompatActivity implements UpdateInterface{
    Switch hasBankAccount;
    boolean itHasBank = true;
    Spinner listOfBanks;
    ArrayList<String> bankListString;
    ArrayList<Integer> bankListInt;
    int bank_selected;
    ProgressDialog progressDialog;
    UpdateOnePresenter onePresenter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_farmer_step_two_layout);

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Updating farmer's personal details");
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);
        onePresenter = new UpdateProfilePresenterImpl(this);

        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitleTextColor(Color.WHITE);
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        final FullFarmerInfoPojo fullFarmerInfoPojo = getIntent().getExtras().getParcelable("data");
        final int farmerId = getIntent().getExtras().getInt("id", 1);
        hasBankAccount = findViewById(R.id.hasBankAccountSpinner);
        if(TextUtils.isEmpty(fullFarmerInfoPojo.getAcct_number())) hasBankAccount.setChecked(false);
        hasBankAccount.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                itHasBank = b;
            }
        });
        listOfBanks = findViewById(R.id.banks_spinner);
        List<BanksJojo> groupList = SQLite.select().
                from(BanksJojo.class).queryList();

        bankListString = new ArrayList<>();
        bankListInt = new ArrayList<>();

        for(int i=0; i<groupList.size(); i++){
            bankListString.add(groupList.get(i).getBank_name());
            bankListInt.add(groupList.get(i).getBank_id());
        }
        ArrayAdapter<String> dataAdapterState = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, bankListString);
        dataAdapterState.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        listOfBanks.setAdapter(dataAdapterState);

        List<BanksJojo> bankie = SQLite.select().from(BanksJojo.class).queryList();
        for(int i=0; i<bankie.size(); i++) {
            if(fullFarmerInfoPojo.getFarm_location_id() == bankie.get(i).getBank_id()) {
                listOfBanks.setSelection(i);
            }
        }

        listOfBanks.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                bank_selected = bankListInt.get(i);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        final FloatingEditText accountNumberEdit = findViewById(R.id.account_number_editText);
        accountNumberEdit.setText(String.valueOf(fullFarmerInfoPojo.getAcct_number()));
        findViewById(R.id.fab).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                fullFarmerInfoPojo.setAcct_number(accountNumberEdit.getText());
                if(bank_selected!=0) fullFarmerInfoPojo.setBank_id(bank_selected);

                onePresenter.doUpdate(fullFarmerInfoPojo, farmerId);

            }
        });


    }

    @Override
    public void updateSuccessful() {
        progressDialog.dismiss();
        progressDialog.cancel();

        Toast.makeText(this, "Successfully updated farmer's profile", Toast.LENGTH_SHORT).show();
        finish();
        finish();


    }

    @Override
    public void failureJors(String error) {
        if(error.contains("No address")) error = "No internet connection found";
        progressDialog.dismiss();
        progressDialog.cancel();
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Something went wrong due to "+error);
        builder.setTitle("Update failure");
        builder.show();
    }
}
