package com.farmcrowdy.farmcrowdytfs.updateProfile;

import com.farmcrowdy.farmcrowdytfs.profile.FullFarmerInfoPojo;

public interface UpdateOnePresenter {
    void doUpdate(FullFarmerInfoPojo fullFarmerInfoPojo, int farmerId);
}
