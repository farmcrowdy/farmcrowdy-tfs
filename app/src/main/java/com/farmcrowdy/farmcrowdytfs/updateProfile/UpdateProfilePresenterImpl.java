package com.farmcrowdy.farmcrowdytfs.updateProfile;

import com.farmcrowdy.farmcrowdytfs.ApplicationInstance;
import com.farmcrowdy.farmcrowdytfs.MyEndpoint;
import com.farmcrowdy.farmcrowdytfs.engine.TinyDB;
import com.farmcrowdy.farmcrowdytfs.profile.FullFarmerInfoPojo;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.http.Part;

public class UpdateProfilePresenterImpl implements UpdateOnePresenter {
    UpdateInterface updateInterface;

    public UpdateProfilePresenterImpl(UpdateInterface updateInterface) {
        this.updateInterface = updateInterface;
    }

    @Override
    public void doUpdate(FullFarmerInfoPojo fullFarmerInfoPojo, int farmerId) {
        MyEndpoint myEndpoint  = ApplicationInstance.getRetrofit().create(MyEndpoint.class);
        int tfs_id = new TinyDB(ApplicationInstance.getContext()).getInt("user_id", 1);

        RequestBody fnameReq =  RequestBody.create(MediaType.parse("text/plain"), fullFarmerInfoPojo.getFname());
        RequestBody snameReq =  RequestBody.create(MediaType.parse("text/plain"), fullFarmerInfoPojo.getSname());
        RequestBody dobReq =  RequestBody.create(MediaType.parse("text/plain"), fullFarmerInfoPojo.getDob());
        RequestBody genderReq =  RequestBody.create(MediaType.parse("text/plain"), fullFarmerInfoPojo.getGender());
        RequestBody phoneReq =  RequestBody.create(MediaType.parse("text/plain"), fullFarmerInfoPojo.getPhone());
        RequestBody maritalStatusReq =  RequestBody.create(MediaType.parse("text/plain"), fullFarmerInfoPojo.getMarital_status());
        RequestBody cpReq =  RequestBody.create(MediaType.parse("text/plain"), fullFarmerInfoPojo.getCrop_proficiency());
        RequestBody incomeRangeReq =  RequestBody.create(MediaType.parse("text/plain"), fullFarmerInfoPojo.getIncome_range());
        RequestBody commentReq =  RequestBody.create(MediaType.parse("text/plain"), fullFarmerInfoPojo.getComment());
        RequestBody accuntReq =  RequestBody.create(MediaType.parse("text/plain"), String.valueOf(fullFarmerInfoPojo.getAcct_number()));
        RequestBody landReq =  RequestBody.create(MediaType.parse("text/plain"), fullFarmerInfoPojo.getLand_area_farmed());

        Call<ResponseBody> call  = myEndpoint.updateFarmer(farmerId,tfs_id,fnameReq,snameReq,dobReq,fullFarmerInfoPojo.getState_id(),fullFarmerInfoPojo.getLocal_id(),
                fullFarmerInfoPojo.getFarm_group_id(),fullFarmerInfoPojo.getFarm_location_id(),fullFarmerInfoPojo.getBank_id(),
                genderReq,phoneReq,maritalStatusReq,fullFarmerInfoPojo.getNumber_of_dependants(), fullFarmerInfoPojo.getAdd_labour(),cpReq,
                accuntReq,
                landReq,fullFarmerInfoPojo.getYears_of_experience(),incomeRangeReq,
                fullFarmerInfoPojo.getPrevious_training(), commentReq);

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    if(response.isSuccessful()) updateInterface.updateSuccessful();
                    else updateInterface.failureJors(response.message());
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                    updateInterface.failureJors(t.getMessage());
            }
        });
    }
}
