package com.farmcrowdy;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.farmcrowdy.farmcrowdytfs.ApplicationInstance;
import com.farmcrowdy.farmcrowdytfs.MainActivity;
import com.farmcrowdy.farmcrowdytfs.R;
import com.rafakob.floatingedittext.FloatingEditText;

public class MiddleWare extends AppCompatActivity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.middleware_layout);

        final FloatingEditText ipEdit = findViewById(R.id.ip_edit);

        findViewById(R.id.assign).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(ipEdit.getText().length() > 8) {
                    ApplicationInstance.setBaseUrl(String.valueOf(ipEdit.getText()));
                    startActivity(new Intent(MiddleWare.this, MainActivity.class));
                }
            }
        });
    }
}
