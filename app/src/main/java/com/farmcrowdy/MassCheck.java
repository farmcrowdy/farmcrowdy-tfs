package com.farmcrowdy;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;

import com.farmcrowdy.farmcrowdytfs.ApplicationInstance;
import com.farmcrowdy.farmcrowdytfs.DirectPojo.FarmerPojo;
import com.farmcrowdy.farmcrowdytfs.FarmersListAll;
import com.farmcrowdy.farmcrowdytfs.ListPojo.FarmerListPojo;
import com.farmcrowdy.farmcrowdytfs.MyEndpoint;
import com.farmcrowdy.farmcrowdytfs.R;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static java.sql.DriverManager.println;

public class MassCheck  extends AppCompatActivity {
    ProgressDialog progressDialog;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.masscheck_layout);
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Loading all farmers data");
        findViewById(R.id.verifyGinger).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                progressDialog.show();
                startVerification();
            }
        });
        
    }
    void startVerification() {
        MyEndpoint myEndpoint = ApplicationInstance.getRetrofit().create(MyEndpoint.class);
        Call<FarmerListPojo> myCaller = myEndpoint.getAllFarmers();
        myCaller.enqueue(new Callback<FarmerListPojo>() {
            @Override
            public void onResponse(Call<FarmerListPojo> call, Response<FarmerListPojo> response) {
                progressDialog.dismiss();
                progressDialog.cancel();
                if(response.isSuccessful()) {
                        ArrayList<String> databasePhoneList = new ArrayList<>();
                        for (int i=0; i<response.body().getMessage().size(); i++) {
                            String myPhone = String.valueOf(response.body().getMessage().get(i).getPhone());
                            String myPin = String.valueOf(response.body().getMessage().get(i).getAccess_pin());
                            int farmgroup = response.body().getMessage().get(i).getFarm_group_id();
                            String name = response.body().getMessage().get(i).getFname() +" "+ response.body().getMessage().get(i).getSname();

                            if (myPhone.length() > 9 && farmgroup !=2) {
                                if (!myPhone.substring(0, 1).contentEquals("0")) {
                                    myPhone = "0" + myPhone;
                                }
                                Log.d("Result", myPhone);
                                //Log.d("Number", myPhone);
                                //Log.d("Pin", myPin);
                            }
                        }

                    /*    ArrayList<String> newRegList = new ArrayList<>();
                    for(int i=0; i< new FarmersListAll().gingeredFarmers.length; i++) {
                        String farmerName = new FarmersListAll().farmerNames[i];
                        String gender = new FarmersListAll().genders[i];
                        String localGovt = new FarmersListAll().localGovt[i];

                        String gingeredPhone = new FarmersListAll().gingeredFarmers[i];
                        if (!gingeredPhone.substring(0, 1).contentEquals("0")) {
                            gingeredPhone = "0" + gingeredPhone;
                        }
                        gingeredPhone = gingeredPhone.replace(" ", "");
                        if(gingeredPhone.length() !=11) {
                            newRegList.add(gingeredPhone);
                            Log.d("Result", farmerName+" / "+gingeredPhone+" / "+gender+" / "+localGovt);
                        }
                    }
                    */



                  //  Log.d("Result", "Malfunc chosen Numbers: "+databasePhoneList.size());
                    //Log.d("Result", "Malfunc database: "+databasePhoneList.size());
                }
                else {
                    Log.d("Farmers", "Quering all farmers error due to: "+response.message());
                }
            }

            @Override
            public void onFailure(Call<FarmerListPojo> call, Throwable t) {
                progressDialog.dismiss();
                progressDialog.cancel();
                Log.d("Farmers", "Quering Fallout: "+t.getMessage());
            }
        });

    }
}
